<?php

namespace Tests\Unit;

use App\Controllers\PageController;
use App\Factory;
use App\Models\Item;
use App\Models\Operation;
use App\Models\Operations\Buy;
use App\Models\Operations\Sell;
use App\Traits\DatabaseMigrations;
use Tests\TestCase;

class ItemTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function item_title_can_be_changed()
    {
        $item = Factory::create(Item::class);

        $item->update([
            'title' => 'new title'
        ]);

        $item->fresh();

        $this->assertEquals('new title', $item->title);
    }
    
    /** @test */
    public function test_items_controller_sell_method()
    {
        $item = Factory::create(Item::class);

        $controller = new PageController();

        $item->update([
            'count' => 2,
            'sale_price' => $item->recommended_price
        ]);

        $item->fresh();

        ob_start();
        $controller->sell();
        ob_end_clean();

        $this->assertDatabaseHas(Operation::getTableName(), [
            'item_ID' => $item->ID,
            'type' => Operation::SELL,
            'date' => date('Y-m-d'),
            'count' => 2,
            'sale_price' => $item->recommended_price
        ]);
    }

    /** @test */
    public function item_today_fields_reset_after_sell_operation()
    {
        $item = Factory::create(Item::class, [
            'count'      => 2,
            'sale_price' => 100
        ]);

        $controller = new PageController();

        $_POST['items'][$item->ID]['count'] = 2;
        $_POST['items'][$item->ID]['sale_price'] = $item->recommended_price;

        ob_start();
        $controller->sell();
        ob_end_clean();

        $item->fresh();

        $this->assertEquals(0, $item->count);
        $this->assertEquals(0, $item->sale_price);
    }


    /** @test */
    public function items_can_be_exported()
    {
        $item = Factory::create(Item::class, [], 5);

        $controller = new PageController();

        ob_start();
        $controller->export();
        $data = json_decode(ob_get_contents(), true);
        ob_end_clean();

        $this->assertNotNull($data['href']);
        $this->assertFileExists($data['href']);
    }
    
    /** @test */
    public function removing_item_also_removes_all_operations_with_it()
    {
        $item = Factory::create(Item::class);

        $operation = new Buy($item);

        $operation->perform();

        $this->assertDatabaseHas(Operation::getTableName(), [
            'item_ID' => $item->ID
        ]);

        $item->remove();

        $this->assertDatabaseMissing(Operation::getTableName(), [
            'item_ID' => $item->ID
        ]);
    }
}
<?php

namespace Tests\Unit;

use App\Controllers\PageController;
use App\Controllers\ImportController;
use App\Controllers\OperationsController;
use App\Factory;
use App\Models\Item;
use App\Models\Operation;
use App\Models\Operations\Buy;
use App\Models\Operations\Sell;
use App\Seeds\OperationsSeeder;
use App\Traits\DatabaseMigrations;
use Tests\TestCase;

class OperationsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function items_can_be_exported()
    {
        (new OperationsSeeder())->execute();

        $controller = new OperationsController();

        ob_start();
        $controller->export();
        $data = json_decode(ob_get_contents(), true);
        ob_end_clean();

        $this->assertNotNull($data['href']);
        $this->assertFileExists($data['href']);
    }
}
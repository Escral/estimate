<?php

namespace Tests\Unit;

use App\Factory;
use App\Models\Item;
use App\Models\Operation;
use App\Models\Operations\Buy;
use App\Traits\DatabaseMigrations;
use Tests\TestCase;

class BuyOperationTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @return Item[]|Buy[]
     * @throws \Exception
     */
    public function createBuyOperation()
    {
        $item = Factory::create(Item::class);

        $operation = new Buy($item);

        return [$item, $operation];
    }

    /** @test */
    public function operation_can_be_created_with_given_item()
    {
        [$item, $operation] = $this->createBuyOperation();

        $this->assertEquals($operation->item_ID, $item->ID);
    }

    /** @test */
    public function operation_can_be_performed()
    {
        [$item, $operation] = $this->createBuyOperation();

        $operation->perform();

        $this->assertDatabaseHas(Operation::getTableName(), [
            'item_ID' => $item->ID,
            'type' => Operation::BUY,
            'date' => date('Y-m-d'),
            'count' => $item->stock,
            'sale_price' => $item->buy_price
        ]);
    }
}

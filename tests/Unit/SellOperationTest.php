<?php

namespace Tests\Unit;

use App\Factory;
use App\Models\Item;
use App\Models\Operation;
use App\Models\Operations\Sell;
use App\Traits\DatabaseMigrations;
use Tests\TestCase;

class SellOperationTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @return Item[]|Sell[]
     * @throws \Exception
     */
    public function createSellOperation()
    {
        $item = Factory::create(Item::class);

        $operation = new Sell($item);

        return [$item, $operation];
    }

    /** @test */
    public function operation_can_be_created_with_given_item()
    {
        [$item, $operation] = $this->createSellOperation();

        $this->assertEquals($operation->item_ID, $item->ID);
    }

    /** @test */
    public function operation_can_be_performed()
    {
        [$item, $operation] = $this->createSellOperation();

        $sale_price = $item->recommended_price + 10;

        $operation->perform($sale_price, 2);

        $this->assertDatabaseHas(Operation::getTableName(), [
            'item_ID'    => $item->ID,
            'type'       => Operation::SELL,
            'date'       => date('Y-m-d'),
            'count'      => 2,
            'sale_price' => $sale_price
        ]);
    }

    /** @test */
    public function item_stock_is_changed_after_operation()
    {
        [$item, $operation] = $this->createSellOperation();

        $stock = $item->getStock();

        $sale_price = $item->recommended_price + 10;

        $operation->perform($sale_price, 2);

        $item = Item::find($item->ID);

        $this->assertEquals($stock - 2, $item->getStock());
    }

    /** @test */
    public function can_not_sell_more_items_then_there_are_in_stock()
    {
        [$item, $operation] = $this->createSellOperation();

        $sale_price = $item->recommended_price + 10;
        $count = $item->getStock() + 1;

        $this->expectException(\Exception::class);

        $operation->perform($sale_price, $count);
    }
}

<?php

namespace Tests\Extensions;

use App\Database;

trait DatabaseConstraits
{
    /**
     * @param string $table
     * @param array $data
     */
    public function assertDatabaseHas($table, $data)
    {
        $result = $this->databaseExists($table, $data);

        self::assertThat($result, self::isTrue(), "Database table `{$table}` missing values: " . json_encode($data));
    }

    /**
     * @param string $table
     * @param array $data
     */
    public function assertDatabaseMissing($table, $data)
    {
        $result = $this->databaseExists($table, $data);

        self::assertThat($result, self::isFalse(), "Database table `{$table}` missing values: " . json_encode($data));
    }

    /**
     * @param string $table
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    protected function databaseExists($table, $data)
    {
        $values = [];
        $fields = [];

        foreach ($data as $key => $value) {
            $fields[] = $key . ' = ?';
            $values[] = $value;
        }

        $query = implode(' AND ', $fields);

        return (bool) Database::query("SELECT COUNT(*) FROM `{$table}` WHERE {$query}", $values)->fetchColumn();
    }
}
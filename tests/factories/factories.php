<?php

use App\Factory;
use App\Models\Item;
use App\Models\Operation;

Factory::setFaker();

Factory::add(Item::class, function(Faker\Generator $faker) {
    $price = $faker->randomNumber(3);

    return [
        'title' => $faker->sentence,
        'buy_date' => $faker->date(),
        'buy_price' => $price,
        'recommended_price' => $price * 1.2,
        'stock' => $faker->numberBetween(10, 50)
    ];
});
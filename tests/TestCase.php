<?php

namespace Tests;

use App\App;
use Tests\Extensions\DatabaseConstraits;

class TestCase extends \PHPUnit\Framework\TestCase
{
    use DatabaseConstraits;

    protected function setUp()
    {
        parent::setUp();

        (new App())->init();

        $reflection = new \ReflectionClass($this);

        foreach ($reflection->getTraits() as $trait) {
            $trait->getName()::boot();
        }
    }
}

<?php

namespace App\Contracts;

abstract class Migration
{
    abstract public function up();

    abstract public function down();

    public function migrate()
    {
        try {
            $this->down();
        } catch (\Exception $ex) {}

        $this->up();
    }
}
<?php

namespace App\Migrations;

use App\Contracts\Migration;
use App\Database;

class CreateEstimateMigration extends Migration
{
    public function up()
    {
        Database::query("
            CREATE TABLE estimate
            (
              ID                INT AUTO_INCREMENT PRIMARY KEY,
              user_ID           INT,
              title             VARCHAR(255) NOT NULL,
              estimate          TEXT         NOT NULL,
              created           DATETIME     NOT NULL DEFAULT NOW()
            )
            ENGINE = InnoDB;
        ");
    }

    public function down()
    {
        Database::query("DROP TABLE estimate");
    }
}
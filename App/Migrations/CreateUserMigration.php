<?php

namespace App\Migrations;

use App\Contracts\Migration;
use App\Database;

class CreateUserMigration extends Migration
{
    public function up()
    {
        Database::query("
            CREATE TABLE user
            (
              ID                INT AUTO_INCREMENT PRIMARY KEY,
              login             VARCHAR(255) NOT NULL,
              password          VARCHAR(255) NOT NULL,
              active_estimate   INT NULL
            )
            ENGINE = InnoDB;
        ");
    }

    public function down()
    {
        Database::query("DROP TABLE estimate");
    }
}
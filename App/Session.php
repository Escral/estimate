<?php

namespace App;

class Session
{
    private static $name;
    private static $data = [];

    public static function init()
    {
        session_start();

        if (isset($_COOKIE['session'])) {
            self::$name = $_COOKIE['session'];
        } else {
            self::$name = md5(microtime());
            setcookie('session', self::$name, time() + 3600 * 24 * 31);
        }

        self::clearOld();

        self::getData();
    }

    public static function clearOld()
    {
        Database::query("DELETE FROM `sessions` WHERE updated < '" . date('Y-m-d', time() - 31 * 3600 * 24) . "'");
    }

    public static function getAll()
    {
        return self::$data;
    }

    /**
     * @param string $key
     * @param null|\Closure $value
     * @return null|mixed
     */
    public static function get($key, $value = null)
    {
        if (isset(self::$data[$key])) {
            return self::$data[$key];
        } else {
            if ($value instanceof \Closure) {
                return $value();
            } else if ($value !== null) {
                return $value;
            } else {
                return null;
            }
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    public static function has($key)
    {
        return isset(self::$data[$key]);
    }

    /**
     * @param string $key
     */
    public static function remove($key)
    {
        if (isset(self::$data[$key])) {
            unset(self::$data[$key]);
        }
    }

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public static function set($key, $value)
    {
        return self::$data[$key] = $value;
    }

    public static function save()
    {
        $value = serialize(self::$data);

        Database::query("UPDATE `sessions` SET `value` = ? WHERE name = '" . self::$name . "'", [$value]);
    }

    public static function clear()
    {
        self::$data = [];

        Database::query("UPDATE `sessions` SET `value` = '' WHERE name = '" . self::$name . "'");
    }

    private static function getData()
    {
        $value = Database::value("SELECT value FROM `sessions` WHERE name = '" . self::$name . "'");

        if (! $value) {
            self::writeSession();
        } else {
            self::$data = unserialize($value['value']);
        }
    }

    private static function writeSession()
    {
        Database::query("INSERT INTO `sessions` SET name = '" . self::$name . "'");
    }
}
<?php

namespace App\Controllers;

use App\Data;
use App\Items;
use App\Models\Floor;
use App\Models\Item;
use App\View;

class BoilerController extends Controller
{
    private $items;

    public function __construct()
    {
        parent::__construct();

        Data::$prefix2 = 'boiler';
    }

    public function kettles()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['kettles'])) {
                $kettles = (array) Data::floor()::get('kettles');

                foreach ($_POST['kettles'] as $key => $item) {
                    if ($item['count'] == 0) {
                        unset($kettles[$key]);
                    } else {
                        $kettles[$key]['count'] = $item['count'];
                    }
                }

                $kettles = Data::floor()::set('kettles', $kettles);

//                Items::afterAdd($kettles);
            }

            unset($_POST['submit']);
            $this->boiler();
        } else {
            $kettles = (array) Data::floor()::get('kettles');
            $selected = [];

            $n = 601;
            foreach ($kettles as $key => $item) {
                $selected[$key] = new Item($n, $item['title'], $item['count'] ?? 0);

                $n++;
            }

            print View::render('boiler/kettles', compact('selected'));
        }
    }

    public function add_kettles()
    {
        $kettles = Data::floor()::get('kettles');

        if (isset($_POST['item'])) {
            $data = [
                'count' => 1,
                'item' => 0,
                'title' => $_POST['item']
            ];

            $kettles[] = $data;
        }

        Data::floor()::set('kettles', $kettles);

        $this->kettles();
    }

    
    public function remove_kettles()
    {
        $kettles = Data::floor()::get('kettles');

        if (isset($_POST['key'])) {
            if (isset($kettles[$_POST['key']]))
                unset($kettles[$_POST['key']]);
        }

        Data::floor()::set('kettles', $kettles);

        $this->kettles();
    }



    public function boiler()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['boiler'])) {
                $boiler = (array) Data::floor()::get('boiler');

                foreach ($_POST['boiler'] as $key => $item) {
                    if ($item['count'] == 0) {
                        unset($boiler[$key]);
                    } else {
                        $boiler[$key]['count'] = $item['count'];
                    }
                }

                $boiler = Data::floor()::set('boiler', $boiler);
            }

            unset($_POST['submit']);
            $this->kettle_piping();
        } else {
            $boiler = (array) Data::floor()::get('boiler');
            $selected = [];

            $n = 701;
            foreach ($boiler as $key => $item) {
                $selected[$key] = new Item($n, $item['title'], $item['count'] ?? 0);

                $n++;
            }

            print View::render('boiler/boiler', compact('selected'));
        }
    }

    public function add_boiler()
    {
        $boiler = Data::floor()::get('boiler');

        if (isset($_POST['item'])) {
            $data = [
                'count' => 1,
                'item' => 0,
                'title' => $_POST['item']
            ];

            $boiler[] = $data;
        }

        Data::floor()::set('boiler', $boiler);

        $this->boiler();
    }
    
    public function remove_boiler()
    {
        $boiler = Data::floor()::get('boiler');

        if (isset($_POST['key'])) {
            if (isset($boiler[$_POST['key']]))
                unset($boiler[$_POST['key']]);
        }

        Data::floor()::set('boiler', $boiler);

        $this->boiler();
    }


    public function kettle_piping()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['kettle_piping'])) {
                $kettle_piping = (array) Data::floor()::get('kettle_piping');

                foreach ($_POST['kettle_piping'] as $key => $item) {
                    if ($item['count'] == 0) {
                        unset($kettle_piping[$key]);
                    } else {
                        $kettle_piping[$key]['count'] = $item['count'];
                    }
                }

                $kettle_piping = Data::floor()::set('kettle_piping', $kettle_piping);
            }

            unset($_POST['submit']);
            $this->boiler_piping();
        } else {
            $kettle_piping = (array) Data::floor()::get('kettle_piping');
            $types = (array) Data::floor()::get('kettle_piping_type');

            $items = [];
            foreach ($types as $key => $count) {
                if ($count) {
                    $items[$key] = [
                        'title' => Items::getKettlePipingTitle($key),
                        'items' => Items::getKettlePiping($key, $count, $kettle_piping),
                        'count' => $count
                    ];
                }
            }


            print View::render('boiler/kettle_piping', compact('items', 'types'));
        }
    }

    public function set_kettle_piping_type()
    {
        if (isset($_POST['type'])) {
            Data::floor()::set('kettle_piping_type', $_POST['type']);
        }

        unset($_POST['submit']);
        $this->kettle_piping();
    }



    public function boiler_piping()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['boiler_piping'])) {
                $boiler_piping = (array) Data::floor()::get('boiler_piping');

                foreach ($_POST['boiler_piping'] as $key => $item) {
                    if ($item['count'] == 0) {
                        unset($boiler_piping[$key]);
                    } else {
                        $boiler_piping[$key]['count'] = $item['count'];
                    }
                }

                $boiler_piping = Data::floor()::set('boiler_piping', $boiler_piping);
            }

            unset($_POST['submit']);
            $this->boiler_collector();
        } else {
            $boiler_piping = (array) Data::floor()::get('boiler_piping');
            $types = (array) Data::floor()::get('boiler_piping_type');

            $items = [];
            foreach ($types as $key => $count) {
                if ($count) {
                    $items[$key] = [
                        'title' => Items::getBoilerPipingTitle($key),
                        'items' => Items::getBoilerPiping($key, $count, $boiler_piping),
                        'count' => $count
                    ];
                }
            }


            print View::render('boiler/boiler_piping', compact('items', 'types'));
        }
    }

    public function set_boiler_piping_type()
    {
        if (isset($_POST['type'])) {
            Data::floor()::set('boiler_piping_type', $_POST['type']);
        }

        unset($_POST['submit']);
        $this->boiler_piping();
    }



    public function boiler_collector()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['boiler_collector'])) {
                Data::floor()::set('boiler_collector', $_POST['boiler_collector']);
            }

            if (isset($_POST['boiler_collector_value'])) {
                Data::floor()::set('boiler_collector_value', $_POST['boiler_collector_value']);
            }

            if (isset($_POST['gunner'])) {
                Data::floor()::set('gunner', $_POST['gunner']);
            }

            unset($_POST['submit']);
            $this->collector_piping();
        } else {
            $boiler_collector = Data::floor()::get('boiler_collector');
            $boiler_collector_value = Data::floor()::get('boiler_collector_value');
            $gunner = Data::floor()::get('gunner');

            print View::render('boiler/boiler_collector', compact('boiler_collector', 'boiler_collector_value', 'gunner'));
        }
    }





    public function collector_piping()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['collector_piping'])) {
                $collector_piping = (array) Data::floor()::get('collector_piping');

                foreach ($_POST['collector_piping'] as $key => $item) {
                    if ($item['count'] == 0) {
                        unset($collector_piping[$key]);
                    } else {
                        $collector_piping[$key]['count'] = $item['count'];
                    }
                }

                $collector_piping = Data::floor()::set('collector_piping', $collector_piping);
            }

            unset($_POST['submit']);
            $this->pump();
        } else {
            $collector_piping = (array) Data::floor()::get('collector_piping');
            $types = (array) Data::floor()::get('collector_piping_type');

            $items = [];
            foreach ($types as $key => $count) {
                if ($count) {
                    $items[$key] = [
                        'title' => Items::getCollectorPipingTitle($key),
                        'items' => Items::getCollectorPiping($key, $count, $collector_piping),
                        'count' => $count
                    ];
                }
            }


            print View::render('boiler/collector_piping', compact('items', 'types'));
        }
    }

    public function set_collector_piping_type()
    {
        if (isset($_POST['type'])) {
            Data::floor()::set('collector_piping_type', $_POST['type']);
        }

        unset($_POST['submit']);
        $this->collector_piping();
    }




    public function pump()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['pump'])) {
                $pump = (array) Data::floor()::get('pump');

                foreach ($_POST['pump'] as $key => $item) {
                    if ($item['count'] == 0) {
                        unset($pump[$key]);
                    } else {
                        $pump[$key]['count'] = $item['count'];
                    }
                }

                $pump = Data::floor()::set('pump', $pump);
            }

            unset($_POST['submit']);
            $this->kettle_piping();
        } else {
            $pump = (array) Data::floor()::get('pump');
            $selected = [];

            $n = 701;
            foreach ($pump as $key => $item) {
                $selected[$key] = new Item($n, $item['title'], $item['count'] ?? 0);

                $n++;
            }

            print View::render('boiler/pump', compact('selected'));
        }
    }

    public function add_pump()
    {
        $pump = Data::floor()::get('pump');

        if (isset($_POST['item'])) {
            $data = [
                'count' => 1,
                'item' => 0,
                'title' => $_POST['item']
            ];

            $pump[] = $data;
        }

        Data::floor()::set('pump', $pump);

        $this->pump();
    }
    
    public function remove_pump()
    {
        $pump = Data::floor()::get('pump');

        if (isset($_POST['key'])) {
            if (isset($pump[$_POST['key']]))
                unset($pump[$_POST['key']]);
        }

        Data::floor()::set('pump', $pump);

        $this->pump();
    }
}
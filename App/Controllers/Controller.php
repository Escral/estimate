<?php

namespace App\Controllers;

use App\Data;

class Controller
{
    public function __construct()
    {
        $floors = Data::get('floors');

        if ($floors == null) {
            $floors = Data::set('floors', [
                0 => 1,
                1 => 1,
            ]);
        }

        $first = key($floors);

        if (Data::get('floor') == null)
            Data::set('floor', $first);
    }
}
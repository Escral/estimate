<?php

namespace App\Controllers;

use App\Data;
use App\Helpers\Export;
use App\Helpers\ListOrganizer;
use App\Items;
use App\Models\Estimate;
use App\Models\Floor;
use App\Models\Item;
use App\Path;
use App\View;

class PageController
{
    public function floors_old($page = 'radiators')
    {
        if (isset($_POST['submit'])) {
            if ($page == 'radiators') {
                $controller = new RadiatorsController();
            } else if ($page == 'water') {
                $controller = new WaterController();
            } else if ($page == 'hfloors') {
                $controller = new HFloorsController();
            }

            if (isset($_POST['floors'])) {
                $floors = [];

                foreach ($_POST['floors'] as $floor => $heated) {
                    $floors[$floor] = $heated;
                }

                Data::set('floors', $floors);
                Data::set('floor', key($floors));
            }

            unset($_POST['submit']);
            $controller->index();
        } else {
            $floors = Data::get('floors');

            if (! is_array($floors)) {
                $floors = Data::set('floors', [
                    0   => 1,
                    1   => 1,
                    100 => 1
                ]);
            }

            $max = 0;
            foreach ($floors as $n => $val) {
                if ($n > $max) {
                    $max = $n;
                }
            }
            $nextFloor = $max + 1;

            print View::render($page . '/floors', compact('floors', 'nextFloor'));
        }
    }

    public function export()
    {
//        $floorsSteps = Floor::getStatuses();
        $floorsVisited = Floor::getVisitedFloorsAll();

        print View::render('pages/export', compact('floors', 'floorsVisited'));
    }

    public function clear()
    {
        Data::clear();
    }

    public function exportFile()
    {
        $folder = Export::$exportDir;

        $files = glob($folder . '/*');

        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        if (isset($_POST['separate'])) {
            $href = $this->exportSeparated($_POST['group'] ?? null);
        } else {
            $href = $this->exportFull($_POST['group'] ?? null);
        }

        if (! $href) {
            $result = [
                'error'   => 1,
                'message' => lang('Can not export data')
            ];
        } else {
            $result = [
                'success' => 1,
                'message' => lang('Exported successfully'),
                'href'    => $href
            ];
        }

        print json_encode($result);
    }

    private function exportSeparated($grouped = false)
    {
        $fields = [
            'code'    => 'Код',
            'title'   => 'Название',
            'count'   => 'Количество',
            'price'   => 'Цена',
            'space'   => '',
            'comment' => $grouped ? '' : 'Комментарий'
        ];

        $params = $_POST['floors'] ?? [];

        $name = Estimate::getActive()->title;

        $file = Export::$exportDir . '/' . $name . '.zip';

        $zip = new \ZipArchive();
        if ($zip->open($file, \ZipArchive::CREATE) === true) {
            foreach ($params as $section => $floors) {
                foreach ($floors as $floor => $is) {
                    list($items, $comments) = Items::getForExport([
                        $section => [
                            $floor => $is
                        ]
                    ]);

                    if ($grouped) {
                        $items = $this->group($items);
                        $comments = [];
                    }

                    $items = ListOrganizer::toArray($items, $grouped);

                    $href = Export::xls($items, $fields, $comments, $section . $floor);

                    $path = $_SERVER['DOCUMENT_ROOT'] . $href;
                    $zip->addFile($path, Floor::getSectionTitleByName($section) . '. ' . Floor::getFloorName($floor) . '.xls');
                }
            }

            if ($zip->close()) {
                return $file;
            }
        }

        return null;
    }

    private function exportFull($grouped = false)
    {
        $fields = [
            'code'    => 'Код',
            'title'   => 'Название',
            'count'   => 'Количество',
            'price'   => 'Цена',
            'space'   => '',
            'comment' => $grouped ? '' : 'Комментарий'
        ];

        list($items, $comments) = Items::getForExport($_POST['floors'] ?? [], $grouped);

        if ($grouped) {
            $items = $this->group($items);
            $comments = [];
        }

        $items = ListOrganizer::toArray($items, $grouped);

        if ($grouped) {
            $this->sort($items);
        }

        $name = $this->getExportFileName($_POST['floors'] ?? []);

        $href = Export::xls($items, $fields, $comments, $name);

        return $href;
    }

    private function group($items)
    {
        $newItems = [];
        foreach ($items as $item) {
            if ($item instanceof Item) {
                if (isset($newItems[$item->id])) {
                    $newItems[$item->id]->addCount($item->count);
                } else {
                    $newItems[$item->id] = $item;
                }
            }
        }

        return $newItems;
    }

    private function sort(&$items)
    {
        usort($items, function($a, $b) {
            return strcmp($a['title'], $b['title']);
        });
    }

    private function getExportFileName($floors = null)
    {
        $estimate = Estimate::getActive()->title;

        if ($floors) {
            $names = [];

            foreach ($floors as $section => $sectionFloors) {
                if (! is_array($sectionFloors))
                    continue;

                $names[] = Floor::getSectionTitleByName($section);
                $sections = [];

                foreach ($sectionFloors as $floor_ID => $is) {
                    $sections[] = Floor::getFloorName($floor_ID);
                }
            }

            if (count($names) == 1) {
                return $estimate . '. ' . $names[0] . '. ' . implode(", ", $sections);
            } else {
                return $estimate . '. ' . implode(", ", $names);
            }
        } else {
            return $estimate;
        }
    }
}
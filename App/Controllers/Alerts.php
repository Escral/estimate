<?php

namespace App\Controllers;

class Alerts
{
    protected static $alerts = [];

    public static function add($message, $tag = 'primary')
    {
        self::$alerts[] = [
            'message' => $message,
            'tag' => $tag
        ];
    }

    public static function getAll()
    {
        return self::$alerts;
    }

    public static function flush()
    {
        self::$alerts = [];
    }

    public static function shift()
    {
        return array_shift(self::$alerts);
    }
}
<?php

namespace App\Controllers;

use App\Auth;
use App\Data;
use App\Models\Estimate;

class EstimateController
{
    public function index()
    {
        $estimates = Estimate::getList([
            'where' => [
                'user_ID' => Auth::user()->ID
            ],
            'orderDirection' => 'desc'
        ]);

        return [
            'estimates' => $estimates
        ];
    }

    public function add()
    {
        $title = $_POST['title'] ?? null;

        if ($title) {
            $estimate = new Estimate();
            $estimate->setData([
                'title'   => $title,
                'user_ID' => Auth::user()->ID
            ]);
            $estimate = $estimate->create();

            Auth::user()->setActiveEstimate($estimate);
            Data::clear();

            echo json_encode([
                'id' => $estimate->ID
            ]);
        }
    }

    public function delete()
    {
        $id = $_REQUEST['id'] ?? null;

        if ($id) {
            $estimate = Estimate::find($id);

            if ($estimate && $estimate->belongsTo(Auth::user()))
                $estimate->remove();
        }
    }

    public function change()
    {
        $id = $_REQUEST['id'] ?? null;

        if ($id) {
            $estimate = Estimate::find($id);

            if ($estimate && $estimate->belongsTo(Auth::user())) {
                Auth::user()->setActiveEstimate($estimate);
            }
        }
    }
}
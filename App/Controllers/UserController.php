<?php

namespace App\Controllers;

use App\Auth;
use App\Helpers\Http;
use App\Models\User;
use App\View;

class UserController
{
    public function loginForm()
    {
        echo View::render('user/login');

        unset($_SESSION['error']);
    }

    public function login()
    {
        $login = $_POST['login'] ?? false;
        $password = $_POST['password'] ?? false;

        if (Auth::attempt($login, $password)) {
            Http::redirect('/');
        } else {
            $_SESSION['error'] = 'Неверный логин или пароль';
        }

        $this->loginForm();
    }

    public function registerForm()
    {
        echo View::render('user/register');

        unset($_SESSION['error']);
    }

    public function register()
    {
        $login = $_POST['login'] ?? false;
        $password = $_POST['password'] ?? false;
        $password_repeat = $_POST['password_repeat'] ?? false;

        $errors = [];

        if ($password !== $password_repeat) {
            $_SESSION['error']['password_repeat'] = 'Пароли не совпадают';
            $this->registerForm();
            return;
        }

        if ($user = User::register($login, $password, $errors)) {
            $user->login();
            Http::redirect('/');
        } else {
            $_SESSION['error'] = $errors;
        }

        $this->registerForm();
    }
}
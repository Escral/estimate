<?php

namespace App\Controllers;

use App\Data;
use App\Items;
use App\Models\Floor;
use App\Path;
use App\View;

class HFloorsController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        Data::$prefix2 = 'hfloors';
    }

    public function riser()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['riser'])) {
                Data::floor()::set('riser', $_POST['riser']);

                if ($_POST['riser']['size'] == 0) {
                    unset($_POST['submit']);
                    $this->area();
                    exit();
                }
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('riser.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->insulation_riser();
        } else {
            $comment = Data::floor()::get('riser.comment');
            $riser = Data::floor()::get('riser');

            $items = [
                Items::get(PIPE_25)->setCount(($pipes[PIPE_25] ?? null)),
                Items::get(PIPE_32)->setCount(($pipes[PIPE_32] ?? null)),
            ];
            
            if (! $riser['item']) {
                $riser['item'] = PIPE_32;
            }

            print View::render('hfloors/riser', compact('items', 'comment', 'riser'));
        }
    }

    public function insulation_riser()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('insulation_riser', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('insultaion_riser.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->corners_riser();
        } else {
            $comment = Data::floor()::get('insultaion_riser.comment');
            $riser = Data::floor()::get('riser');
            $insulation = Data::floor()::get('insulation_riser');

            $count = [
                INSULATION_25 => $insulation[INSULATION_25] ?? ((int) $riser['item'] == PIPE_25 ? (int) $riser['size'] : 0),
                INSULATION_32 => $insulation[INSULATION_32] ?? ((int) $riser['item'] == PIPE_32 ? (int) $riser['size'] : 0),
            ];

            $items = [
                PIPE_25 => Items::get(INSULATION_25)->setCount($count[INSULATION_25]),
                PIPE_32 => Items::get(INSULATION_32)->setCount($count[INSULATION_32]),
            ];

            print View::render('hfloors/insulation_riser', compact('items', 'comment'));
        }
    }

    public function corners_riser()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('corners_riser', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('corners_riser.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->couplings_riser();
        } else {
            $comment = Data::floor()::get('corners_riser.comment');
            $riser = Data::floor()::get('riser');
            $corners = Data::floor()::get('corners_riser');

            $pipes = [
                PIPE_25 => 0,
                PIPE_32 => 0
            ];

            if ($riser['item'] == PIPE_25) {
                $pipes[PIPE_25] = $riser['size'];
            }
            if ($riser['item'] == PIPE_32) {
                $pipes[PIPE_32] = $riser['size'];
            }

            $count = [
                CORNER_90_25 => $corners[CORNER_90_25] ?? (max(ceil(((int) $pipes[PIPE_25] * 0.4) / 2) * 2, ($pipes[PIPE_25] ? 6 : ''))),
                CORNER_90_32 => $corners[CORNER_90_32] ?? (max(ceil(((int) $pipes[PIPE_32] * 0.4) / 2) * 2, ($pipes[PIPE_32] ? 6 : ''))),
                CORNER_45_25 => $corners[CORNER_45_25] ?? (ceil(((int) $pipes[PIPE_25] * 0.2) / 2) * 2),
                CORNER_45_32 => $corners[CORNER_45_32] ?? (ceil(((int) $pipes[PIPE_32] * 0.2) / 2) * 2),
            ];

            $items90 = [
                Items::get(CORNER_90_25)->setCount($count[CORNER_90_25]),
                Items::get(CORNER_90_32)->setCount($count[CORNER_90_32])
            ];

            $items45 = [
                Items::get(CORNER_45_25)->setCount($count[CORNER_45_25]),
                Items::get(CORNER_45_32)->setCount($count[CORNER_45_32])
            ];

            print View::render('hfloors/corners_riser', compact('items90', 'items45', 'comment'));
        }
    }

    public function couplings_riser()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('couplings_riser', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('couplings_riser.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->area();
        } else {
            $comment = Data::floor()::get('couplings_riser.comment');
            $riser = Data::floor()::get('riser');
            $couplings = Data::floor()::get('couplings_riser');

            $pipes = [
                PIPE_25 => 0,
                PIPE_32 => 0
            ];

            if ($riser['item'] == PIPE_25) {
                $pipes[PIPE_25] = $riser['size'];
            }
            if ($riser['item'] == PIPE_32) {
                $pipes[PIPE_32] = $riser['size'];
            }


            $count = [
                COUPLING_25 => $couplings[COUPLING_25] ?? (ceil((int) $pipes[PIPE_25] * 0.25 + ($pipes[PIPE_25] ? 2 : 0))),
                COUPLING_32 => $couplings[COUPLING_32] ?? (ceil((int) $pipes[PIPE_32] * 0.25 + ($pipes[PIPE_32] ? 2 : 0))),
            ];

            $items = [
                Items::get(COUPLING_25)->setCount($count[COUPLING_25]),
                Items::get(COUPLING_32)->setCount($count[COUPLING_32])
            ];

            print View::render('hfloors/couplings_riser', compact('items', 'comment'));
        }
    }

    public function scheme()
    {
        $scheme = $schemeWas = Data::floor()::get('scheme');

        if (isset($_POST['submit'])) {
            if (isset($_POST['scheme'])) {
                $scheme = Data::floor()::set('scheme', $_POST['scheme']);
            }

            if ($schemeWas and $schemeWas != $scheme) {
                if ($schemeWas == 1) {
                    Path::forget('area');
                }
                elseif ($schemeWas == 2) {
                    Path::forget('area_2');
                }
            }

            // Вся комната
            if ($scheme == 1) {
                unset($_POST['submit']);
                $this->riser();
            } else if ($scheme == 2) {
                unset($_POST['submit']);
                $this->area_2();
            }
        } else {
            print View::render('hfloors/scheme', compact('scheme'));
        }
    }

    public function area($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['area'])) {
                Data::floor()::set('area', $_POST['area']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('area.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            if (isset($data['next'])) {
                $this->pipe_2();
            } else {
                $this->pipe();
            }
        } else {
            $comment = Data::floor()::get('area.comment');
            $area = Data::floor()::get('area');

            print View::render('hfloors/area' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('area', 'comment')));
        }
    }

    public function pipe($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('pipes', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('pipes.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            if (isset($data['next'])) {
                $this->insulation_2();
            } else {
                $this->insulation();
            }
        } else {
            $comment = Data::floor()::get('pipes.comment');
            $pipes = Data::floor()::get('pipes');
            $area = Data::floor()::get('area');

            $count = ceil($area * 6.5);

            $items = [
                Items::get(H_PIPE_16)->setCount(($pipes[H_PIPE_16] ?? $count)),
            ];

            print View::render('hfloors/pipe' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('items', 'comment')));
        }
    }

    public function insulation($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['insulation'])) {
                Data::floor()::set('insulation', $_POST['insulation']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('insulation.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            if (isset($data['next'])) {
                $this->skin_2();
            } else {
                $this->skin();
            }
        } else {
            $comment = Data::floor()::get('insulation.comment');
            $insulation = Data::floor()::get('insulation');

            if (! isset($insulation[0]['thickness']))
                $insulation[0]['thickness'] = 3;
            if (! isset($insulation[1]['thickness']))
                $insulation[1]['thickness'] = 1;

            $area = Data::floor()::get('area');

            print View::render('hfloors/insulation' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('insulation', 'area', 'comment')));
        }
    }

    public function skin($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('skin', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('skin.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            if (isset($data['next'])) {
                $this->insulation_join_2();
            } else {
                $this->insulation_join();
            }
        } else {
            $skin = Data::floor()::get('skin');
            $area = Data::floor()::get('area');
            $comment = Data::floor()::get('skin.comment');

            $items = [
                Items::get(SKIN_1)->setCount(($skin[SKIN_1] ?? $area)),
            ];

            print View::render('hfloors/skin' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('items', 'comment')));
        }
    }

    public function insulation_join($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['insulation'])) {
                Data::floor()::set('insulation_join', $_POST['insulation']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('insulation_join.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            if (isset($data['next'])) {
                $this->clips_2();
            } else {
                $this->clips();
            }
        } else {
            $comment = Data::floor()::get('insulation_join.comment');
            $insulation = Data::floor()::get('insulation_join');
            $insulation_normal = Data::floor()::get('insulation');

            if (! isset($insulation[0]['per_list']))
                $insulation[0]['per_list'] = 3;

            $items = [
                Items::get(JOIN_1),
                Items::get(JOIN_2),
                Items::get(JOIN_3),
                Items::get(JOIN_4),
                Items::get(JOIN_5)
            ];

            $area = Data::floor()::get('area');

            if (! isset($insulation[0]['count']) and isset($insulation_normal[0]['count']))
                $insulation[0]['count'] = $insulation_normal[0]['count'] * 2;

            if (! isset($insulation[1]['count']) and isset($insulation_normal[0]['count']))
                $insulation[1]['count'] = ceil($insulation_normal[0]['count'] / 6);

            print View::render('hfloors/insulation_join' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('insulation', 'insulation_normal', 'area', 'items', 'comment')));
        }
    }

    public function clips($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('clips', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('clips.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            if (isset($data['next'])) {
                $this->fjvr();
            } else {
                $this->collector();
            }
        } else {
            $comment = Data::floor()::get('clips.comment');
            $clips = Data::floor()::get('clips');
            $pipes = Data::floor()::get('pipes');

            $count = ceil(($pipes[H_PIPE_16] ?? 0) * 1);

            $items = [
                Items::get(CLIPS_1)->setCount(($clips[CLIPS_1] ?? $count)),
            ];

            print View::render('hfloors/clips' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('items', 'comment')));
        }
    }

    public function collector($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['collector'])) {
                Data::floor()::set('collector', $_POST['collector']);
            }

            if (isset($_POST['flowmeter'])) {
                Data::floor()::set('flowmeter', $_POST['flowmeter']);
            }

            if (isset($_POST['tailstocks'])) {
                Data::floor()::set('tailstocks', $_POST['tailstocks']);
            }

            if (isset($_POST['col_valves'])) {
                Data::floor()::set('col_valves', $_POST['col_valves']);
            }

            if (isset($_POST['col_mp'])) {
                Data::floor()::set('col_mp', $_POST['col_mp']);
            }
            if (isset($_POST['col_corners'])) {
                Data::floor()::set('col_corners', $_POST['col_corners']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('collector.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            if (isset($data['next'])) {
                $this->gerpex_2();
            } else {
                $this->gerpex();
            }
        } else {
            $comment = Data::floor()::get('collector.comment');
            $count = ceil(Data::floor()::get('area') / 12);

            $count = max($count, 3);

            $collector = Data::floor()::get('collector');
            $flowmeter = Data::floor()::get('flowmeter');
            $tailstocks = Data::floor()::get('tailstocks');
            $col_valves = Data::floor()::get('col_valves');
            $col_mp = Data::floor()::get('col_mp');
            $col_corners = Data::floor()::get('col_corners');
            $use_tailstocks = Data::floor()::get('use_tailstocks');
            $use_valves = Data::floor()::get('use_valves');
            $use_col_mp = Data::floor()::get('use_col_mp');
            $use_col_corners = Data::floor()::get('use_col_corners');

            if (! isset($flowmeter))
                $flowmeter = 2;

            print View::render('hfloors/collector' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('count', 'collector', 'flowmeter', 'tailstocks', 'col_valves', 'use_tailstocks', 'use_valves', 'use_col_mp', 'col_mp', 'use_col_corners', 'col_corners', 'comment')));
        }
    }

    public function gerpex($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['gerpex'])) {
                Data::floor()::set('gerpex', $_POST['gerpex']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('gerpex.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            if (isset($data['next'])) {
                $this->corner_fixes_2();
            } else {
                $this->corner_fixes();
            }
        } else {
            $comment = Data::floor()::get('gerpex.comment');
            if (! isset($data['next'])) {
                $count = (int) Data::floor()::get('collector') * 2;
            } else {
                $count = (int) Data::floor()::get('fjvr') * 2;
            }

            $gerpex = Data::floor()::get('gerpex');

            print View::render('hfloors/gerpex' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('count', 'gerpex', 'comment')));
        }
    }

    public function corner_fixes($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['corner_fixes'])) {
                Data::floor()::set('corner_fixes', $_POST['corner_fixes']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('corner_fixes.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            if (isset($data['next'])) {
                $this->work_2();
            } else {
                $this->locker();
            }
        } else {
            $comment = Data::floor()::get('corner_fixes.comment');
            if (! isset($data['next'])) {
                $count = (int) Data::floor()::get('collector') * 2;
            } else {
                $count = (int) Data::floor()::get('fjvr') * 2;
            }

            $corner_fixes = Data::floor()::get('corner_fixes');

            print View::render('hfloors/corner_fixes' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('count', 'corner_fixes', 'comment')));
        }
    }

    public function locker($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['locker'])) {
                Data::floor()::set('locker', $_POST['locker']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('locker.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            if (isset($data['next'])) {
                $this->additional_2();
            } else {
                $this->additional();
            }
        } else {
            $comment = Data::floor()::get('locker.comment');
            $size = (int) Data::floor()::get('collector') * 5 + 35;
            $size = (int) Data::floor()::get('collector') * 5 * 2;
            $locker = Data::floor()::get('locker');

            print View::render('hfloors/locker' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('size', 'locker', 'comment')));
        }
    }

    public function additional($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('additional', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('additional.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            if (isset($data['next'])) {
                $this->work_2();
            } else {
                $this->work();
            }
        } else {
            $comment = Data::floor()::get('additional.comment');
            $additional = Data::floor()::get('additional');
            $area = Data::floor()::get('area');

            $riser = Data::floor()::get('riser');

            $count = [
                ADDITIONAL_SCREWS      => (array_sum((array) Data::floor()::get('legs')) * 8),
                ADDITIONAL_CLAMPING_1  => ($riser['item'] == PIPE_20 ? $riser['size'] : ''),
                ADDITIONAL_CLAMPING_2  => ($riser['item'] == PIPE_25 ? $riser['size'] : ''),
                ADDITIONAL_CLAMPING_3  => ($riser['item'] == PIPE_32 ? $riser['size'] : ''),
                ADDITIONAL_WALL_INSULATION  => ($additional[ADDITIONAL_WALL_INSULATION] ?? ceil($area / 1.1)),
                ADDITIONAL_SELF_SCREWS => 0
            ];

            $items = [
                Items::get(ADDITIONAL_SCREWS)->setCount($count[ADDITIONAL_SCREWS]),
                Items::get(ADDITIONAL_CLAMPING_1)->setCount($count[ADDITIONAL_CLAMPING_1]),
                Items::get(ADDITIONAL_CLAMPING_2)->setCount($count[ADDITIONAL_CLAMPING_2]),
                Items::get(ADDITIONAL_CLAMPING_3)->setCount($count[ADDITIONAL_CLAMPING_3]),
                Items::get(ADDITIONAL_WALL_INSULATION)->setCount($count[ADDITIONAL_WALL_INSULATION]),
                Items::get(ADDITIONAL_SELF_SCREWS)->setCount($count[ADDITIONAL_SELF_SCREWS])
            ];

            print View::render('hfloors/additional' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('additional', 'items', 'comment')));
        }
    }

    public function work($data = [])
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('work', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('work.comment', $_POST['comment']);
            }

            unset($_POST['submit']);

            Alerts::add("Вы закончили редактирование этажа");
            (new FloorsController())->floors();
        } else {
            $comment = Data::floor()::get('work.comment');
            $riser = Data::floor()::get('riser');

            $work = Data::floor()::get('work');

            $area = Data::floor()::get('area');

            $fjvr = Data::floor()::get('fjvr');

            $count = [
                WORK_HFLOORS          => ($work[WORK_HFLOORS]['count'] ?? $area),
                WORK_DRILL            => ($work[WORK_DRILL]['count'] ?? ''),
                WORK_RISER            => ($riser['size'] ?? ''),
                WORK_HIGHWAY          => '',
                WORK_COLLECTOR        => ($work[WORK_COLLECTOR]['count'] ?? 1),
                WORK_COLLECTOR_LOCKER => ($work[WORK_COLLECTOR_LOCKER]['count'] ?? 1),
                WORK_FJVR             => ($work[WORK_FJVR]['count'] ?? 1),
                WORK_ADDITIONAL       => ''
            ];

            $price = [
                WORK_HFLOORS          => ($work[WORK_HFLOORS]['price'] ?? 130),
                WORK_DRILL            => ($work[WORK_DRILL]['price'] ?? 80),
                WORK_RISER            => ($work[WORK_RISER]['price'] ?? 40),
                WORK_HIGHWAY          => ($work[WORK_HIGHWAY]['price'] ?? 40),
                WORK_COLLECTOR        => ($work[WORK_COLLECTOR]['price'] ?? 550),
                WORK_COLLECTOR_LOCKER => ($work[WORK_COLLECTOR_LOCKER]['price'] ?? 550),
                WORK_FJVR             => ($work[WORK_FJVR]['price'] ?? 250),
                WORK_ADDITIONAL       => ''
            ];

            $items[] = Items::get(WORK_HFLOORS)->setCount($count[WORK_HFLOORS])->setPrice($price[WORK_HFLOORS]);

            if (! isset($data['next'])) {
                $items[] = Items::get(WORK_COLLECTOR)->setCount($count[WORK_COLLECTOR])->setPrice($price[WORK_COLLECTOR]);
                $items[] = Items::get(WORK_COLLECTOR_LOCKER)->setCount($count[WORK_COLLECTOR_LOCKER])->setPrice($price[WORK_COLLECTOR_LOCKER]);
            } else {
                if ($fjvr > 0)
                    $items[] = Items::get(WORK_FJVR)->setCount($count[WORK_FJVR])->setPrice($price[WORK_FJVR]);
            }

            $items[] = Items::get(WORK_DRILL)->setPrice($price[WORK_DRILL]);

            if ($riser && $riser['size'])
                $items[] = Items::get(WORK_RISER)->setPrice($price[WORK_RISER])->setCount($count[WORK_RISER]);

            $items[] = Items::get(WORK_HIGHWAY)->setPrice($price[WORK_HIGHWAY]);
            $items[] = Items::get(WORK_ADDITIONAL);

            print View::render('hfloors/work' . (isset($data['next']) ? '_2' : ''), array_merge($data, compact('items', 'comment')));
        }
    }

    public function area_2()
    {
        $data = [
            'next' => 'area_2'
        ];

        return $this->area($data);
    }

    public function pipe_2()
    {
        $data = [
            'next' => 'pipe_2'
        ];

        return $this->pipe($data);
    }

    public function insulation_2()
    {
        $data = [
            'next' => 'insulation_2'
        ];

        return $this->insulation($data);
    }

    public function skin_2()
    {
        $data = [
            'next' => 'skin_2'
        ];

        return $this->skin($data);
    }

    public function insulation_join_2()
    {
        $data = [
            'next' => 'insulation_join_2'
        ];

        return $this->insulation_join($data);
    }

    public function clips_2()
    {
        $data = [
            'next' => 'clips_2'
        ];

        return $this->clips($data);
    }

    public function fjvr()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['fjvr'])) {
                Data::floor()::set('fjvr', $_POST['fjvr']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('fjvr.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->gerpex_2();
        } else {
            $comment = Data::floor()::get('fjvr.comment');
            $fjvr = Data::floor()::get('fjvr');

            print View::render('hfloors/fjvr', array_merge(compact('fjvr', 'comment')));
        }
    }

    public function gerpex_2()
    {
        $data = [
            'next' => 'gerpex_2'
        ];

        return $this->gerpex($data);
    }

    public function corner_fixes_2()
    {
        $data = [
            'next' => 'corner_fixes_2'
        ];

        return $this->corner_fixes($data);
    }

    public function locker_2()
    {
        $data = [
            'next' => 'locker_2'
        ];

        return $this->locker($data);
    }

    public function additional_2()
    {
        $data = [
            'next' => 'additional_2'
        ];

        return $this->additional($data);
    }

    public function work_2()
    {
        $data = [
            'next' => 'work_2'
        ];

        return $this->work($data);
    }
}
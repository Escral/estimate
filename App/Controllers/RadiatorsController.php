<?php

namespace App\Controllers;

use App\Data;
use App\Items;
use App\Models\Floor;
use App\Models\Item;
use App\Path;
use App\View;

class RadiatorsController extends Controller
{
    private $items;

    public function __construct()
    {
        parent::__construct();

        Data::$prefix2 = 'heating';

        $this->items = [
            Items::get(RADIATOR_STEAL_1),
            Items::get(RADIATOR_STEAL_2),
            Items::get(RADIATOR_ALLUM_3)
        ];
    }

    public function floors($page = 'radiators')
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['floors'])) {
                $floors = [];

                foreach ($_POST['floors'] as $floor => $heated) {
                    $floors[$floor] = $heated;
                }

                Data::set('floors', $floors);
                Data::set('floor', key($floors));
            }

            unset($_POST['submit']);
            $this->items();
        } else {
            $floors = Data::get('floors');

            if (! is_array($floors)) {
                $floors = Data::set('floors', [
                    0 => 1,
                    1 => 1
                ]);
            }

            $prev = null;
            foreach ($floors as $n => $floor) {
                if ($n == 100) {
                    $nextFloor = $prev + 1;
                    break;
                }
                $prev = $n;
            }

            print View::render('radiators/floors', compact('floors', 'nextFloor'));
        }
    }


    
    public function items()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                $items = (array) Data::floor()::get('items');

                foreach ($_POST['items'] as $key => $item) {
                    if ($item['count'] == 0) {
                        unset($items[$key]);
                    } else {
                        $items[$key]['count'] = $item['count'];
                        $items[$key]['sections'] = $item['sections'] ?? 1;
                    }
                }

                $items = Data::floor()::set('items', $items);

                if (isset($_POST['comment'])) {
                    Data::floor()::set('items.comment', $_POST['comment']);
                }

//                Items::afterAdd($items);
            }

            unset($_POST['submit']);
            $this->futorki();
        } else {
            $comment = Data::floor()::get('items.comment');

            $items = (array) Data::floor()::get('items');
            $selected = [];

            $n = 601;
            foreach ($items as $key => $item) {
                $selected[$key] = (new Item($n, $item['title'], $item['count'] ?? 0))->setSections($item['sections']);

                $n++;
            }

            print View::render('radiators/select', compact('selected', 'comment'));
        }
    }


    // public function items()
    // { // @todo Удаление радиаторов кнопкой
    //     if (isset($_POST['submit'])) {
    //         if (isset($_POST['items'])) {
    //             foreach ($_POST['items'] as $key => $item) {
    //                 if ($item['count'] == 0) {
    //                     unset($_POST['items'][$key]);
    //                 }
    //             }

    //             $items = Data::floor()::set('items', $_POST['items']);

    //             Items::afterAdd($items);
    //         }

    //         unset($_POST['submit']);
    //         $this->futorki();
    //     } else {
    //         while (! Floor::withRadiators()) {
    //             if (! Floor::setNext())
    //                 break;
    //         }

    //         $items = $this->items;
    //         $arr = (array) Data::floor()::get('items');
    //         $selected = [];

    //         foreach ($arr as $key => $item) {
    //             $ID = $item['item'];
    //             $selected[$key] = Items::get($ID)->setCount(($item['count'] ?? 0))->setSections(($item['sections'] ?? 0));

    //             if ($item == null)
    //                 unset($selected[$key]);
    //         }

    //         print View::render('radiators/select', compact('items', 'selected'));
    //     }
    // }

    public function add_items()
    {
        $items = Data::floor()::get('items');

        if (isset($_POST['items'])) {
            foreach ($_POST['items'] as $item) {
                $data = [
                    'count' => 1,
                    'item' => $item,
                    'sections' => 1
                ];

                $items[] = $data;
            }
        }

        Data::floor()::set('items', $items);

        unset($_POST['submit']);
        $this->items();
    }


    public function add_radiators()
    {
        $items = Data::floor()::get('items');

        if (isset($_POST['item'])) {
            $data = [
                'count' => 1,
                'item' => 0,
                'title' => $_POST['item'],
                'sections' => isset($_POST['sections']) ? $_POST['sections'] : 0,
            ];

            $f = false;
            if (is_array($items)) {
                foreach ($items as &$item) {
                    if ($item['title'] == $_POST['item'] and (! isset($item['sections']) or $item['sections'] == $_POST['sections'])) {
                        $item['count'] += 1;
                        $f = true;
                        break;
                    }
                }
            }

            if (! $f)
                $items[] = $data;
        }

        Data::floor()::set('items', $items);

        unset($_POST['submit']);
        $this->items();
    }

    public function remove_radiators()
    {
        $items = Data::floor()::get('items');

        if (isset($_POST['key'])) {
            if (isset($items[$_POST['key']]))
                unset($items[$_POST['key']]);
        }

        Data::floor()::set('items', $items);

        $this->items();
    }




    public function futorki()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('futorki', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('futorki.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->legs();
        } else {

            $comment = Data::floor()::get('futorki.comment');
            $futorki = Data::floor()::get('futorki');

            $items = [
                Items::get(FUTORKA_1)->setCount($futorki[FUTORKA_1] ?? Items::getAlluminumRadiatorsCount())
            ];

            print View::render('radiators/futorki', compact('items', 'comment'));
        }
    }


    public function legs()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('legs', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('legs.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->scheme();
        } else {
            $comment = Data::floor()::get('legs.comment');

            $legs = Data::floor()::get('legs');

            $items = [];

            if (Items::stealRadiatorsSelected())
                $items[] = Items::get(LEGS_STEEL)->setCount(($legs[LEGS_STEEL] ?? 0));

            if (Items::aluminumRadiatorsSelected())
                $items[] = Items::get(LEGS_ALUMINUM)->setCount(($legs[LEGS_ALUMINUM] ?? 0));

            print View::render('radiators/legs', compact('items', 'comment'));
        }
    }

    public function scheme()
    {
        $scheme = $schemeWas = Data::floor()::get('scheme');

        if (isset($_POST['submit'])) {
            if (isset($_POST['scheme'])) {
                $scheme = Data::floor()::set('scheme', $_POST['scheme']);
            }

            if ($schemeWas and $schemeWas != $scheme) {
                if ($schemeWas == 1)
                    Path::forget('duopipe');
                elseif ($schemeWas == 2)
                    Path::forget('beam');
            }

            // Двухтрубная
            if ($scheme == 1) {
                unset($_POST['submit']);
                $this->duopipe();
            } else if ($scheme == 2) {
                unset($_POST['submit']);
                $this->beam();
            }
        } else {
            print View::render('radiators/scheme', compact('scheme'));
        }
    }

    public function duopipe()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('pipes', $_POST['items']);
            }

            if (isset($_POST['radiators'])) {
                Data::floor()::set('radiators_on_pipes', $_POST['radiators']);
            }

            if (isset($_POST['riser'])) {
                Data::floor()::set('riser', $_POST['riser']);
            }

            if (isset($_POST['supply'])) {
                Data::floor()::set('supply', $_POST['supply']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('pipes.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->insulation();
        } else {

            $comment = Data::floor()::get('pipes.comment');
            $pipes = Data::floor()::get('pipes');
            $on_pipes = Data::floor()::get('radiators_on_pipes');
            $riser = Data::floor()::get('riser');
            $supply = Data::floor()::get('supply');

            $items = [
                Items::get(PIPE_20)->setCount(($pipes[PIPE_20] ?? null)),
                Items::get(PIPE_25)->setCount(($pipes[PIPE_25] ?? null)),
                Items::get(PIPE_32)->setCount(($pipes[PIPE_32] ?? null)),
            ];

            $length = Items::getLengthPodvodki('radiators');

            $radiatorsCount = Items::getRadiatorsCount();

            if (! $riser['item']) {
                $riser['item'] = PIPE_32;
            }

            print View::render('radiators/duopipe', compact('items', 'on_pipes', 'comment', 'riser', 'length', 'supply', 'radiatorsCount'));
        }
    }

    public function beam()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('pipes', $_POST['items']);
            }

            if (isset($_POST['riser'])) {
                Data::floor()::set('riser', $_POST['riser']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('pipes.comment', $_POST['comment']);
            }

            if ($_POST['riser']['size'] == 0) {
                unset($_POST['submit']);
                $this->collector();
                return;
            }

            unset($_POST['submit']);
            $this->insulation_riser();
        } else {
            $comment = Data::floor()::get('pipes.comment');
            $pipes = Data::floor()::get('pipes');
            $riser = Data::floor()::get('riser');

            $items = [
                Items::get(PIPE_16)->setCount(($pipes[PIPE_16] ?? '')),
            ];

            $ppr = [
                Items::get(PIPE_25)->setCount($pipes[PIPE_25] ?? null),
                Items::get(PIPE_32)->setCount($pipes[PIPE_32] ?? null),
            ];
            
            if (! $riser['item']) {
                $riser['item'] = PIPE_32;
            }

            print View::render('radiators/beam', compact('items', 'ppr', 'riser', 'comment'));
        }
    }

    public function insulation_riser()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('insulation_riser', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('insulation_riser.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->corners_riser();
        } else {
            $comment = Data::floor()::get('insulation_riser.comment');

            $riser = Data::floor()::get('riser');
            $insulation = Data::floor()::get('insulation_riser');

            $count = [
                INSULATION_25 => $insulation[INSULATION_25] ?? ((int) $riser['item'] == PIPE_25 ? (int) $riser['size'] : 0),
                INSULATION_32 => $insulation[INSULATION_32] ?? ((int) $riser['item'] == PIPE_32 ? (int) $riser['size'] : 0),
            ];

            $items = [
                PIPE_25 => Items::get(INSULATION_25)->setCount($count[INSULATION_25]),
                PIPE_32 => Items::get(INSULATION_32)->setCount($count[INSULATION_32]),
            ];

            print View::render('radiators/insulation_riser', compact('items', 'comment'));
        }
    }

    public function corners_riser()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('corners_riser', $_POST['items']);
            }



            if (isset($_POST['comment'])) {
                Data::floor()::set('corners_riser.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->couplings_riser();
        } else {
            $comment = Data::floor()::get('corners_riser.comment');
            $riser = Data::floor()::get('riser');
            $corners = Data::floor()::get('corners_riser');

            $pipes = [
                PIPE_25 => 0,
                PIPE_32 => 0
            ];

            if ($riser['item'] == PIPE_25) {
                $pipes[PIPE_25] = $riser['size'];
            }
            if ($riser['item'] == PIPE_32) {
                $pipes[PIPE_32] = $riser['size'];
            }

            $count = [
                CORNER_90_25 => $corners[CORNER_90_25] ?? ceil(max(ceil(((int) $pipes[PIPE_25] * 0.4) / 2) * 2, ($pipes[PIPE_25] ? 6 : ''))),
                CORNER_90_32 => $corners[CORNER_90_32] ?? ceil(max(ceil(((int) $pipes[PIPE_32] * 0.4) / 2) * 2, ($pipes[PIPE_32] ? 6 : ''))),
                CORNER_45_25 => $corners[CORNER_45_25] ?? ceil(ceil(((int) $pipes[PIPE_25] * 0.2) / 2) * 2),
                CORNER_45_32 => $corners[CORNER_45_32] ?? ceil(ceil(((int) $pipes[PIPE_32] * 0.2) / 2) * 2),
            ];

            $items90 = [
                Items::get(CORNER_90_25)->setCount($count[CORNER_90_25]),
                Items::get(CORNER_90_32)->setCount($count[CORNER_90_32])
            ];

            $items45 = [
                Items::get(CORNER_45_25)->setCount($count[CORNER_45_25]),
                Items::get(CORNER_45_32)->setCount($count[CORNER_45_32])
            ];

            print View::render('radiators/corners_riser', compact('items90', 'items45', 'comment'));
        }
    }

    public function couplings_riser()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('couplings_riser', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('couplings_riser.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->collector();
        } else {
            $comment = Data::floor()::get('couplings_riser.comment');
            $riser = Data::floor()::get('riser');
            $couplings = Data::floor()::get('couplings_riser');

            $pipes = [
                PIPE_25 => 0,
                PIPE_32 => 0
            ];

            if ($riser['item'] == PIPE_25) {
                $pipes[PIPE_25] = $riser['size'];
            }
            if ($riser['item'] == PIPE_32) {
                $pipes[PIPE_32] = $riser['size'];
            }


            $count = [
                COUPLING_25 => $couplings[COUPLING_25] ?? (ceil((int) $pipes[PIPE_25] * 0.25 + ($pipes[PIPE_25] ? 2 : 0))),
                COUPLING_32 => $couplings[COUPLING_32] ?? (ceil((int) $pipes[PIPE_32] * 0.25 + ($pipes[PIPE_32] ? 2 : 0))),
            ];

            $items = [
                Items::get(COUPLING_25)->setCount($count[COUPLING_25]),
                Items::get(COUPLING_32)->setCount($count[COUPLING_32])
            ];

            print View::render('radiators/couplings_riser', compact('items', 'comment'));
        }
    }

    public function collector()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['collector'])) {
                Data::floor()::set('collector', $_POST['collector']);
            }

            if (isset($_POST['flowmeter'])) {
                Data::floor()::set('flowmeter', $_POST['flowmeter']);
            }

            if (isset($_POST['tailstocks'])) {
                Data::floor()::set('tailstocks', $_POST['tailstocks']);
            }

            if (isset($_POST['col_valves'])) {
                Data::floor()::set('col_valves', $_POST['col_valves']);
            }

            if (isset($_POST['mp'])) {
                Data::floor()::set('col_mp', $_POST['mp']);
            }

            if (isset($_POST['col_corners'])) {
                Data::floor()::set('col_corners', $_POST['col_corners']);
            }


            if (isset($_POST['comment'])) {
                Data::floor()::set('collector.comment', $_POST['comment']);
            }


            unset($_POST['submit']);
            $this->gerpex();
        } else {
            $count = min(12, max(3, Items::getRadiatorsCount()));

            $collector = Data::floor()::get('collector');
            $flowmeter = Data::floor()::get('flowmeter');
            $tailstocks = Data::floor()::get('tailstocks');
            $col_valves = Data::floor()::get('col_valves');
            $mp = Data::floor()::get('col_mp');
            $col_corners = Data::floor()::get('col_corners');
            $use_tailstocks = Data::floor()::get('use_tailstocks');
            $use_valves = Data::floor()::get('use_valves');
            $use_mp = Data::floor()::get('use_mp');
            $use_col_corners = Data::floor()::get('use_col_corners');

            $comment = Data::floor()::get('collector.comment');

            print View::render('radiators/collector', compact('count', 'collector', 'flowmeter', 'tailstocks', 'col_valves', 'use_tailstocks', 'use_valves', 'use_mp', 'mp', 'use_col_corners', 'col_corners', 'comment'));
        }
    }

    public function gerpex()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['gerpex'])) {
                Data::floor()::set('gerpex', $_POST['gerpex']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('gerpex.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->locker();
        } else {
            $comment = Data::floor()::get('gerpex.comment');
            $count = (int) Data::floor()::get('collector') * 2;
            $gerpex = Data::floor()::get('gerpex');

            print View::render('radiators/gerpex', compact('count', 'gerpex', 'comment'));
        }
    }

    public function locker()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['locker'])) {
                Data::floor()::set('locker', $_POST['locker']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('locker.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->valves_metal();
        } else {
            $comment = Data::floor()::get('locker.comment');
            $size = (int) Data::floor()::get('collector') * 5 + 35;
            $locker = Data::floor()::get('locker');

            print View::render('radiators/locker', compact('size', 'locker', 'comment'));
        }
    }

    public function valves_metal()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('valves', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('valves_metal.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->gerpex_valves();
        } else {
            $comment = Data::floor()::get('valves_metal.comment');
            $valves = Data::floor()::get('valves');

            $items = [
                Items::get(VALVE_CORNER_IN)->setCount($valves[VALVE_CORNER_IN]),
                Items::get(VALVE_CORNER_OUT)->setCount($valves[VALVE_CORNER_OUT]),
                Items::get(VALVE_STRAIGHT_IN)->setCount($valves[VALVE_STRAIGHT_IN]),
                Items::get(VALVE_STRAIGHT_OUT)->setCount($valves[VALVE_STRAIGHT_OUT]),
                Items::get(VALVE_TERMO_CORNER_IN)->setCount($valves[VALVE_TERMO_CORNER_IN]),
                Items::get(VALVE_TERMO_CORNER_OUT)->setCount($valves[VALVE_TERMO_CORNER_OUT]),
                Items::get(VALVE_TERMO_STRAIGHT_IN)->setCount($valves[VALVE_TERMO_STRAIGHT_IN]),
                Items::get(VALVE_TERMO_STRAIGHT_OUT)->setCount($valves[VALVE_TERMO_STRAIGHT_OUT]),
            ];

            $maxValves = Items::getRadiatorsCount() * 2;

            print View::render('radiators/valves_metal', compact('items', 'maxValves', 'comment'));
        }
    }

    public function gerpex_valves()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['gerpex_valves'])) {
                Data::floor()::set('gerpex_valves', $_POST['gerpex_valves']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('geprex_valves.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->additional_beam();
        } else {
            $comment = Data::floor()::get('geprex_valves.comment');
            $count = array_sum((array) Data::floor()::get('valves'));
            $gerpex_valves = Data::floor()::get('gerpex_valves');

            print View::render('radiators/gerpex_valves', compact('count', 'gerpex_valves', 'comment'));
        }
    }

    public function additional_beam()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['additional_beam'])) {
                Data::floor()::set('additional_beam', $_POST['additional_beam']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('additional_beam.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->work_beam();
        } else {
            $comment = Data::floor()::get('additional_beam.comment');
            $additional = Data::floor()::get('additional_beam');

            $riser = Data::floor()::get('riser');

            $count = [
                ADDITIONAL_SCREWS       => $additional[ADDITIONAL_SCREWS] ?? (array_sum((array) Data::floor()::get('legs')) * 8 + 4),
                ADDITIONAL_WIRE         => $additional[ADDITIONAL_WIRE] ?? 1,
                ADDITIONAL_FOAM         => $additional[ADDITIONAL_FOAM] ?? ((bool) Data::floor()::get('locker') ? 1 : ''),
                ADDITIONAL_CORNER_FIXES => $additional[ADDITIONAL_CORNER_FIXES] ?? Items::getRadiatorsCount() * 2,
                ADDITIONAL_CLAMPING_1   => $additional[ADDITIONAL_CLAMPING_1] ?? ($riser['item'] == PIPE_20 ? $riser['size'] : ''),
                ADDITIONAL_CLAMPING_2   => $additional[ADDITIONAL_CLAMPING_2] ?? ($riser['item'] == PIPE_25 ? $riser['size'] : ''),
                ADDITIONAL_CLAMPING_3   => $additional[ADDITIONAL_CLAMPING_3] ?? ($riser['item'] == PIPE_32 ? $riser['size'] : ''),
                ADDITIONAL_SELF_SCREWS  => $additional[ADDITIONAL_SELF_SCREWS] ?? ''
            ];

            $items = [
                Items::get(ADDITIONAL_SCREWS)->setCount($count[ADDITIONAL_SCREWS]),
                Items::get(ADDITIONAL_WIRE)->setCount($count[ADDITIONAL_WIRE]),
                Items::get(ADDITIONAL_FOAM)->setCount($count[ADDITIONAL_FOAM]),
                Items::get(ADDITIONAL_CORNER_FIXES)->setCount($count[ADDITIONAL_CORNER_FIXES]),
                Items::get(ADDITIONAL_CLAMPING_1)->setCount($count[ADDITIONAL_CLAMPING_1]),
                Items::get(ADDITIONAL_CLAMPING_2)->setCount($count[ADDITIONAL_CLAMPING_2]),
                Items::get(ADDITIONAL_CLAMPING_3)->setCount($count[ADDITIONAL_CLAMPING_3]),
                Items::get(ADDITIONAL_SELF_SCREWS)->setCount($count[ADDITIONAL_SELF_SCREWS])
            ];

            print View::render('radiators/additional_beam', compact('additional', 'items', 'comment'));
        }
    }

    public function work_beam()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('work_beam', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('work_beam.comment', $_POST['comment']);
            }

            unset($_POST['submit']);

            Alerts::add("Вы закончили редактирование этажа");
            (new FloorsController())->floors();
        } else {
            $comment = Data::floor()::get('work_beam.comment');
            $riser = Data::floor()::get('riser');
            $work = Data::floor()::get('work');

            $count = [
                WORK_RADIATORS => ($work[WORK_RADIATORS]['count'] ?? Items::getRadiatorsCount()),
                WORK_DRILL => ($work[WORK_DRILL]['count'] ?? Items::getRadiatorsCount()),
                WORK_RISER => ($riser['size'] ?? ''),
                WORK_HIGHWAY => '',
                WORK_COLLECTOR => ($work[WORK_COLLECTOR]['count'] ?? 1),
                WORK_COLLECTOR_LOCKER => ($work[WORK_COLLECTOR_LOCKER]['count'] ?? 1),
                WORK_ADDITIONAL => ''
            ];

            $price = [
                WORK_RADIATORS => ($work[WORK_RADIATORS]['price'] ?? 550),
                WORK_DRILL => ($work[WORK_DRILL]['price'] ?? 80),
                WORK_RISER => ($work[WORK_RISER]['price'] ?? 40),
                WORK_HIGHWAY => ($work[WORK_HIGHWAY]['price'] ?? 40),
                WORK_COLLECTOR => ($work[WORK_COLLECTOR]['price'] ?? 550),
                WORK_COLLECTOR_LOCKER => ($work[WORK_COLLECTOR_LOCKER]['price'] ?? 550),
                WORK_ADDITIONAL => ''
            ];

            $items[] = Items::get(WORK_RADIATORS)->setCount($count[WORK_RADIATORS])->setPrice($price[WORK_RADIATORS]);
            $items[] = Items::get(WORK_DRILL)->setCount($count[WORK_DRILL])->setPrice($price[WORK_DRILL]);

            if ($riser && $riser['size'])
                $items[] = Items::get(WORK_RISER)->setCount($count[WORK_RISER])->setPrice($price[WORK_RISER]);

            $items[] = Items::get(WORK_HIGHWAY)->setPrice($price[WORK_HIGHWAY]);
            $items[] = Items::get(WORK_COLLECTOR)->setPrice($price[WORK_COLLECTOR])->setCount($count[WORK_COLLECTOR]);

            $locker = Data::floor()::get('locker');
            if ($locker and $locker['isset'])
                $items[] = Items::get(WORK_COLLECTOR_LOCKER)->setPrice($price[WORK_COLLECTOR_LOCKER])->setCount($count[WORK_COLLECTOR_LOCKER]);

            $items[] = Items::get(WORK_ADDITIONAL);

            print View::render('radiators/work_beam', compact('items', 'comment'));
        }
    }

    public function insulation()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('insulation', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('insulation.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->corners();
        } else {
            $items = Data::floor()::get('pipes');
            $insulation = Data::floor()::get('insulation');
            $comment = Data::floor()::get('insulation.comment');

            // @todo Добавлять к 20 трубе количество радиаторов + учитывать изоляцию

            $radiatorsCount = Items::getRadiatorsCount();

            $count = [
                INSULATION_20 => $insulation[INSULATION_20] ?? (isset($items) ? ceil((int) $items[PIPE_20] * 1 + $radiatorsCount) : 0),
                INSULATION_25 => $insulation[INSULATION_25] ?? (isset($items) ? ceil((int) $items[PIPE_25] * 1) : 0),
                INSULATION_32 => $insulation[INSULATION_32] ?? (isset($items) ? ceil((int) $items[PIPE_32] * 1) : 0),
            ];

            $items = [
                PIPE_20 => Items::get(INSULATION_20)->setCount($count[INSULATION_20]),
                PIPE_25 => Items::get(INSULATION_25)->setCount($count[INSULATION_25]),
                PIPE_32 => Items::get(INSULATION_32)->setCount($count[INSULATION_32]),
            ];

            $riser = Data::floor()::get('riser');

            if ($riser) {
                if ($riser['size'] > 0) {
                    $items[$riser['item']]->addCount($riser['size']);
                }
            }

            // Сохранить комментарий сколько метров на стояк

            print View::render('radiators/insulation', compact('items', 'comment'));
        }
    }

    public function corners()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('corners', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('corners.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->couplings();
        } else {
            $comment = Data::floor()::get('corners.comment');
            $pipes = Data::floor()::get('pipes');
            $corners = Data::floor()::get('corners');
            $radiatorsCount = Items::getRadiatorsCount();

            // Минимум 10
            $count = [
                CORNER_90_20 => $corners[CORNER_90_20] ?? max((ceil(((int) $pipes[PIPE_20] * 0.45 + $radiatorsCount * 4) / 2) * 2), $pipes[PIPE_20] ? 10 : ''),
                CORNER_90_25 => $corners[CORNER_90_25] ?? max((ceil(((int) $pipes[PIPE_25] * 0.6) / 2) * 2), $pipes[PIPE_25] ? 10 : ''),
                CORNER_90_32 => $corners[CORNER_90_32] ?? max((ceil(((int) $pipes[PIPE_32] * 0.25) / 2) * 2), $pipes[PIPE_32] ? 10 : ''),
                CORNER_45_20 => $corners[CORNER_45_20] ?? max((ceil(((int) $pipes[PIPE_20] * 0.3 + $radiatorsCount * 2) / 2) * 2), $pipes[PIPE_20] ? 10 : ''),
                CORNER_45_25 => $corners[CORNER_45_25] ?? max((ceil(((int) $pipes[PIPE_25] * 0.2) / 2) * 2), $pipes[PIPE_25] ? 10 : ''),
                CORNER_45_32 => $corners[CORNER_45_32] ?? max((ceil(((int) $pipes[PIPE_32] * 0.15) / 2) * 2), $pipes[PIPE_32] ? 10 : ''),
            ];

            $items90 = [
                Items::get(CORNER_90_20)->setCount($count[CORNER_90_20]),
                Items::get(CORNER_90_25)->setCount($count[CORNER_90_25]),
                Items::get(CORNER_90_32)->setCount($count[CORNER_90_32])
            ];

            $items45 = [
                Items::get(CORNER_45_20)->setCount($count[CORNER_45_20]),
                Items::get(CORNER_45_25)->setCount($count[CORNER_45_25]),
                Items::get(CORNER_45_32)->setCount($count[CORNER_45_32])
            ];

            print View::render('radiators/corners', compact('items90', 'items45', 'comment'));
        }
    }

    public function couplings()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('couplings', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('couplings.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->transitions();
        } else {
            $comment = Data::floor()::get('couplings.comment');
            $pipes = Data::floor()::get('pipes');
            $couplings = Data::floor()::get('couplings');

            $count = [
                COUPLING_20 => $couplings[COUPLING_20] ?? (ceil((int) $pipes[PIPE_20] * 1.5 * 0.25)),
                COUPLING_25 => $couplings[COUPLING_25] ?? (ceil((int) $pipes[PIPE_25] * 1.5 * 0.25)),
                COUPLING_32 => $couplings[COUPLING_32] ?? (ceil((int) $pipes[PIPE_32] * 1.5 * 0.25)),
            ];

            $items = [
                Items::get(COUPLING_20)->setCount($count[COUPLING_20]),
                Items::get(COUPLING_25)->setCount($count[COUPLING_25]),
                Items::get(COUPLING_32)->setCount($count[COUPLING_32])
            ];

            print View::render('radiators/couplings', compact('items', 'comment'));
        }
    }

    public function transitions()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('transitions', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('transitions.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->tees();
        } else {
            $comment = Data::floor()::get('transitions.comment');
            $pipes = Data::floor()::get('pipes');
            $transitions = Data::floor()::get('transitions');

            $count = [
                TRANSITION_25_20 => $transitions[TRANSITION_25_20] ?? (((int) $pipes[PIPE_20] ? max(ceil($pipes[PIPE_20] * 0.3), 8) : 0)),
                TRANSITION_32_25 => $transitions[TRANSITION_32_25] ?? (((int) $pipes[PIPE_25] ? max(ceil($pipes[PIPE_25] * 0.2), 6) : 0)),
                TRANSITION_32_20 => $transitions[TRANSITION_32_20] ?? (((int) $pipes[PIPE_32] ? max(ceil($pipes[PIPE_32] * 0.15), 4) : 0)),
            ];

            $items = [
                Items::get(TRANSITION_25_20)->setCount($count[TRANSITION_25_20]),
                Items::get(TRANSITION_32_25)->setCount($count[TRANSITION_32_25]),
                Items::get(TRANSITION_32_20)->setCount($count[TRANSITION_32_20])
            ];

            print View::render('radiators/transitions', compact('items', 'comment'));
        }
    }

    public function tees()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('tees', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('tees.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->valves();
        } else {
            $comment = Data::floor()::get('tees.comment');
            $radiators = Data::floor()::get('radiators_on_pipes');
            $pipes = Data::floor()::get('pipes');
            $tees = Data::floor()::get('tees');

            $count = [
                TEE_20 => $tees[TEE_20] ?? ceil((int) $radiators[PIPE_20] * 2 * 1.3),
                TEE_25 => $tees[TEE_25] ?? (((int) $pipes[PIPE_25] ? min(ceil($pipes[PIPE_25] * 0.5), 10) : 0)),
                TEE_32 => $tees[TEE_32] ?? (((int) $pipes[PIPE_32] ? min(ceil($pipes[PIPE_25] * 0.5), 10) : 0)),
                TEE_25_20_25 => $tees[TEE_25_20_25] ?? ((int) $radiators[PIPE_25] * 2),
                TEE_25_20_20 => $tees[TEE_25_20_20] ?? (((int) $pipes[PIPE_25] ? $radiators[PIPE_20] * 2 : 0)),
                TEE_32_20_32 => $tees[TEE_32_20_32] ?? ((int) $radiators[PIPE_32] * 2),
                TEE_32_25_32 => $tees[TEE_32_25_32] ?? (((int) $pipes[PIPE_32] ? min(ceil($pipes[PIPE_32] * 0.5), 8) : 0)),
            ];

            $items = [
                Items::get(TEE_20)->setCount($count[TEE_20]),
                Items::get(TEE_25)->setCount($count[TEE_25]),
                Items::get(TEE_32)->setCount($count[TEE_32]),
                Items::get(TEE_25_20_25)->setCount($count[TEE_25_20_25]),
                Items::get(TEE_25_20_20)->setCount($count[TEE_25_20_20]),
                Items::get(TEE_32_20_32)->setCount($count[TEE_32_20_32]),
                Items::get(TEE_32_25_32)->setCount($count[TEE_32_25_32])
            ];

            print View::render('radiators/tees', compact('items', 'comment'));
        }
    }

    public function valves()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('valves', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('valves.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->MP();
        } else {
            $comment = Data::floor()::get('valves.comment');
            $valves = Data::floor()::get('valves');

            $items = [
                Items::get(VALVE_CORNER_IN)->setCount(($valves[VALVE_CORNER_IN] ?? '')),
                Items::get(VALVE_CORNER_OUT)->setCount(($valves[VALVE_CORNER_OUT] ?? '')),
                Items::get(VALVE_STRAIGHT_IN)->setCount(($valves[VALVE_STRAIGHT_IN] ?? '')),
                Items::get(VALVE_STRAIGHT_OUT)->setCount(($valves[VALVE_STRAIGHT_OUT] ?? '')),
                Items::get(VALVE_TERMO_CORNER_IN)->setCount(($valves[VALVE_TERMO_CORNER_IN] ?? '')),
                Items::get(VALVE_TERMO_CORNER_OUT)->setCount(($valves[VALVE_TERMO_CORNER_OUT] ?? '')),
                Items::get(VALVE_TERMO_STRAIGHT_IN)->setCount(($valves[VALVE_TERMO_STRAIGHT_IN] ?? '')),
                Items::get(VALVE_TERMO_STRAIGHT_OUT)->setCount(($valves[VALVE_TERMO_STRAIGHT_OUT] ?? '')),
            ];

            $maxValves = Items::getRadiatorsCount() * 2;

            print View::render('radiators/valves', compact('items', 'maxValves', 'comment'));
        }
    }

    public function MP()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('MP', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('MP.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->additional();
        } else {
            $comment = Data::floor()::get('MP.comment');
            $valves = Data::floor()::get('valves');
            $MP = Data::floor()::get('MP');

            $count = 0;
            if (is_array($valves)) {
                foreach ($valves as $valve) {
                    $count += (int) $valve;
                }
            }

            $items = [
                Items::get(MP_20_1_2_FATHER)->setCount($MP[MP_20_1_2_FATHER] ?? $count),
            ];

            print View::render('radiators/MP', compact('items', 'comment'));
        }
    }

    public function additional()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('additional', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('additional.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->work();
        } else {
            $comment = Data::floor()::get('additional.comment');
            $additional = Data::floor()::get('additional');

            $riser = Data::floor()::get('riser');

            $count = [
                ADDITIONAL_SCREWS      => (array_sum((array) Data::floor()::get('legs')) * 8),
                ADDITIONAL_CLAMPING_1  => ($riser['item'] == PIPE_20 ? $riser['size'] : ''),
                ADDITIONAL_CLAMPING_2  => ($riser['item'] == PIPE_25 ? $riser['size'] : ''),
                ADDITIONAL_CLAMPING_3  => ($riser['item'] == PIPE_32 ? $riser['size'] : ''),
                ADDITIONAL_SELF_SCREWS => 0
            ];

            $items = [
                Items::get(ADDITIONAL_SCREWS)->setCount($count[ADDITIONAL_SCREWS]),
                Items::get(ADDITIONAL_CLAMPING_1)->setCount($count[ADDITIONAL_CLAMPING_1]),
                Items::get(ADDITIONAL_CLAMPING_2)->setCount($count[ADDITIONAL_CLAMPING_2]),
                Items::get(ADDITIONAL_CLAMPING_3)->setCount($count[ADDITIONAL_CLAMPING_3]),
                Items::get(ADDITIONAL_SELF_SCREWS)->setCount($count[ADDITIONAL_SELF_SCREWS])
            ];

            print View::render('radiators/additional', compact('additional', 'items', 'comment'));
        }
    }

    public function work()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('work', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('work.comment', $_POST['comment']);
            }

            unset($_POST['submit']);

            Alerts::add("Вы закончили редактирование этажа");
            (new FloorsController())->floors();
        } else {
            $comment = Data::floor()::get('work.comment');
            $riser = Data::floor()::get('riser');

            $work = Data::floor()::get('work');

            $count = [
                WORK_RADIATORS => ($work[WORK_RADIATORS]['count'] ?? Items::getRadiatorsCount()),
                WORK_DRILL => ($work[WORK_DRILL]['count'] ?? Items::getRadiatorsCount()),
                WORK_RISER => ($riser['size'] ?? ''),
                WORK_HIGHWAY => '',
                WORK_COLLECTOR => ($work[WORK_COLLECTOR]['count'] ?? 1),
                WORK_COLLECTOR_LOCKER => ($work[WORK_COLLECTOR_LOCKER]['count'] ?? 1),
                WORK_ADDITIONAL => ($work[WORK_ADDITIONAL]['count'] ?? 1),
            ];

            $price = [
                WORK_RADIATORS => ($work[WORK_RADIATORS]['price'] ?? 550),
                WORK_DRILL => ($work[WORK_DRILL]['price'] ?? 80),
                WORK_RISER => ($work[WORK_RISER]['price'] ?? 40),
                WORK_HIGHWAY => ($work[WORK_HIGHWAY]['price'] ?? 40),
                WORK_COLLECTOR => ($work[WORK_COLLECTOR]['price'] ?? 550),
                WORK_COLLECTOR_LOCKER => ($work[WORK_COLLECTOR_LOCKER]['price'] ?? 550),
                WORK_ADDITIONAL => ($work[WORK_ADDITIONAL]['price'] ?? ''),
            ];

            $items[] = Items::get(WORK_RADIATORS)->setCount($count[WORK_RADIATORS])->setPrice($price[WORK_RADIATORS]);
            $items[] = Items::get(WORK_DRILL)->setCount($count[WORK_DRILL])->setPrice($price[WORK_DRILL]);

            if ($riser && $riser['size'])
                $items[] = Items::get(WORK_RISER)->setCount($count[WORK_RISER])->setPrice($price[WORK_RISER]);

            $items[] = Items::get(WORK_HIGHWAY)->setPrice($price[WORK_HIGHWAY]);
            $items[] = Items::get(WORK_ADDITIONAL)->setPrice($price[WORK_ADDITIONAL]);

            print View::render('radiators/work', compact('items', 'comment'));
        }
    }
}
<?php

namespace App\Controllers;

use App\Data;
use App\Items;
use App\Path;
use App\View;

class ToolsController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        Data::$prefix2 = 'tools';
    }

    public function items()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                $_POST['items'] = array_filter($_POST['items'], function($value) { return $value !== ''; });

                Data::set('items', $_POST['items']);
            }

            unset($_POST['submit']);

            Path::remember('tools');

            Alerts::add("Вы закончили редактирование этажа");
            (new FloorsController())->floors();
        } else {
            $selected = Data::get('items');

            $items = Items::getToolsItems($selected);

            print View::render('tools/items', compact('items'));
        }
    }
}
<?php

namespace App\Controllers;

use App\Tasks;

class TasksController
{
    public function execute($task)
    {
        if (isset($task) and $task = (new Tasks())->find($task)) {
            $task->execute();
        }
    }
}
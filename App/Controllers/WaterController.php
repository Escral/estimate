<?php

namespace App\Controllers;

use App\Data;
use App\Items;
use App\Models\Floor;
use App\Models\Item;
use App\Path;
use App\View;

class WaterController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        Data::$prefix2 = 'water';
    }

    public function scheme()
    {
        $scheme = $schemeWas = Data::floor()::get('scheme');

        if (isset($_POST['submit'])) {
            if (isset($_POST['scheme'])) {
                $scheme = Data::floor()::set('scheme', $_POST['scheme']);
            }


//            if ($schemeWas and $schemeWas != $scheme)
//                Path::forget('scheme');

            // Двухтрубная
            if ($scheme == 1) {
                unset($_POST['submit']);
                $this->items();
            } else if ($scheme == 2) {
                unset($_POST['submit']);
//                $this->items();
            }
        } else {
            print View::render('water/scheme', compact('scheme'));
        }
    }

    public function items()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('items', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('items.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->pipe();
        } else {

            $comment = Data::floor()::get('items.comment');
            $selected = Data::floor()::get('items');

            $items = [
                Items::get(PRIBOR_1)->setCount($selected[PRIBOR_1] ?? ''),
                Items::get(PRIBOR_2)->setCount($selected[PRIBOR_2] ?? ''),
                Items::get(PRIBOR_3)->setCount($selected[PRIBOR_3] ?? ''),
                Items::get(PRIBOR_4)->setCount($selected[PRIBOR_4] ?? ''),
                Items::get(PRIBOR_10)->setCount($selected[PRIBOR_10] ?? ''),
                Items::get(PRIBOR_11)->setCount($selected[PRIBOR_11] ?? ''),
                Items::get(PRIBOR_5)->setCount($selected[PRIBOR_5] ?? ''),
                Items::get(PRIBOR_6)->setCount($selected[PRIBOR_6] ?? ''),
                Items::get(PRIBOR_7)->setCount($selected[PRIBOR_7] ?? ''),
                Items::get(PRIBOR_8)->setCount($selected[PRIBOR_8] ?? ''),
                Items::get(PRIBOR_9)->setCount($selected[PRIBOR_9] ?? ''),
            ];

            print View::render('water/items', compact('items', 'comment'));
        }
    }

    public function pipe()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('pipes', $_POST['items']);
            }

            if (isset($_POST['water'])) {
                Data::floor()::set('water_on_pipes', $_POST['water']);
            }

            if (isset($_POST['riser'])) {
                Data::floor()::set('riser', $_POST['riser']);
            }

            if (isset($_POST['supply'])) {
                Data::floor()::set('supply', $_POST['supply']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('pipes.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->insulation();
        } else {
            $comment = Data::floor()::get('pipes.comment');
            $pipes = Data::floor()::get('pipes');
            $on_pipes = Data::floor()::get('water_on_pipes');
            $riser = Data::floor()::get('riser');
            $supply = Data::floor()::get('supply');

            $items = [
                Items::get(PIPE_20)->setCount(($pipes[PIPE_20] ?? null)),
                Items::get(PIPE_25)->setCount(($pipes[PIPE_25] ?? null)),
                Items::get(PIPE_32)->setCount(($pipes[PIPE_32] ?? null)),
            ];

            $length = Items::getLengthPodvodki('water');

            $count = array_sum((array) Data::floor()::get('items'));

            print View::render('water/pipe', compact('items', 'on_pipes', 'comment', 'riser', 'supply', 'length', 'count'));
        }
    }




    public function insulation()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('insulation', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('insulation.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->corners();
        } else {
            $comment = Data::floor()::get('insulation.comment');
            $items = Data::floor()::get('pipes');
            $insulation = (array) Data::floor()::get('insulation');

            $insulation = array_filter($insulation);

            $exitsCount = Items::getCountOfWaterExits((array) Data::floor()::get('items'));

            $count = [
                INSULATION_20 => $insulation[INSULATION_20] ?? (isset($items) ? ceil((int) $items[PIPE_20] * 1 + $exitsCount) : 1),
                INSULATION_25 => $insulation[INSULATION_25] ?? (isset($items) ? ceil((int) $items[PIPE_25] * 1) : 0),
                INSULATION_32 => $insulation[INSULATION_32] ?? (isset($items) ? ceil((int) $items[PIPE_32] * 1) : 0),
            ];

            $items = [
                PIPE_20 => Items::get(INSULATION_20)->setCount($count[INSULATION_20]),
                PIPE_25 => Items::get(INSULATION_25)->setCount($count[INSULATION_25]),
                PIPE_32 => Items::get(INSULATION_32)->setCount($count[INSULATION_32]),
            ];

            $riser = Data::floor()::get('riser');

            if ($riser) {
                if ($riser['size'] > 0) {
                    $items[$riser['item']]->addCount($riser['size']);
                }
            }

            // Сохранить комментарий сколько метров на стояк

            print View::render('water/insulation', compact('items', 'comment'));
        }
    }

    public function corners()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('corners', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('corners.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->couplings();
        } else {
            $comment = Data::floor()::get('corners.comment');
            $pipes = Data::floor()::get('pipes');
            $corners = Data::floor()::get('corners');
            $items = Data::floor()::get('items');

            $corners = ! $corners ?? array_filter($corners);

            $count = [
                CORNER_90_20 => $corners[CORNER_90_20] ?? max((ceil(((int) $pipes[PIPE_20] * 0.45 + Items::getCountOfWaterExits($items)) / 2) * 2), $pipes[PIPE_20] ? 10 : ''),
                CORNER_90_25 => $corners[CORNER_90_25] ?? max((ceil(((int) $pipes[PIPE_25] * 0.3 * 2) / 2) * 2), $pipes[PIPE_25] ? 8 : ''),
                CORNER_90_32 => $corners[CORNER_90_32] ?? max((ceil(((int) $pipes[PIPE_32] * 0.2 * 2) / 2) * 2), $pipes[PIPE_32] ? 10 : ''),
                CORNER_45_20 => $corners[CORNER_45_20] ?? max((ceil(((int) $pipes[PIPE_20] * 0.25) / 2) * 2), $pipes[PIPE_20] ? 10 : ''),
                CORNER_45_25 => $corners[CORNER_45_25] ?? max((ceil(((int) $pipes[PIPE_25] * 0.15 * 2) / 2) * 2), $pipes[PIPE_25] ? 8 : ''),
                CORNER_45_32 => $corners[CORNER_45_32] ?? max((ceil(((int) $pipes[PIPE_32] * 0.1 * 2) / 2) * 2), $pipes[PIPE_32] ? 10 : ''),
            ];

            $items90 = [
                Items::get(CORNER_90_20)->setCount($count[CORNER_90_20]),
                Items::get(CORNER_90_25)->setCount($count[CORNER_90_25]),
                Items::get(CORNER_90_32)->setCount($count[CORNER_90_32])
            ];

            $items45 = [
                Items::get(CORNER_45_20)->setCount($count[CORNER_45_20]),
                Items::get(CORNER_45_25)->setCount($count[CORNER_45_25]),
                Items::get(CORNER_45_32)->setCount($count[CORNER_45_32])
            ];

            print View::render('water/corners', compact('items90', 'items45', 'comment'));
        }
    }

    public function couplings()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('couplings', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('couplings.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->transitions();
        } else {
            $comment = Data::floor()::get('couplings.comment');
            $pipes = Data::floor()::get('pipes');
            $couplings = Data::floor()::get('couplings');

            $count = [
                COUPLING_20 => $couplings[COUPLING_20] ?? (max(ceil((int) $pipes[PIPE_20] * 1.5 * 0.25), 6)),
                COUPLING_25 => $couplings[COUPLING_25] ?? (max(ceil((int) $pipes[PIPE_25] * 1.5 * 0.25), 6)),
                COUPLING_32 => $couplings[COUPLING_32] ?? (max(ceil((int) $pipes[PIPE_32] * 1.5 * 0.25), 6)),
            ];

            $items = [
                Items::get(COUPLING_20)->setCount($count[COUPLING_20]),
                Items::get(COUPLING_25)->setCount($count[COUPLING_25]),
                Items::get(COUPLING_32)->setCount($count[COUPLING_32])
            ];

            print View::render('water/couplings', compact('items', 'comment'));
        }
    }

    public function transitions()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('transitions', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('transitions.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->tees();
        } else {
            $comment = Data::floor()::get('transitions.comment');
            $pipes = Data::floor()::get('pipes');
            $transitions = Data::floor()::get('transitions');

            $count = [
                TRANSITION_25_20 => $transitions[TRANSITION_25_20] ?? (((int) $pipes[PIPE_20] ? 10 : 0)),
                TRANSITION_32_25 => $transitions[TRANSITION_32_25] ?? (((int) $pipes[PIPE_25] ? 8 : 0)),
                TRANSITION_32_20 => $transitions[TRANSITION_32_20] ?? (((int) $pipes[PIPE_32] ? 6 : 0)),
            ];

            $items = [
                Items::get(TRANSITION_25_20)->setCount($count[TRANSITION_25_20]),
                Items::get(TRANSITION_32_25)->setCount($count[TRANSITION_32_25]),
                Items::get(TRANSITION_32_20)->setCount($count[TRANSITION_32_20])
            ];

            print View::render('water/transitions', compact('items', 'comment'));
        }
    }

    public function tees()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('tees', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('tees.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->MP();
        } else {
            $comment = Data::floor()::get('tees.comment');
            $itemsOnPipes = Data::floor()::get('water_on_pipes');
            $pipes = Data::floor()::get('pipes');
            $tees = Data::floor()::get('tees');

            $count = [
                TEE_20 => $tees[TEE_20] ?? ((int) $itemsOnPipes[PIPE_20] * 2),
                TEE_25 => $tees[TEE_25] ?? (((int) $pipes[PIPE_25] ? 6 : 0)),
                TEE_32 => $tees[TEE_32] ?? (((int) $pipes[PIPE_32] ? 6 : 0)),
                TEE_25_20_25 => $tees[TEE_25_20_25] ?? ((int) $itemsOnPipes[PIPE_25] * 2),
                TEE_25_20_20 => $tees[TEE_25_20_20] ?? (((int) $pipes[PIPE_25] ? 4 : 0)),
                TEE_32_20_32 => $tees[TEE_32_20_32] ?? ((int) $itemsOnPipes[PIPE_32] * 2),
                TEE_32_25_32 => $tees[TEE_32_25_32] ?? (((int) $pipes[PIPE_32] ? 4 : 0)),
            ];

            $items = [
                Items::get(TEE_20)->setCount($count[TEE_20]),
                Items::get(TEE_25)->setCount($count[TEE_25]),
                Items::get(TEE_32)->setCount($count[TEE_32]),
                Items::get(TEE_25_20_25)->setCount($count[TEE_25_20_25]),
                Items::get(TEE_25_20_20)->setCount($count[TEE_25_20_20]),
                Items::get(TEE_32_20_32)->setCount($count[TEE_32_20_32]),
                Items::get(TEE_32_25_32)->setCount($count[TEE_32_25_32])
            ];

            print View::render('water/tees', compact('items', 'comment'));
        }
    }

    public function MP()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('MP', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('MP.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->stub();
        } else {
            $comment = Data::floor()::get('MP.comment');
            $MP = Data::floor()::get('MP');
            $items = Data::floor()::get('items');

            $count = Items::getCountOfWaterExits($items);

            $items = [
                Items::get(MP_20_1_2_MOTHER)->setCount($MP[MP_20_1_2_MOTHER] ?? $count),
            ];

            print View::render('water/MP', compact('items', 'comment'));
        }
    }

    public function stub()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('stub', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('stub.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->additional();
        } else {
            $comment = Data::floor()::get('stub.comment');
            $stub = Data::floor()::get('stub');
            $items = Data::floor()::get('items');

            $count = Items::getCountOfWaterExits($items);

            $items = [
                Items::get(STUB_1_2_FATHER)->setCount($stub[STUB_1_2_FATHER] ?? $count),
            ];

            print View::render('water/stub', compact('items', 'comment'));
        }
    }


    // Couplings -> MP




    public function additional()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('additional', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('additional.comment', $_POST['comment']);
            }

            unset($_POST['submit']);
            $this->work();
        } else {
            $comment = Data::floor()::get('additional.comment');
            $additional = Data::floor()::get('additional');

            $riser = Data::floor()::get('riser');

            $count = [
                ADDITIONAL_SCREWS      => (array_sum((array) Data::floor()::get('legs')) * 8),
                ADDITIONAL_CLAMPING_1  => ($riser['item'] == PIPE_20 ? $riser['size'] : ''),
                ADDITIONAL_CLAMPING_2  => ($riser['item'] == PIPE_25 ? $riser['size'] : ''),
                ADDITIONAL_CLAMPING_3  => ($riser['item'] == PIPE_32 ? $riser['size'] : ''),
                ADDITIONAL_SELF_SCREWS => 0
            ];

            $items = [
                Items::get(ADDITIONAL_SCREWS)->setCount($count[ADDITIONAL_SCREWS]),
                Items::get(ADDITIONAL_CLAMPING_1)->setCount($count[ADDITIONAL_CLAMPING_1]),
                Items::get(ADDITIONAL_CLAMPING_2)->setCount($count[ADDITIONAL_CLAMPING_2]),
                Items::get(ADDITIONAL_CLAMPING_3)->setCount($count[ADDITIONAL_CLAMPING_3]),
                Items::get(ADDITIONAL_SELF_SCREWS)->setCount($count[ADDITIONAL_SELF_SCREWS])
            ];

            print View::render('water/additional', compact('additional', 'items', 'comment'));
        }
    }

    public function work()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                Data::floor()::set('work', $_POST['items']);
            }

            if (isset($_POST['comment'])) {
                Data::floor()::set('work.comment', $_POST['comment']);
            }

            unset($_POST['submit']);

            Alerts::add("Вы закончили редактирование этажа");
            (new FloorsController())->floors();
        } else {
            $comment = Data::floor()::get('work.comment');
            $pribori = Data::floor()::get('items');
            $riser = Data::floor()::get('riser');

            $work = Data::floor()::get('work');

            $count = [
                WORK_RADIATORS => ($work[WORK_RADIATORS]['count'] ?? Items::getRadiatorsCount()),
                WORK_DRILL => ($work[WORK_DRILL]['count'] ?? Items::getCountOfWaterExits($pribori)),
                WORK_RISER => '',
                WORK_HIGHWAY => '',
                WORK_COLLECTOR => ($work[WORK_COLLECTOR]['count'] ?? 1),
                WORK_COLLECTOR_LOCKER => ($work[WORK_COLLECTOR_LOCKER]['count'] ?? 1),
                WORK_ADDITIONAL => ''
            ];

            $price = [
                WORK_RADIATORS => ($work[WORK_RADIATORS]['price'] ?? 550),
                WORK_DRILL => ($work[WORK_DRILL]['price'] ?? 80),
                WORK_RISER => ($work[WORK_RISER]['price'] ?? 40),
                WORK_HIGHWAY => ($work[WORK_HIGHWAY]['price'] ?? 40),
                WORK_COLLECTOR => ($work[WORK_COLLECTOR]['price'] ?? 550),
                WORK_COLLECTOR_LOCKER => ($work[WORK_COLLECTOR_LOCKER]['price'] ?? 550),
                WORK_ADDITIONAL => ($work[WORK_ADDITIONAL]['price'] ?? 0),
            ];


            if ((int) $pribori[PRIBOR_1] > 0)
                $items[] = Items::get(WORK_PRIBOR_1)->setCount($pribori[PRIBOR_1] ?? '')->setPrice(Items::getExitsCount(PRIBOR_1) == 1 ? 250 : 500);
            if ((int) $pribori[PRIBOR_2] > 0)
                $items[] = Items::get(WORK_PRIBOR_2)->setCount($pribori[PRIBOR_2] ?? '')->setPrice(Items::getExitsCount(PRIBOR_2) == 1 ? 250 : 500);
            if ((int) $pribori[PRIBOR_3] > 0)
                $items[] = Items::get(WORK_PRIBOR_3)->setCount($pribori[PRIBOR_3] ?? '')->setPrice(Items::getExitsCount(PRIBOR_3) == 1 ? 250 : 500);
            if ((int) $pribori[PRIBOR_4] > 0)
                $items[] = Items::get(WORK_PRIBOR_4)->setCount($pribori[PRIBOR_4] ?? '')->setPrice(Items::getExitsCount(PRIBOR_4) == 1 ? 250 : 500);
            if ((int) $pribori[PRIBOR_5] > 0)
                $items[] = Items::get(WORK_PRIBOR_5)->setCount($pribori[PRIBOR_5] ?? '')->setPrice(Items::getExitsCount(PRIBOR_5) == 1 ? 250 : 500);
            if ((int) $pribori[PRIBOR_6] > 0)
                $items[] = Items::get(WORK_PRIBOR_6)->setCount($pribori[PRIBOR_6] ?? '')->setPrice(Items::getExitsCount(PRIBOR_6) == 1 ? 250 : 500);
            if ((int) $pribori[PRIBOR_7] > 0)
                $items[] = Items::get(WORK_PRIBOR_7)->setCount($pribori[PRIBOR_7] ?? '')->setPrice(Items::getExitsCount(PRIBOR_7) == 1 ? 250 : 500);
            if ((int) $pribori[PRIBOR_8] > 0)
                $items[] = Items::get(WORK_PRIBOR_8)->setCount($pribori[PRIBOR_8] ?? '')->setPrice(Items::getExitsCount(PRIBOR_8) == 1 ? 250 : 500);
            if ((int) $pribori[PRIBOR_9] > 0)
                $items[] = Items::get(WORK_PRIBOR_9)->setCount($pribori[PRIBOR_9] ?? '')->setPrice(Items::getExitsCount(PRIBOR_9) == 1 ? 250 : 500);
            if ((int) $pribori[PRIBOR_10] > 0)
                $items[] = Items::get(WORK_PRIBOR_10)->setCount($pribori[PRIBOR_10] ?? '')->setPrice(Items::getExitsCount(PRIBOR_10) == 1 ? 250 : 500);
            if ((int) $pribori[PRIBOR_11] > 0)
                $items[] = Items::get(WORK_PRIBOR_11)->setCount($pribori[PRIBOR_11] ?? '')->setPrice(Items::getExitsCount(PRIBOR_11) == 1 ? 250 : 500);

            $items[] = Items::get(WORK_DRILL)->setPrice($price[WORK_DRILL])->setCount($count[WORK_DRILL]);

            if ($riser && $riser['size'])
                $items[] = Items::get(WORK_RISER)->setPrice($price[WORK_RISER]);

            $items[] = Items::get(WORK_ADDITIONAL)->setPrice($price[WORK_ADDITIONAL]);

            print View::render('water/work', compact('items', 'comment'));
        }
    }
}
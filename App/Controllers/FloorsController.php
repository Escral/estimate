<?php

namespace App\Controllers;

use App\Data;
use App\Items;
use App\Models\Floor;
use App\Models\Item;
use App\View;

class FloorsController
{
    public function floors($section = 'heating')
    {
        $floors = Data::get('floors');

        if (! is_array($floors)) {
            $floors = Data::set('floors', [
                0 => "Подвал",
                1 => "1 этаж",
            ]);
        }

        $max = 0;
        foreach ($floors as $n => $val) {
            if ($n > $max) {
                $max = $n;
            }
        } 
        $nextFloor = $max + 1;

        $floorsVisited = Floor::getVisitedFloorsAll();
        $steps = [
            'heating' => 15,
            'water' => 12,
            'hfloors' => 10,
            'sewage' => 1,
            'boiler' => 15,
            'toold' => 1
        ];

        print View::render('pages/floors', compact('floors', 'nextFloor', 'floorsVisited', 'steps', 'section'));
    }

    public function change($floor)
    {
        $floor = (int) $floor;

        Floor::change($floor);

        $this->floors();
    }

    public function add($floor)
    {
        $floor = (int) $floor;

        $floors = Data::get('floors', [
            0 => "Подвал",
            1 => "1 этаж",
        ]);

        if (! isset($floors[$floor])) {
            $floors[$floor] = $_GET['name'] ?? $floor . " этаж";

            ksort($floors);

            Data::set('floors', $floors);
        }

        $this->floors();
    }

    public function edit($floor)
    {
        $floor = (int) $floor;

        $floors = Data::get('floors', [
            0 => "Подвал",
            1 => "1 этаж",
        ]);

        if (isset($_GET['name']))
            $floors[$floor] = $_GET['name'];

        Data::set('floors', $floors);


        $this->floors();
    }

    public function remove($floor)
    {
        $floor = (int) $floor;

        $floors = Data::get('floors', [
            0 => "Подвал",
            1 => "1 этаж",
        ]);

        if (isset($floors[$floor])) {
            unset($floors[$floor]);

            Data::set('floors', $floors);
        }


        $this->floors();
    }
}
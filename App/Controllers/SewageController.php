<?php

namespace App\Controllers;

use App\Data;
use App\Items;
use App\Path;
use App\View;

class SewageController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        Data::$prefix2 = 'sewage';
    }

    public function items()
    {
        if (isset($_POST['submit'])) {
            if (isset($_POST['items'])) {
                $_POST['items'] = array_filter($_POST['items'], function($value) { return $value !== ''; });

                Data::floor()::set('items', $_POST['items']);
            }

            unset($_POST['submit']);

            Path::remember('sewage');

            Alerts::add("Вы закончили редактирование этажа");
            (new FloorsController())->floors();
        } else {
            $selected = Data::floor()::get('items');

            list($items110, $items50, $items32, $items) = Items::getSewageItems($selected);

            print View::render('sewage/items', compact('items', 'items110', 'items50', 'items32'));
        }
    }
}
<?php

namespace App;

use App\Helpers\Http;
use App\Models\User;

class Auth
{
    /**
     * @return User|null
     */
    public static function user()
    {
        if (! Session::has('user'))
            return null;

        return User::find(Session::get('user')['ID']);
    }

    public static function getActiveUserId()
    {
        $user = self::user();

        if ($user)
            return $user->ID;

        return null;
    }

    public static function loggined()
	{	
        return ! is_null(self::user());
	}

    public static function check()
    {
        if (! self::loggined())
            Http::redirect("/login");
	}

    public static function attempt($login, $password)
    {
        $userID = Database::value(Database::query("SELECT ID FROM user WHERE login = ? AND password = ?", [$login, md5($password)]));

        if ($userID) {
            $user = User::find($userID['ID']);

            if (! $user)
                return false;

            $user->login();
            return $user;
        } else {
            return false;
        }
    }

    public static function login(User $user)
    {
        Session::set('user', [
            'ID' => $user->ID,
            'login' => $user->login
        ]);
    }
}
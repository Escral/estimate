<?php

namespace App;

use Symfony\Component\Finder\Finder;

class Translation
{
    protected static $translations;
    public static $file;

    public static function loadTranslations()
    {
        self::$file = Settings::get('translations_dir') . '/' . Settings::get('lang') . '.json';

        self::$translations = json_decode(file_get_contents(self::$file), true);
    }

    public static function get($text)
    {
        if (isset(self::$translations[$text]))
            return self::$translations[$text];
    }

    public static function index()
    {
        $dirs = [
            base_dir('App'),
            views_dir()
        ];

        $functions = ['lang'];

        $groupPattern =                          // See https://regexr.com/3i79b
            // "[^\w|>]" .                          // Must not have an alphanum or _ or > before real method
            '(' . implode('|', $functions) . ')' .  // Must start with one of the functions
            "\(" .                               // Match opening parenthesis
            "[\'\"]" .                           // Match " or '
            '(([a-zA-Z0-9_-]+)::)*' .              // Package prefix
            '(' .                                // Start a new group to match:
            '[a-zA-Z0-9_-]+' .               // Must start with group
            "([.|\/][^\1)]+)+" .             // Be followed by one or more items/keys
            ')' .                                // Close group
            "[\'\"]" .                           // Closing quote
            "[\),]";                             // Close parentheses or new parameter

        $stringPattern =
            // "[^\w|>]" .                                     // Must not have an alphanum or _ or > before real method
            '(' . implode('|', $functions) . ')' .          // Must start with one of the functions
            "\(" .                                          // Match opening parenthesis
            "(?P<quote>['\"])" .                            // Match " or ' and store in {quote}
            "(?P<string>(?:\\\k{quote}|(?!\k{quote}).)*)" . // Match any string that can be {quote} escaped
            "\k{quote}" .                                   // Match " or ' previously matched
            "[\),]";                                        // Close parentheses or new parameter

        // Find all PHP + Twig files in the app folder, except for storage
//        foreach ($dirs as $dir) {
        $groupKeys = [];
        $stringKeys = [];

        $finder = new Finder();
        $finder->in($dirs)->exclude(['assets', 'public'])->name('*.php')->files();

        /** @var \Symfony\Component\Finder\SplFileInfo $file */
        foreach ($finder as $file) {
            // Search the current file for the pattern
            if (preg_match_all("/$groupPattern/siU", $file->getContents(), $matches)) {
                // Get all matches
                foreach ($matches[4] as $key => $value) {
                    $groupKeys[] = ($matches[2][$key]) . $value;
                }
            }

            if (preg_match_all("/$stringPattern/siU", $file->getContents(), $matches)) {
                foreach ($matches['string'] as $key) {
                    if (preg_match("/(^(([a-zA-Z0-9_-]+)::)*[a-zA-Z0-9_-]+([.][^\1)\ ]+)+$)/siU", $key, $groupMatches)) {
                        // group{.group}.key format, already in $groupKeys but also matched here
                        // do nothing, it has to be treated as a group
                        continue;
                    }
                    $stringKeys[] = $key;
                }
            }
        }

        $stringKeys = array_flip(array_unique($stringKeys));

        $stringKeys = array_map(function () {
            return '';
        }, $stringKeys);

        $count = max(0, count($stringKeys) - count(self::$translations));

        self::$translations = array_merge($stringKeys, array_intersect_key(self::$translations, $stringKeys));

        file_put_contents(self::$file, json_encode(self::$translations, JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE));
        chmod(self::$file, 0777);

        return $count;
    }
}
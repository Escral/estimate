<?php

namespace App;

class Entity
{
    public static $modelName;
    public $info;

    public function __get($var)
    {
        if (method_exists($this, 'get' . ucfirst($var)))
            return $this->{'get' . ucfirst($var)}();

        if (isset($this->info[$var]))
            return $this->info[$var];
    }

    public function __set($var, $value)
    {
        if (method_exists($this, 'set' . ucfirst($var)))
            return $this->info[$var] = $this->{'set' . ucfirst($var)}();

        return $this->info[$var] = $value;
    }

    public static function getModelName()
    {
        $classname = static::class;

        $pos = strrpos($classname, '\\');

        return substr($classname, $pos + 1);

//		if (static::$modelName !== null)
//			return static::$modelName;
//
//        $classname = static::class;
//
//        if ($pos = strrpos($classname, '\\'))
//            $classname = substr($classname, $pos + 1);
//
//        static::$modelName = $classname;
//
//		return static::$modelName;
    }

    public static function getTableName()
    {
        return strtolower(static::getModelName());
    }

    public static function exists($value, $field = 'ID')
    {
        $table = static::getTableName();

        return (bool) Database::query("SELECT COUNT(*) FROM `" . $table . "` WHERE `" . $field . "` = ?", [$value])->fetchColumn();
    }

    public static function findAsArray($table, $value, $fields = false)
    {
        $query = [];

        $fields = ($fields !== false) ? $fields : ['ID'];

        foreach ($fields as $field) {
            if ($field == 'ID' and ! preg_match('#^[0-9]+$#isu', $value))
                continue;

            $query[] = ($field == 'ID' ? 'i.' : '') . '`' . $field . '` = \'' . $value . '\'';
        }

        if (count($query)) {
            $data = Database::value("SELECT * FROM `" . $table . "` i WHERE " . implode(' OR ', $query));
        }

        return (isset($data)) ? $data : null;
    }

    /**
     * @param $value
     * @param bool $fields
     * @return null|static
     */
    public static function find($value, $fields = false)
    {
        $model = static::class;
        $table = static::getTableName();

        $object = new $model();

        $data = static::findAsArray($table, $value, $fields);

        $object->setData($data ?? []);

        return ($data && count($data)) ? $object : null;
    }

    public function fresh()
    {
        $table = static::getTableName();

        $data = static::findAsArray($table, $this->ID, ['ID']);

        return $this->setData($data);
    }

    public function setData($data)
    {
        $this->info = $data;

        return $this;
    }

    public function toArray()
    {
        return (array) $this->info;
    }

    /**
     * @param array $params
     * @param bool $paginate
     * @return static[]|Pagination[]
     * @throws \Exception
     */
    public static function getList($params = [], $paginate = false)
    {
        $model = static::class;
        $table = static::getTableName();

        if (isset($params['where'])) {
            foreach ($params['where'] as $field => $q) {
                $query[] = "`" . $field . "` = '" . $q . "'";
            }
        } else {
            $query = [
                '1'
            ];
        }

        $order = (isset($params['order'])) ? $params['order'] : 'ID';
        $orderDirection = (isset($params['orderDirection'])) ? $params['orderDirection'] : 'ASC';

        if (isset($params['join'])) {
            $join = ' LEFT JOIN ' . $params['join'];
        } else {
            $join = '';
        }

        if (isset($params['select'])) {
            $select = ', ' . $params['select'];
        } else {
            $select = '';
        }


        // Pagination
        $page = (isset($params['page'])) ? $params['page'] : 1;
        $onPage = (isset($params['onPage'])) ? $params['onPage'] : false;

        $limit = $onPage ? " LIMIT " . (($page - 1) * $onPage) . ", " . $onPage : '';

        $array = Database::getAll("SELECT *" . $select . " FROM `" . $table . '` i' . $join . " WHERE " . implode(' AND ', $query) . " ORDER BY " . $order . " " . $orderDirection . $limit);

        $items = [];

        foreach ($array as $item) {
            $items[] = (new $model())->setData($item);
        }

        if ($paginate) {
            $total = Database::query("SELECT COUNT(*) FROM `" . $table . '` i' . $join . " WHERE " . implode(' AND ', $query))->fetchColumn();

            $pagination = new Pagination($total, $onPage, $page);

            return [$items, $pagination];
        }

        return $items;
    }

    public function create()
    {
        $table = static::getTableName();

        $q = implode(', ', array_fill(0, count($this->info), '?'));

        $array = array_slice($this->info, 0, count($this->info));
        $titles = implode(', ', array_keys($array));
        $values = array_values($array);

        if (! Database::query("INSERT INTO {$table} ({$titles}) VALUES ($q)", $values)) {
            throw new \Exception("Can't add to `{$table}`");
        }

        return self::find(Database::getLastInsertId());
    }

    public function update($data = [])
    {
        $table = static::getTableName();

        $values = [];
        $fields = [];

        foreach ($data as $key => $value) {
            $fields[] = $key . ' = ?';
            $values[] = $value;
        }

        $query = implode(', ', $fields);
echo "UPDATE {$table} SET {$query} WHERE ID = '{$this->ID}'";
        return Database::query("UPDATE {$table} SET {$query} WHERE ID = '{$this->ID}'", $values);
    }

    public static function updateField($ID, $field, $text = '')
    {
        return Database::query('UPDATE `' . static::getTableName() . '` SET `' . $field . '` = ? WHERE `ID` = ?', [$text, $ID]);
    }

    /**
     * @return \PDOStatement
     */
    public function remove()
    {
        $table = static::getTableName();

        return Database::query("DELETE FROM {$table} WHERE ID = '{$this->ID}'");
    }
}
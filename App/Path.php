<?php

namespace App;

class Path
{
    public static $PATH = [
        'select' => [
            'next'  => 'futorki',
            'title' => "Выбор радиаторов",
            'path'  => '/radiators'
        ],
        'futorki' => [
            'next'  => 'legs',
            'title' => "Радиаторный комплект",
            'path'  => '/radiators/futorki'
        ],
        'legs'   => [
            'next'  => 'scheme',
            'title' => "Выбор ножек для радиаторов",
            'path'  => '/radiators/legs'
        ],
        'scheme' => [
            'next'  => ['duopipe', 'beam'],
            'title' => "Выбор схемы обвязки",
            'path'  => '/radiators/scheme'
        ],

                    'beam'         => [
                        'next'  => 'radiators_insulation_riser',
                        'title' => "Выбор металлопласта",
                        'path'  => '/radiators/beam'
                    ],
                    'radiators_insulation_riser' => [
                        'next'  => 'radiators_corners_riser',
                        'title' => "Выбор изоляции для стояка",
                        'path'  => '/radiators/insulation_riser'
                    ],
                    'radiators_corners_riser' => [
                        'next'  => 'radiators_couplings_riser',
                        'title' => "Выбор углов для стояка",
                        'path'  => '/radiators/corners_riser'
                    ],
                    'radiators_couplings_riser' => [
                        'next'  => 'collector',
                        'title' => "Выбор муфт для стояка",
                        'path'  => '/radiators/couplings_riser'
                    ],
                    'collector'    => [
                        'next'  => 'gerpex',
                        'title' => "Выбор коллектора",
                        'path'  => '/radiators/collector'
                    ],
                    'gerpex'       => [
                        'next'  => 'locker',
                        'title' => "Выбор герпексов",
                        'path'  => '/radiators/gerpex'
                    ],
                    'locker'       => [
                        'next'  => 'valves_metal',
                        'title' => "Выбор шкафа для коллектора",
                        'path'  => '/radiators/locker'
                    ],
                    'valves_metal' => [
                        'next'  => 'gerpex_valves',
                        'title' => "Выбор кранов для радиаторов",
                        'path'  => '/radiators/valves_metal'
                    ],
                    'gerpex_valves' => [
                        'next'  => 'additional_beam',
                        'title' => "Выбор герпексов для кранов",
                        'path'  => '/radiators/gerpex_valves'
                    ],
                    'additional_beam' => [
                        'next'  => 'work_beam',
                        'title' => "Дополнительные материалы",
                        'path'  => '/radiators/additional_beam'
                    ],
                    'work_beam'        => [
                        'next'  => '',
                        'title' => "Монтажные работы",
                        'path'  => '/radiators/work_beam'
                    ],

        'duopipe'     => [
            'next'  => 'insulation',
            'title' => "Выбор ППР трубы",
            'path'  => '/radiators/duopipe'
        ],
        'insulation'  => [
            'next'  => 'corners',
            'title' => "Выбор изоляции для трубы",
            'path'  => '/radiators/insulation'
        ],
        'corners'     => [
            'next'  => 'couplings',
            'title' => "Выбор уголков",
            'path'  => '/radiators/corners'
        ],
        'couplings'   => [
            'next'  => 'transitions',
            'title' => "Выбор муфты",
            'path'  => '/radiators/couplings'
        ],
        'transitions' => [
            'next'  => 'tees',
            'title' => "Выбор переходов ППР",
            'path'  => '/radiators/transitions'
        ],
        'tees'        => [
            'next'  => 'valves',
            'title' => "Выбор тройников ППР",
            'path'  => '/radiators/tees'
        ],
        'valves'      => [
            'next'  => 'MP',
            'title' => "Выбор кранов для радиаторов",
            'path'  => '/radiators/valves'
        ],
        'MP'          => [
            'next'  => 'additional',
            'title' => "Выбор МП-шек",
            'path'  => '/radiators/MP'
        ],
        'additional' => [
            'next'  => 'work',
            'title' => "Дополнительные материалы",
            'path'  => '/radiators/additional'
        ],
        'work'        => [
            'next'  => '',
            'title' => "Монтажные работы",
            'path'  => '/radiators/work'
        ],



        'water_scheme'        => [
            'next'  => 'water_items',
            'title' => "Выбор схемы разводки",
            'path'  => '/water'
        ],
        'water_items'        => [
            'next'  => 'water_pipe',
            'title' => "Выбор приборов",
            'path'  => '/water/items'
        ],
        'water_pipe'         => [
            'next'  => 'water_insulation',
            'title' => "Выбор ППР для водоснабжения",
            'path'  => '/water/pipe'
        ],
        'water_insulation'  => [
            'next'  => 'water_corners',
            'title' => "Выбор изоляции для трубы",
            'path'  => '/water/insulation'
        ],
        'water_corners'     => [
            'next'  => 'water_couplings',
            'title' => "Выбор уголков",
            'path'  => '/water/corners'
        ],
        'water_couplings'   => [
            'next'  => 'water_transitions',
            'title' => "Выбор муфты",
            'path'  => '/water/couplings'
        ],
        'water_transitions' => [
            'next'  => 'water_tees',
            'title' => "Выбор переходов ППР",
            'path'  => '/water/transitions'
        ],
        'water_tees'        => [
            'next'  => 'water_MP',
            'title' => "Выбор тройников ППР",
            'path'  => '/water/tees'
        ],
        'water_MP'          => [
            'next'  => 'water_stub',
            'title' => "Выбор МП-шек",
            'path'  => '/water/MP'
        ],
        'water_stub'        => [
            'next'  => 'water_additional',
            'title' => "Выбор заглушек",
            'path'  => '/water/stub'
        ],
        'water_additional' => [
            'next'  => 'water_work',
            'title' => "Дополнительные материалы",
            'path'  => '/water/additional'
        ],
        'water_work'        => [
            'next'  => '',
            'title' => "Монтажные работы",
            'path'  => '/water/work'
        ],



        'hfloors_scheme'        => [
            'next'  => ['hfloors_riser', 'hfloors_area_2'],
            'title' => "Выбор схемы укладки",
            'path'  => '/hfloors'
        ],


        'hfloors_riser' => [
            'next'  => 'hfloors_insulation_riser',
            'title' => "Выбор стояка",
            'path'  => '/hfloors/riser'
        ],
        'hfloors_insulation_riser' => [
            'next'  => 'hfloors_corners_riser',
            'title' => "Выбор изоляции для стояка",
            'path'  => '/hfloors/insulation_riser'
        ],
        'hfloors_corners_riser' => [
            'next'  => 'hfloors_couplings_riser',
            'title' => "Выбор углов для стояка",
            'path'  => '/hfloors/corners_riser'
        ],
        'hfloors_couplings_riser' => [
            'next'  => 'hfloors_area',
            'title' => "Выбор муфт для стояка",
            'path'  => '/hfloors/couplings_riser'
        ],


        'hfloors_area'        => [
            'next'  => 'hfloors_pipe',
            'title' => "Выбор площади помещения",
            'path'  => '/hfloors/area'
        ],
        'hfloors_pipe'        => [
            'next'  => 'hfloors_insulation',
            'title' => "Выбор трубы",
            'path'  => '/hfloors/pipe'
        ],
        'hfloors_insulation'  => [
            'next'  => 'hfloors_skin',
            'title' => "Выбор изоляции",
            'path'  => '/hfloors/insulation'
        ],
        'hfloors_skin'  => [
            'next'  => 'hfloors_insulation_join',
            'title' => "Выбор плёнки",
            'path'  => '/hfloors/skin'
        ],
        'hfloors_insulation_join'  => [
            'next'  => 'hfloors_clips',
            'title' => "Выбор крепления изоляции к полу",
            'path'  => '/hfloors/insulation_join'
        ],
        'hfloors_clips'  => [
            'next'  => 'hfloors_collector',
            'title' => "Выбор гарпунов",
            'path'  => '/hfloors/clips'
        ],
        'hfloors_collector'  => [
            'next'  => 'hfloors_gerpex',
            'title' => "Выбор коллектора",
            'path'  => '/hfloors/collector'
        ],
        'hfloors_gerpex'  => [
            'next'  => 'hfloors_corner_fixes',
            'title' => "Выбор герпексов",
            'path'  => '/hfloors/gerpex'
        ],
        'hfloors_corner_fixes'  => [
            'next'  => 'hfloors_locker',
            'title' => "Выбор угловых фиксаторов",
            'path'  => '/hfloors/corner_fixes'
        ],
        'hfloors_locker'  => [
            'next'  => 'hfloors_additional',
            'title' => "Выбор шкафа для коллектора",
            'path'  => '/hfloors/locker'
        ],
        'hfloors_additional'  => [
            'next'  => 'hfloors_work',
            'title' => "Дополнительные материалы",
            'path'  => '/hfloors/additional'
        ],
        'hfloors_work'  => [
            'next'  => '',
            'title' => "Монтажные работы",
            'path'  => '/hfloors/work'
        ],





        'hfloors_area_2'        => [
            'next'  => 'hfloors_pipe_2',
            'title' => "Выбор площади помещения",
            'path'  => '/hfloors/area_2'
        ],
        'hfloors_pipe_2'        => [
            'next'  => 'hfloors_insulation_2',
            'title' => "Выбор трубы",
            'path'  => '/hfloors/pipe_2'
        ],
        'hfloors_insulation_2'  => [
            'next'  => 'hfloors_skin_2',
            'title' => "Выбор изоляции",
            'path'  => '/hfloors/insulation_2'
        ],
        'hfloors_skin_2'  => [
            'next'  => 'hfloors_insulation_join_2',
            'title' => "Выбор плёнки",
            'path'  => '/hfloors/skin_2'
        ],
        'hfloors_insulation_join_2'  => [
            'next'  => 'hfloors_clips_2',
            'title' => "Выбор крепления изоляции к полу",
            'path'  => '/hfloors/insulation_join_2'
        ],
        'hfloors_clips_2'  => [
            'next'  => 'hfloors_fjvr',
            'title' => "Выбор гарпунов",
            'path'  => '/hfloors/clips_2'
        ],
        'hfloors_fjvr'  => [
            'next'  => 'hfloors_gerpex_2',
            'title' => "Выбор FJVR клапана",
            'path'  => '/hfloors/fjvr'
        ],
        'hfloors_gerpex_2'  => [
            'next'  => 'hfloors_corner_fixes_2',
            'title' => "Выбор герпексов",
            'path'  => '/hfloors/gerpex_2'
        ],
        'hfloors_corner_fixes_2'  => [
            'next'  => 'hfloors_work_2',
            'title' => "Выбор угловых фиксаторов",
            'path'  => '/hfloors/corner_fixes_2'
        ],
        'hfloors_work_2'  => [
            'next'  => '',
            'title' => "Монтажные работы",
            'path'  => '/hfloors/work_2'
        ],







        'sewage'  => [
            'next'  => '',
            'title' => "Выбор товаров",
            'path'  => '/sewage'
        ],



        'tools'  => [
            'next'  => '',
            'title' => "Выбор инструментов",
            'path'  => '/tools'
        ],





        'kettles'  => [
            'next'  => 'boiler',
            'title' => "Выбор котлов",
            'path'  => '/boiler'
        ],
        'boiler'  => [
            'next'  => 'kettle_piping',
            'title' => "Выбор бойлера",
            'path'  => '/boiler/boiler'
        ],
        'kettle_piping'  => [
            'next'  => 'boiler_piping',
            'title' => "Обвязка котла",
            'path'  => '/boiler/kettle_piping'
        ],
        'boiler_piping'  => [
            'next'  => 'boiler_collector',
            'title' => "Обвязка бойлера",
            'path'  => '/boiler/boiler_piping'
        ],
        'boiler_collector'  => [
            'next'  => 'collector_piping',
            'title' => "Выбор коллектора",
            'path'  => '/boiler/boiler_collector'
        ],
        'collector_piping'  => [
            'next'  => '',
            'title' => "Группы быстрого монтажа",
            'path'  => '/boiler/collector_piping'
        ],
        'pump'  => [
            'next'  => '',
            'title' => "Выбор насоса",
            'path'  => '/boiler/pump'
        ],
    ];

    public static function getFirstRoute($section)
    {
        if ($section == 'heating')
            return 'select';
        elseif ($section == 'water')
            return 'water_scheme';
        elseif ($section == 'hfloors')
            return 'hfloors_riser';
        elseif ($section == 'sewage')
            return 'sewage_items';
        elseif ($section == 'boiler')
            return 'kettles';
        elseif ($section == 'tools')
            return 'tools_items';
    }

    public static function getPath($page)
    {
        $back = [];
        foreach (self::$PATH as $n => $i) {
            $a = $i['next'];
            if (is_array($a)) {
                foreach ($a as $v)
                    $back[$v] = $n;
            } else {
                $back[$a] = $n;
            }
        }




        $visited = Data::floor()::get('path');
        $visited = $visited[Data::$prefix2] ?? null;

        $n = 0;
        foreach ($back as $item => &$prev) {
            $prev = [
                'prev' => $prev,
                'index' => $n++
            ];
        }

        if (is_array($visited)) {
            $index = 0;
            $key = $page;
            foreach ($visited as $item => $is) {
                $i = $back[$item]['index'] ?? 0;
                if ($i > $index) {
                    $index = $i;
                    $key = $item;
                }
            }

            if (! isset($back[$page]) or ($key != '' and $back[$page]['index'] < $index)) {
                $page = $key;
            }
        }



        $path = [];

        $item = self::$PATH[$name = $page];

        do {
            $item['name'] = $name;
            $path[$name] = $item;

            if (! isset($back[$name]))
                break;

            $name = $back[$name]['prev'];
        } while ($item = self::$PATH[$name]);

        $path = array_reverse($path);

        return $path;
    }

    public static function get($page)
    {
        return self::$PATH[$page];
    }

    public static function getTitle($page)
    {
        return self::get($page)['title'];
    }

    public static function remember($pathItem)
    {
        $page = Data::$prefix2;

        $path = Data::floor()::get('path');

        if (! isset($path[$page]))
            $path[$page] = [];

        $path[$page][$pathItem] = true;

        Data::floor()::set('path', $path);
    }

//    public static function forget($pathItem)
//    {
//        $page = Data::$prefix2;
//
//        $path = Data::floor()::get('path');
//
//        $f = false;
//        foreach ($path[$page] as $item => $is) {
//            if ($item == $pathItem)
//                $f = true;
//
//            if ($f)
//                unset($path[$page][$item]);
//        }
//
//        Data::floor()::set('path', $path);
//    }

    public static function forget($key)
    {
        $page = Data::$prefix2;

        $path = Data::floor()::get('path');

        if ($key == 'beam') {
            unset($path[$page]['beam']);
            unset($path[$page]['radiators_insulation_riser']);
            unset($path[$page]['radiators_corners_riser']);
            unset($path[$page]['radiators_couplings_riser']);
            unset($path[$page]['collector']);
            unset($path[$page]['gerpex']);
            unset($path[$page]['locker']);
            unset($path[$page]['valves_metal']);
            unset($path[$page]['gerpex_valves']);
            unset($path[$page]['additional_beam']);
            unset($path[$page]['work_beam']);
        } elseif ($key == 'duopipe') {
            unset($path[$page]['duopipe']);
            unset($path[$page]['insulation']);
            unset($path[$page]['corners']);
            unset($path[$page]['couplings']);
            unset($path[$page]['transitions']);
            unset($path[$page]['tees']);
            unset($path[$page]['valves']);
            unset($path[$page]['MP']);
            unset($path[$page]['additional']);
            unset($path[$page]['work']);
        }elseif ($key == 'area') {
            unset($path[$page]['hfloors_riser']);
            unset($path[$page]['hfloors_insulation_riser']);
            unset($path[$page]['hfloors_corners_riser']);
            unset($path[$page]['hfloors_couplings_riser']);
            unset($path[$page]['hfloors_area']);
            unset($path[$page]['hfloors_pipe']);
            unset($path[$page]['hfloors_insulation']);
            unset($path[$page]['hfloors_skin']);
            unset($path[$page]['hfloors_insulation_join']);
            unset($path[$page]['hfloors_clips']);
            unset($path[$page]['hfloors_collector']);
            unset($path[$page]['hfloors_gerpex']);
            unset($path[$page]['hfloors_corner_fixes']);
            unset($path[$page]['hfloors_locker']);
            unset($path[$page]['hfloors_additional']);
            unset($path[$page]['hfloors_work']);

            Data::floor()::remove('insulation_riser');
            Data::floor()::remove('corners_riser');
            Data::floor()::remove('couplings_riser');
            Data::floor()::remove('area');
            Data::floor()::remove('pipes');
            Data::floor()::remove('insulation');
            Data::floor()::remove('skin');
            Data::floor()::remove('insulation_join');
            Data::floor()::remove('clips');
            Data::floor()::remove('collector');
            Data::floor()::remove('gerpex');
            Data::floor()::remove('corner_fixes');
            Data::floor()::remove('locker');
            Data::floor()::remove('additional');
            Data::floor()::remove('work');
        } elseif ($key == 'area_2') {
            unset($path[$page]['hfloors_area_2']);
            unset($path[$page]['hfloors_pipe_2']);
            unset($path[$page]['hfloors_insulation_2']);
            unset($path[$page]['hfloors_skin_2']);
            unset($path[$page]['hfloors_insulation_join_2']);
            unset($path[$page]['hfloors_clips_2']);
            unset($path[$page]['hfloors_fjvr']);
            unset($path[$page]['hfloors_gerpex_2']);
            unset($path[$page]['hfloors_corner_fixes_2']);
            unset($path[$page]['hfloors_work_2']);

            Data::floor()::remove('insulation_riser');
            Data::floor()::remove('corners_riser');
            Data::floor()::remove('couplings_riser');
            Data::floor()::remove('area');
            Data::floor()::remove('pipes');
            Data::floor()::remove('insulation');
            Data::floor()::remove('skin');
            Data::floor()::remove('insulation_join');
            Data::floor()::remove('clips');
            Data::floor()::remove('collector');
            Data::floor()::remove('gerpex');
            Data::floor()::remove('corner_fixes');
            Data::floor()::remove('locker');
            Data::floor()::remove('additional');
            Data::floor()::remove('work');
        }

        Data::floor()::set('path', $path);
    }
}
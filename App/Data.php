<?php

namespace App;

use App\Models\Estimate;
use App\Models\Floor;

class Data
{
    public static $prefix2 = '';
    private static $data;
    private static $prefix = '';
    public static $global = ['floor', 'path', 'floors'];

    public static function init()
    {
        $data = Database::value("SELECT `estimate` FROM `estimate` WHERE ID = " . Estimate::getActiveId() . " AND user_ID = " . (int) Auth::getActiveUserId());

        if ($data)
            self::$data = unserialize($data['estimate']);
    }

    /**
     * @return self
     */
    public static function floor($floor = null)
    {
        $floor = $floor !== null ? $floor : Floor::getFloor();

        self::$prefix = 'floor' . $floor;

        return __CLASS__;
    }

    public static function getAll()
    {
        return self::$data;
    }
    
    /**
     * @param string $key
     * @param null|\Closure $value
     * @return null|mixed
     */
    public static function get($key, $value = null)
    {
        $origKey = $key;

        if (self::$prefix != '')
            $key = self::$prefix . '.' . $key;

        if (self::$prefix2 != '' && ! in_array($origKey, self::$global))
            $key = self::$prefix2 . '.' . $key;

        self::$prefix = '';

        if (isset(self::$data[$key])) {
            return self::$data[$key];
        } else {
            if ($value instanceof \Closure) {
                return $value();
//                Data::set($key, $value());
            } else if ($value !== null) {
                return $value;
//                Data::set($key, $value);
            } else {
                return null;
            }
            return self::$data[$key];
        }
    }

    /**
     * @param string $key
     */
    public static function remove($key)
    {
        $origKey = $key;

        if (self::$prefix != '')
            $key = self::$prefix . '.' . $key;

        if (self::$prefix2 != '' && ! in_array($origKey, self::$global))
            $key = self::$prefix2 . '.' . $key;

        self::$prefix = '';

        if (isset(self::$data[$key])) {
            unset(self::$data[$key]);
        }

        if (isset(self::$data[$key . '.comment'])) {
            unset(self::$data[$key . '.comment']);
        }
    }

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public static function set($key, $value)
    {
        $origKey = $key;

        if (self::$prefix != '')
            $key = self::$prefix . '.' . $key;

        if (self::$prefix2 != '' && ! in_array($origKey, self::$global))
            $key = self::$prefix2 . '.' . $key;

        self::$prefix = '';

        return self::$data[$key] = $value;
    }

    public static function save()
    {
        if (! Auth::loggined())
            return;

        $value = serialize(self::$data);

        Database::query("UPDATE `estimate` SET `estimate` = ? WHERE ID = " . Estimate::getActiveId() . " AND user_ID = " . (int) Auth::getActiveUserId(), [$value]);
    }

    public static function clear()
    {
        self::$data = [];

        Database::query("UPDATE `estimate` SET `estimate` = '' WHERE ID = " . Estimate::getActiveId() . " AND user_ID = " . (int) Auth::getActiveUserId());
    }

    public static function setSection($section)
    {
        self::$prefix2 = $section;
    }
}
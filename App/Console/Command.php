<?php

namespace App\Console;

use App\App;
use Symfony\Component\Console\Application;

class Command extends \Symfony\Component\Console\Command\Command
{
    public function setApplication(Application $application = null)
    {
        (new App())->init();

        parent::setApplication($application);
    }
}
<?php

namespace App\Console\Command;

use App\App;
use App\Console\Command;
use App\Translation;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class IndexPhrasesCommand extends Command
{
    protected function configure()
    {
        $this->setName('index')
        ->setDescription('Index multilang phrases')
        ->setHelp('Index multilang phrases and saves them to .json file in translations directory');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $count = Translation::index();

        $output->writeln('<info>' . $count . ' phrases indexed</info>');
    }
}
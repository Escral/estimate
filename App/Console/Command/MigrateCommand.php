<?php

namespace App\Console\Command;

use App\App;
use App\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateCommand extends Command
{
    protected function configure()
    {
        $this->setName('migrate')
        ->setDescription('Migrate database')
        ->setHelp('This command allows you to migrate database');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Migrating database</comment>');

        App::migrate();

        $output->writeln('<info>Done</info>');
    }
}
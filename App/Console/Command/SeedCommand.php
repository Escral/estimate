<?php

namespace App\Console\Command;

use App\App;
use App\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SeedCommand extends Command
{
    protected function configure()
    {
        $this->setName('seed')
        ->setDescription('Seed database')
        ->setHelp('This command allows you to generate data');

        $this->addArgument('seeder', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $seederName = $input->getArgument('seeder');

        if ($seederName != '') {
            $class = '\App\Seeds\\' . ucfirst($seederName) . 'Seeder';

            $seeder = new $class();

            $output->writeln('<comment>Seeding ' . $class . '</comment>');

            $seeder->execute();

            $output->writeln('<info>Seeded ' . $class . '</info>');
        } else {
            $output->writeln('<comment>Seeding database</comment>');

            App::seed();

            $output->writeln('<info>Done</info>');
        }
    }
}
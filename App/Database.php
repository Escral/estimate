<?php

namespace App;

use PDO;
use PDOStatement;

/**
 * Работа с базой данных
 *
 * Class Database
 */
class Database
{
    protected static $db;
    public static $queries = 0;

    public function __construct()
    {
        self::$db = $this->databaseConnect();
    }

    /**
     * Подключение к базе данных
     *
     * @return PDO
     */
    protected function databaseConnect()
    {
        $dsn = 'mysql:host=' . Settings::get('DB_HOST') . ';dbname=' . Settings::get('DB_NAME') . ';charset=' . Settings::get('DB_ENCODING');

        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ORACLE_NULLS       => PDO::NULL_EMPTY_STRING
        ];

        $db = new PDO($dsn, Settings::get('DB_USER'), Settings::get('DB_PASS'), $opt);

        $db->exec('SET NAMES ' . Settings::get('DB_ENCODING'));
        $db->exec('SET CHARACTER SET ' . Settings::get('DB_ENCODING'));
        $db->exec('SET COLLATION_CONNECTION="' . Settings::get('DB_COLLATION') . '"');
        $db->exec('SET time_zone = "' . Settings::get('DB_TIMEZONE') . '"');

        return $db;
    }

    /**
     * Выполнение стандартного Query запроса
     *
     * Если передать параметр $variables, скрипт будет использовать подготовленные выражения.
     *
     * @param string $sql_query SQL запрос
     * @param bool|array $variables Подготовленные переменные
     * @return PDOStatement
     */
    public static function query($sql_query, $variables = false)
    {
        // try {
        if (! $variables) {
            // Использование обычного выражения
            $q = self::$db->query($sql_query);
        } else {
            // Использование подготовленного выражения

            $stmt = self::$db->prepare($sql_query);

            $stmt->execute($variables);

            $q = $stmt;
        }

        // } catch (\mysqli_sql_exception $e) {
        // 	throw new \Exception('Bad Query: ' . $sql_query, $e->getCode(), $e);
        // }

        // Увеличиваем счётчик запросов
        self::$queries++;

        return $q;
    }

    /**
     * Возвращает строку результата запроса
     *
     * @param PDOStatement $query
     * @param int $fetch_style
     * @return array|false False в случае ошибки
     */
    public static function fetch(PDOStatement &$query, $fetch_style = null)
    {
        try {
            $res = $query->fetch($fetch_style);

            if (! $res) {
                $query->closeCursor();
            }
        } catch (\mysqli_sql_exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }

        return $res;
    }

    /**
     * Возвращает весь результат запроса
     *
     * @param PDOStatement|string $query
     * @param int $fetch_style
     * @return array|false False в случае ошибки
     */
    public static function getAll($query, $fetch_style = null)
    {
        try {
            if (is_string($query)) {
                $query = self::query($query);
            }

            $array = $query->fetchAll($fetch_style);
        } catch (\mysqli_sql_exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }

        return $array;
    }

    public static function value($query, $fetch_style = null)
    {
        try {
            if (is_string($query)) {
                $query = self::query($query);
            }

            $value = $query->fetch($fetch_style);
        } catch (\mysqli_sql_exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }

        return $value;
    }

    /**
     * Экранирует данные
     *
     * Доступные типы:
     * 's' - string, 'i' - integer, 'd' - double, 'a' - array
     *
     * @param mixed $value
     * @param string $type
     * @return mixed
     */
    public static function escape($value, $type = 's')
    {
        if ($type == 's') {
            return self::$db->quote($value);
        } else if ($type == 'i') {
            return intval($value);
        }
        if ($type == 'd') {
            return floatval($value);
        }
        if ($type == 'a') { // Array
            if (! is_array($value)) {
                $value = [];
            }
            return self::$db->quote(serialize($value));
        }

        return $value;
    }

    public static function getLastInsertId()
    {
        return self::$db->lastInsertId();
    }
}

<?php

namespace App;

class Hook
{
    protected $name;
    protected $content;

    public function __construct($name, $content)
    {
        $this->name = $name;
        $this->content = $content;
    }

    public function render()
    {
        return $this->content;
    }
}
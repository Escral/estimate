<?php

namespace App;

use App\Helpers\Export;
use App\Models\Floor;
use App\Models\Item;

class Items
{
    public static $items;

    private static function initiate($ID)
    {
        $item = null;

        switch ($ID) {
            // Радиаторы
            case RADIATOR_STEAL_1:
                return (new Item(RADIATOR_STEAL_1, 'Стальной Тип-33 500x2400'))->setCategoryName('Радиатор');
                break;
            case RADIATOR_STEAL_2:
                return (new Item(RADIATOR_STEAL_2, 'Стальной Тип-22 500x2200'))->setCategoryName('Радиатор');
                break;
            case RADIATOR_ALLUM_3:
                return (new Item(RADIATOR_ALLUM_3, 'Аллюминиевый Тип-22 500x2000'))->setCategoryName('Радиатор');
                break;


            // Труба
            case PIPE_20:
                return (new Item(PIPE_20, 'ППР Ø20'))->setCategoryName('Труба');
                break;
            case PIPE_25:
                return (new Item(PIPE_25, 'ППР Ø25'))->setCategoryName('Труба');
                break;
            case PIPE_32:
                return (new Item(PIPE_32, 'ППР Ø32'))->setCategoryName('Труба');
                break;
            case PIPE_16:
                return new Item(PIPE_16, 'Металлопласт в изоляции Ø16');
                break;

            // Изоляция
            case INSULATION_20:
                return (new Item(INSULATION_20, 'Ø22'))->setCategoryName('Изоляция для ППР');
                break;
            case INSULATION_25:
                return (new Item(INSULATION_25, 'Ø28'))->setCategoryName('Изоляция для ППР');
                break;
            case INSULATION_32:
                return (new Item(INSULATION_32, 'Ø35'))->setCategoryName('Изоляция для ППР');
                break;

            // Углы
            case CORNER_90_20:
                return (new Item(CORNER_90_20, 'Ø20 90°'))->setCategoryName('Угол ППР');
                break;
            case CORNER_90_25:
                return (new Item(CORNER_90_25, 'Ø25 90°'))->setCategoryName('Угол ППР');
                break;
            case CORNER_90_32:
                return (new Item(CORNER_90_32, 'Ø32 90°'))->setCategoryName('Угол ППР');
                break;
            case CORNER_45_20:
                return (new Item(CORNER_45_20, 'Ø20 45°'))->setCategoryName('Угол ППР');
                break;
            case CORNER_45_25:
                return (new Item(CORNER_45_25, 'Ø25 45°'))->setCategoryName('Угол ППР');
                break;
            case CORNER_45_32:
                return (new Item(CORNER_45_32, 'Ø32 45°'))->setCategoryName('Угол ППР');
                break;

            // Муфты
            case COUPLING_20:
                return (new Item(COUPLING_20, 'Ø20'))->setCategoryName('Муфта ППР');
                break;
            case COUPLING_25:
                return (new Item(COUPLING_25, 'Ø25'))->setCategoryName('Муфта ППР');
                break;
            case COUPLING_32:
                return (new Item(COUPLING_32, 'Ø32'))->setCategoryName('Муфта ППР');
                break;

            // Переходы ППР
            case TRANSITION_25_20:
                return (new Item(TRANSITION_25_20, 'Ø25-20'))->setCategoryName('Переход ППР');
                break;
            case TRANSITION_32_25:
                return (new Item(TRANSITION_32_25, 'Ø32-25'))->setCategoryName('Переход ППР');
                break;
            case TRANSITION_32_20:
                return (new Item(TRANSITION_32_20, 'Ø32-20'))->setCategoryName('Переход ППР');
                break;

            // Тройники
            case TEE_20:
                return (new Item(TEE_20, 'Ø20'))->setCategoryName('Тройник ППР');
                break;
            case TEE_25:
                return (new Item(TEE_25, 'Ø25'))->setCategoryName('Тройник ППР');
                break;
            case TEE_32:
                return (new Item(TEE_32, 'Ø32'))->setCategoryName('Тройник ППР');
                break;
            case TEE_25_20_25:
                return (new Item(TEE_25_20_25, 'Ø25x20x25'))->setCategoryName('Тройник ППР');
                break;
            case TEE_25_20_20:
                return (new Item(TEE_25_20_20, 'Ø25x20x20'))->setCategoryName('Тройник ППР');
                break;
            case TEE_32_20_32:
                return (new Item(TEE_32_20_32, 'Ø32x20x32'))->setCategoryName('Тройник ППР');
                break;
            case TEE_32_25_32:
                return (new Item(TEE_32_25_32, 'Ø32x25x32'))->setCategoryName('Тройник ППР');
                break;

            // Краны
            case VALVE_CORNER_IN:
                return new Item(VALVE_CORNER_IN, 'Кран угловой подача');
                break;
            case VALVE_CORNER_OUT:
                return new Item(VALVE_CORNER_OUT, 'Кран угловой обратка');
                break;
            case VALVE_STRAIGHT_IN:
                return new Item(VALVE_STRAIGHT_IN, 'Кран прямой подача');
                break;
            case VALVE_STRAIGHT_OUT:
                return new Item(VALVE_STRAIGHT_OUT, 'Кран прямой обратка');
                break;
            case VALVE_TERMO_CORNER_IN:
                return new Item(VALVE_TERMO_CORNER_IN, 'Термоголовка угловая подача');
                break;
            case VALVE_TERMO_CORNER_OUT:
                return new Item(VALVE_TERMO_CORNER_OUT, 'Термоголовка угловая обратка');
                break;
            case VALVE_TERMO_STRAIGHT_IN:
                return new Item(VALVE_TERMO_STRAIGHT_IN, 'Термоголовка прямая подача');
                break;
            case VALVE_TERMO_STRAIGHT_OUT:
                return new Item(VALVE_TERMO_STRAIGHT_OUT, 'Термоголовка прямая обратка');
                break;
            case VALVES_GERPEX:
                return new Item(VALVES_GERPEX, 'Герпексы для кранов под метталопласт Ø16x1/2');
                break;

            // Ножки для радиаторов
            case LEGS_STEEL:
                return new Item(LEGS_STEEL, 'Ножки для стальных радиаторов');
                break;
            case LEGS_ALUMINUM:
                return new Item(LEGS_ALUMINUM, 'Ножки для алюминиевых (биметаллических) радиаторов');
                break;

            // МП-шки
            case MP_20_1_2_FATHER:
                return new Item(MP_20_1_2_FATHER, 'МП Ø20-1/2" папа');
                break;
            case MP_20_1_2_MOTHER:
                return new Item(MP_20_1_2_MOTHER, 'МП-угловая Ø20-1/2" мама (с ушками)');
                break;

            // Коллектор
            case COLLECTOR_CORNER_1_2_M_F:
                return new Item(COLLECTOR_CORNER_1_2_M_F, 'Угол бронзовый 1" мама-папа');
                break;
            case COLLECTOR_WITH_CONSUM:
                return new Item(COLLECTOR_WITH_CONSUM, 'Коллектор с расходомером');
                break;
            case COLLECTOR_WITHOUT_CONSUM:
                return new Item(COLLECTOR_WITHOUT_CONSUM, 'Коллектор без расходомера');
                break;
            case COLLECTOR_TAILSTOCKS:
                return new Item(COLLECTOR_TAILSTOCKS, 'Концевики на коллектор');
                break;
            case COLLECTOR_VALVES:
                return new Item(COLLECTOR_VALVES, 'Краны с американкой 1" мама-папа');
                break;
            case COLLECTOR_MP:
                return new Item(COLLECTOR_MP, 'МП/ППР 32x1" папа');
                break;
            case COLLECTOR_CORNERS:
                return new Item(COLLECTOR_CORNERS, 'Угол бронзовый 1" мама-папа');
                break;
            case COLLECTOR_GERPEX:
                return new Item(COLLECTOR_GERPEX, 'Герпексы для коллекторов Ø16x3/4');
                break;

            case LOCKER_IN:
                return new Item(LOCKER_IN, 'Встроенный шкаф для коллектора');
                break;
            case LOCKER_OUT:
                return new Item(LOCKER_OUT, 'Наружный шкаф для коллектора');
                break;

            // Работа
            case WORK_RADIATORS:
                return new Item(WORK_RADIATORS, 'Монтаж радиаторов (и полотенцесушителей)');
                break;
            case WORK_DRILL:
                return new Item(WORK_DRILL, 'Перфорация');
                break;
            case WORK_RISER:
                return new Item(WORK_RISER, 'Монтаж стояка');
                break;
            case WORK_HIGHWAY:
                return new Item(WORK_HIGHWAY, 'Прокладка магистрали');
                break;
            case WORK_ADDITIONAL:
                return new Item(WORK_ADDITIONAL, 'Дополнительные работы');
                break;
            case WORK_COLLECTOR:
                return new Item(WORK_COLLECTOR, 'Монтаж коллектора');
                break;
            case WORK_COLLECTOR_LOCKER:
                return new Item(WORK_COLLECTOR_LOCKER, 'Монтаж коллекторного шкафа');
                break;
            case WORK_FJVR:
                return new Item(WORK_FJVR, 'Монтаж FJVR');
                break;

            // Дополонительные материалы
            case ADDITIONAL_SCREWS:
                return new Item(ADDITIONAL_SCREWS, 'Болт с дюбилем под 12 сверло Ø8/100 (как под кодиционеры)');
                break;
            case ADDITIONAL_WIRE:
                return new Item(ADDITIONAL_WIRE, 'Перевязочная проволока');
                break;
            case ADDITIONAL_FOAM:
                return new Item(ADDITIONAL_FOAM, 'Монтажная пена');
                break;
            case ADDITIONAL_CORNER_FIXES:
                return new Item(ADDITIONAL_CORNER_FIXES, 'Угловые фиксаторы');
                break;
            case ADDITIONAL_CLAMPING_1:
                return new Item(ADDITIONAL_CLAMPING_1, 'Хомут со шпилькой Ø под 20 ППР');
                break;
            case ADDITIONAL_CLAMPING_2:
                return new Item(ADDITIONAL_CLAMPING_2, 'Хомут со шпилькой Ø под 25 ППР');
                break;
            case ADDITIONAL_CLAMPING_3:
                return new Item(ADDITIONAL_CLAMPING_3, 'Хомут со шпилькой Ø под 32 ППР');
                break;
            case ADDITIONAL_SELF_SCREWS:
                return new Item(ADDITIONAL_SELF_SCREWS, 'Саморез с дюбилем Ø8/40 как под клипсы (100 шт. в упаковке)');
                break;
            case ADDITIONAL_WALL_INSULATION:
                return new Item(ADDITIONAL_WALL_INSULATION, 'Пристенная изоляция');
                break;

            // Футорки
            case FUTORKA_1:
                return new Item(FUTORKA_1, 'Комплект футорок с доп. креплениями Ø1/2');
                break;



            // Водоснабжение
            // ------------------------------------------------------

            // Приборы
            case PRIBOR_1:
                return new Item(PRIBOR_1, 'Душ');
                break;
            case PRIBOR_2:
                return new Item(PRIBOR_2, 'Ванна');
                break;
            case PRIBOR_3:
                return new Item(PRIBOR_3, 'Умывальник');
                break;
            case PRIBOR_4:
                return new Item(PRIBOR_4, 'Кухонная раковина');
                break;
            case PRIBOR_5:
                return new Item(PRIBOR_5, 'Туалет (унитаз напольный)');
                break;
            case PRIBOR_6:
                return new Item(PRIBOR_6, 'Туалет (инсталяция)');
                break;
            case PRIBOR_7:
                return new Item(PRIBOR_7, 'Стиральная машина');
                break;
            case PRIBOR_8:
                return new Item(PRIBOR_8, 'Посудомоечная машина');
                break;
            case PRIBOR_9:
                return new Item(PRIBOR_9, 'Поливочный кран');
                break;
            case PRIBOR_10:
                return new Item(PRIBOR_10, 'Биде');
                break;
            case PRIBOR_11:
                return new Item(PRIBOR_11, 'Гигиенический душ (жопомойка)');
                break;

            // Заглушки
            case STUB_1_2_FATHER:
                return new Item(STUB_1_2_FATHER, 'Заглушка пластиковая с резинкой (или бронзовая) Ø1/2 папа');
                break;


            // Монтажные работы
            case WORK_PRIBOR_1:
                return (new Item(WORK_PRIBOR_1, 'Душ'))->setCategoryName('Монтаж:');
                break;
            case WORK_PRIBOR_2:
                return (new Item(WORK_PRIBOR_2, 'Ванна'))->setCategoryName('Монтаж:');
                break;
            case WORK_PRIBOR_3:
                return (new Item(WORK_PRIBOR_3, 'Умывальник'))->setCategoryName('Монтаж:');
                break;
            case WORK_PRIBOR_4:
                return (new Item(WORK_PRIBOR_4, 'Кухонная раковина'))->setCategoryName('Монтаж:');
                break;
            case WORK_PRIBOR_5:
                return (new Item(WORK_PRIBOR_5, 'Туалет (унитаз напольный)'))->setCategoryName('Монтаж:');
                break;
            case WORK_PRIBOR_6:
                return (new Item(WORK_PRIBOR_6, 'Туалет (инсталяция)'))->setCategoryName('Монтаж:');
                break;
            case WORK_PRIBOR_7:
                return (new Item(WORK_PRIBOR_7, 'Стиральная машина'))->setCategoryName('Монтаж:');
                break;
            case WORK_PRIBOR_8:
                return (new Item(WORK_PRIBOR_8, 'Посудомоечная машина'))->setCategoryName('Монтаж:');
                break;
            case WORK_PRIBOR_9:
                return (new Item(WORK_PRIBOR_9, 'Поливочный кран'))->setCategoryName('Монтаж:');
                break;
            case WORK_PRIBOR_10:
                return (new Item(WORK_PRIBOR_10, 'Биде'))->setCategoryName('Монтаж:');
                break;
            case WORK_PRIBOR_11:
                return (new Item(WORK_PRIBOR_11, 'Гигиенический душ (жопомойка)'))->setCategoryName('Монтаж:');
                break;




            // Тёплый пол
            // Труба
            case H_PIPE_16:
                return new Item(H_PIPE_16, 'Сшитый полиэтилен Ø16 (PEX/PERT)');
                break;

            // Полистирол
            case POLYSTYR_1:
                return new Item(POLYSTYR_1, 'Полистирол обычный (35 кг/м³)');
                break;
            case POLYSTYR_2:
                return new Item(POLYSTYR_2, 'Полистирол формованный');
                break;

            // Плёнка
            case SKIN_1:
                return new Item(SKIN_1, 'Отражающая плёнка IZOROL');
                break;

            // Крепления изоляции к полу
            case JOIN_1:
                return new Item(JOIN_1, '10*80');
                break;
            case JOIN_2:
                return new Item(JOIN_2, '10*90');
                break;
            case JOIN_3:
                return new Item(JOIN_3, '10*100');
                break;
            case JOIN_4:
                return new Item(JOIN_4, '10*120');
                break;
            case JOIN_5:
                return new Item(JOIN_5, '10*140');
                break;

            // Пена монтаная
            case FOAM_1:
                return new Item(FOAM_1, 'Пена монтажная');
                break;

            // Парашюты
            case PARACHUTE_1:
                return new Item(PARACHUTE_1, 'Парашюты');
                break;

            // FJVR
            case FJVR:
                return new Item(FJVR, 'FJVR клапан');
                break;

            //  Клипсы
            case CLIPS_1:
                return new Item(CLIPS_1, 'Скобы для тёплого пола');
                break;

            // Монтажные работы
            case WORK_HFLOORS:
                return new Item(WORK_HFLOORS, 'Монтаж тёплого пола');
                break;
        }

        return $item;
    }

    public static function get($ID)
    {
//        if (isset(self::$items[$ID])) {
//            return self::$items[$ID];
//        }

        return self::$items[$ID] = self::initiate($ID);
    }

    public static function getWaterGroup($n)
    {
        switch ($n) {
            case 1:
                return [
                    PRIBOR_5,
                    PRIBOR_6,
                    PRIBOR_7,
                    PRIBOR_8,
                    PRIBOR_9
                ];
                break;
            case 2:
                return [
                    PRIBOR_1,
                    PRIBOR_2,
                    PRIBOR_3,
                    PRIBOR_4,
                    PRIBOR_11,
                    PRIBOR_10
                ];
                break;
            default:
                return 0;
        }
    }

    public static function getExitsCount($ID)
    {
        $exits = [
            PRIBOR_5  => 1,
            PRIBOR_6  => 1,
            PRIBOR_7  => 1,
            PRIBOR_8  => 1,
            PRIBOR_9  => 1,

            PRIBOR_1 => 2,
            PRIBOR_2 => 2,
            PRIBOR_3 => 2,
            PRIBOR_4 => 2,
            PRIBOR_10 => 2,
            PRIBOR_11 => 2,
        ];

        return $exits[$ID] ?? false;
    }

    /**
     * @param $items
     * @return int
     */
    public static function getCountOfWaterExits($items)
    {
        $count = 0;

        foreach ($items as $ID => $item) {
            $c = $item['count'] ?? $item;
            if (in_array($ID, self::getWaterGroup(1)))
                $count += (int) $c;
            else if (in_array($ID, self::getWaterGroup(2)))
                $count += (int) $c * 2;
        }

        return $count;
    }

    public static function getRadiatorsCount()
    {
        $items = Data::floor()::get('items');

        if (! is_array($items))
            return 0;

        $sum = 0;

        foreach ($items as $item) {
            if (isset($item['count'])) {
                $sum += $item['count'];
            }
        }

        return $sum;
    }

    public static function afterAdd($items)
    {
        if (is_array($items)) {
            foreach ($items as $item) {
                self::get($item['item'])->setCount($item['count'])->afterAdd();
            }
        }
    }

    public static function stealRadiatorsSelected($floor = null)
    {
        if (! $floor)
            $floor = Floor::getFloor();

        $selected = false;

        if (is_array(Data::floor($floor)::get('items'))) {
            foreach (Data::floor($floor)::get('items') as $item) {
                if (strpos($item['title'], 'Стальной') !== false)
                    $selected = true;
            }
        }

        return $selected;
    }

    public static function aluminumRadiatorsSelected($floor = null)
    {
        if (! $floor)
            $floor = Floor::getFloor();

        $selected = false;

        if (is_array(Data::floor($floor)::get('items'))) {
            foreach (Data::floor($floor)::get('items') as $item) {
                if (strpos($item['title'], 'Алюминиевый') !== false or strpos($item['title'], 'Биметаллический') !== false)
                    $selected = true;
            }
        }

        return $selected;
    }

    public static function getForExport($floors)
    {
        $items = $comments = [];

        if ($floors) {
            foreach ($floors as $section => $sectionFloors) {
                if (! is_array($sectionFloors))
                    continue;

                Export::separator($items);
                Export::section($items, Floor::getSectionTitleByName($section), true);

                foreach ($sectionFloors as $floor_ID => $is) {
                    // Set floor title and separator
                    Export::separator($items);
                    Export::section($items, Floor::getFloorName($floor_ID));

                    if ($section == 'heating') {
                        self::getRadiatorsForExport($items, $comments, $floor_ID);
                    } elseif ($section == 'water') {
                        self::getWaterForExport($items, $comments, $floor_ID);
                    } elseif ($section == 'hfloors') {
                        self::getHFloorsForExport($items, $comments, $floor_ID);
                    } elseif ($section == 'sewage') {
                        self::getSewageForExport($items, $comments, $floor_ID);
                    }
                }
            }
        }

        return [$items, $comments];
    }

    public static function getRadiatorsForExport(&$items, &$comments, $floor)
    {
        $startKey = key($items);

        Data::$prefix2 = 'heating';

        // Radiators
        $arr = Data::floor($floor)::get('items');
        if (is_array($arr) and count($arr)) {
            Export::space($items);
            Export::title($items, 'Радиаторы');
            $items = self::getManyCustom($arr, $items, 'Радиатор');

            self::setComment($items, $comments, $floor, 'items', $arr);
        }

        // Футорки
//        if (Items::getAlluminumRadiatorsCount($floor)) {
        self::getForExportSection($items, $comments, $floor, 'futorki', 'Футорки');
//        }

        self::getForExportSection($items, $comments, $floor, 'legs', 'Ножки для радиаторов');


        $scheme = Data::floor($floor)::get('scheme');
        if ($scheme == 1) {
            // Scheme
            Export::space($items);
            Export::title($items, 'Схема обвязки радиаторов: Двухтрубная на ППР');


            $radiatorsCount = Items::getRadiatorsCount();
            $arr = Data::floor($floor)::get('pipes');
            $arr[PIPE_20] = isset($arr[PIPE_20]) ? (int) $arr[PIPE_20] + $radiatorsCount : $radiatorsCount;

            // Стояк
            $riser = Data::floor($floor)::get('riser');
            if (is_array($riser) and $riser['size'] ?? 0) {
//                Export::space($items);
//                Export::title($items, 'Стояк ППР');
//                $items[] = Items::get($riser['item'])->setCount($riser['size']);
                $arr[$riser['item']] = isset($arr[$riser['item']]) ? (int) $arr[$riser['item']] + $riser['size'] : $riser['size'];
            }

            self::getForExportSection($items, $comments, $floor, 'pipes', 'Трубы ППР', $arr);




            // Подводка к радиаторам
            $supply = Data::floor($floor)::get('supply');
            if (is_array($supply) and $supply['size'] ?? 0) {
                Export::space($items);
                Export::title($items, 'Подводка к радиаторам ППР');
                $items[] = Items::get($supply['item'])->setCount($supply['size']);
            }

            self::getForExportSection($items, $comments, $floor, 'insulation', 'Изоляция для ППР');

            self::getForExportSection($items, $comments, $floor, 'corners', 'Углы ППР');

            self::getForExportSection($items, $comments, $floor, 'couplings', 'Муфты ППР');

            self::getForExportSection($items, $comments, $floor, 'transitions', 'Переходы ППР');

            self::getForExportSection($items, $comments, $floor, 'tees', 'Тройники ППР');

            self::getForExportSection($items, $comments, $floor, 'valves', 'Краны');

            self::getForExportSection($items, $comments, $floor, 'MP', 'МП ППР');

            self::getForExportSection($items, $comments, $floor, 'additional', 'Дополнительные материалы');

            // Work
            $work = Data::floor($floor)::get('work');
            self::getForExportSectionComplicated($items, $comments, $floor, 'work', 'Монтажные работы');
        } else if ($scheme == 2) {
            // Scheme
            Export::space($items);
            Export::title($items, 'Схема обвязки радиаторов: Лучевая на металлопласте');


            self::getForExportSection($items, $comments, $floor, 'pipes', 'Трубы ППР');

            // Стояк
            $riser = Data::floor($floor)::get('riser');
            if (is_array($riser) and $riser['size'] ?? 0) {
                Export::space($items);
                Export::title($items, 'Стояк ППР');
                $items[] = Items::get($riser['item'])->setCount($riser['size']);
            }



            if (is_array($riser) and $riser['size'] ?? 0) {
                self::getForExportSection($items, $comments, $floor, 'insulation_riser', 'Изоляция для стояка ППР');

                self::getForExportSection($items, $comments, $floor, 'corners_riser', 'Углы для стояка ППР');

                self::getForExportSection($items, $comments, $floor, 'couplings_riser', 'Муфты для стояка ППР');
            }

            // Коллектор
            self::collectorForExport($items, $comments, $floor);

            // Шкаф для коллектора
            self::lockerForExport($items, $comments, $floor);


            $gerpex = Data::floor($floor)::get('gerpex');
            if ($gerpex) {
                Export::space($items);
                Export::title($items, 'Герпексы для коллекторов');
                $items[] = Items::get(COLLECTOR_GERPEX)->setCount($gerpex);
                self::setComment($items, $comments, $floor, 'gerpex', 1);
            }

            $gerpex = Data::floor($floor)::get('gerpex_valves');
            if ($gerpex) {
                Export::space($items);
                Export::title($items, 'Герпексы для кранов');
                $items[] = Items::get(VALVES_GERPEX)->setCount($gerpex);
                self::setComment($items, $comments, $floor, 'geprex_valves', 1);
            }

            self::getForExportSection($items, $comments, $floor, 'valves_metal', 'Краны');


            self::getForExportSection($items, $comments, $floor, 'additional_beam', 'Дополнительные материалы');

            // Work
            $work = Data::floor($floor)::get('work_beam');
            self::getForExportSectionComplicated($items, $comments, $floor, 'work_beam', 'Монтажные работы');
        }
    }


    public static function getWaterForExport(&$items, &$comments, $floor)
    {
        $startKey = key($items);

        Data::$prefix2 = 'water';

//        $arr = Data::floor($floor)::get('items');
//        if (is_array($arr) and count($arr)) {
//            Export::space($items);
//            Export::title($items, 'Приборы');
//
//            $c = count($items);
//            $items = self::getMany($arr, $items);
//            $c = count($items) - $c;
//
//            self::setComment($items, $comments, $floor, 'items', $c);
//        }


        $scheme = Data::floor($floor)::get('scheme');
        if ($scheme == 1) {
            // Scheme
            Export::space($items);
            Export::title($items, 'Схема разводки: Двухтрубная на ППР');




            $exitsCount = Items::getCountOfWaterExits((array) Data::floor()::get('items'));
            $arr = Data::floor($floor)::get('pipes');
            $arr[PIPE_20] = isset($arr[PIPE_20]) ? (int) $arr[PIPE_20] + $exitsCount : $exitsCount;

            // Стояк
            $riser = Data::floor($floor)::get('riser');
            if (is_array($riser) and $riser['size'] ?? 0) {
//                Export::space($items);
//                Export::title($items, 'Стояк ППР');
//                $items[] = Items::get($riser['item'])->setCount($riser['size']);
                $arr[$riser['item']] = isset($arr[$riser['item']]) ? (int) $arr[$riser['item']] + $riser['size'] : $riser['size'];
            }

            self::getForExportSection($items, $comments, $floor, 'pipes', 'Трубы ППР', $arr);


            self::getForExportSection($items, $comments, $floor, 'insulation', 'Изоляция для ППР');

            self::getForExportSection($items, $comments, $floor, 'corners', 'Углы ППР');

            self::getForExportSection($items, $comments, $floor, 'couplings', 'Муфты ППР');

            self::getForExportSection($items, $comments, $floor, 'transitions', 'Переходы ППР');

            self::getForExportSection($items, $comments, $floor, 'tees', 'Тройники ППР');

            self::getForExportSection($items, $comments, $floor, 'MP', 'МП ППР');

            self::getForExportSection($items, $comments, $floor, 'stub', 'Заглушки');

            self::getForExportSection($items, $comments, $floor, 'additional', 'Дополнительные материалы');

            // Work
            self::getForExportSectionComplicated($items, $comments, $floor, 'work', 'Монтажные работы');
        }
    }

    public static function getHFloorsForExport(&$items, &$comments, $floor)
    {
        $startKey = key($items);

        Data::$prefix2 = 'hfloors';

        $scheme = Data::floor($floor)::get('scheme');

        if ($scheme == 1) {

            // Scheme
            Export::space($items);
            Export::title($items, 'Схема укладки: Коллекторная разводка');


            // Стояк
            $riser = Data::floor($floor)::get('riser');
            if (is_array($riser) and $riser['size'] ?? 0) {
                Export::space($items);
                Export::title($items, 'Стояк ППР');
                $items[] = Items::get($riser['item'])->setCount($riser['size']);
            }


            if (is_array($riser) and $riser['size'] ?? 0) {
                self::getForExportSection($items, $comments, $floor, 'insulation_riser', 'Изоляция для стояка ППР');

                self::getForExportSection($items, $comments, $floor, 'corners_riser', 'Углы для стояка ППР');

                self::getForExportSection($items, $comments, $floor, 'couplings_riser', 'Муфты для стояка ППР');
            }


            // Area
            Export::space($items);
            Export::title($items, 'Площадь: ' . Data::floor($floor)::get('area') . ' м');
            self::setComment($items, $comments, $floor, 'area', 0);


            // Трубы
            self::getForExportSection($items, $comments, $floor, 'pipes', 'Трубы');


            // Custom Radio
            $arr = Data::floor($floor)::get('insulation');
            if (is_array($arr) and count($arr)) {
                Export::space($items);
                Export::title($items, 'Изоляция');
                $c = count($items);
                if ($arr['item'] == 0) {
                    $items[] = Items::get(POLYSTYR_1)->setCount($arr[0]['count'])->appendTitle($arr[0]['width'] . 'x' . $arr[0]['lenght'] . 'мм ' . $arr[0]['thickness'] . 'см');
                } else if ($arr['item'] == 1) {
                    $items[] = Items::get(POLYSTYR_2)->setCount($arr[1]['count'])->appendTitle($arr[1]['width'] . 'x' . $arr[1]['lenght'] . 'мм ' . $arr[1]['thickness'] . 'см');
                }

                $c = count($items) - $c;

                self::setComment($items, $comments, $floor, 'insulation', $c);
            }
            // ------

            self::getForExportSection($items, $comments, $floor, 'skin', 'Пленка');

            // Custom Radio
            $arr = Data::floor($floor)::get('insulation_join');
            if (is_array($arr) and count($arr)) {
                Export::space($items);
                Export::title($items, 'Крепления изоляции к полу');
                $c = count($items);
                if ($arr['item'] == 1) {
                    $items[] = Items::get(PARACHUTE_1)->setCount($arr[0]['count'])->appendTitle($arr[0]['per_list'] . 'шт на лист ' . Items::get($arr[0]['size'])->title . 'мм');
                } else if ($arr['item'] == 2) {
                    $items[] = Items::get(FOAM_1)->setCount($arr[1]['count']);
                }

                $c = count($items) - $c;

                self::setComment($items, $comments, $floor, 'insulation_join', $c);
            }
            // ------


            self::getForExportSection($items, $comments, $floor, 'clips', 'Скобы');


            // Коллектор
            self::collectorForExport($items, $comments, $floor);

            // Шкаф для коллектора
            self::lockerForExport($items, $comments, $floor);


            $gerpex = Data::floor($floor)::get('gerpex');
            if ($gerpex) {
                Export::space($items);
                Export::title($items, 'Герпексы для коллекторов');
                $items[] = Items::get(COLLECTOR_GERPEX)->setCount($gerpex);
                self::setComment($items, $comments, $floor, 'gerpex', 1);
            }


            $corner_fixes = Data::floor($floor)::get('corner_fixes');
            if ($corner_fixes) {
                Export::space($items);
                Export::title($items, 'Угловые фиксаторы');
                $items[] = Items::get(ADDITIONAL_CORNER_FIXES)->setCount($corner_fixes);
                self::setComment($items, $comments, $floor, 'corner_fixes', 1);
            }
        } elseif ($scheme == 2) {

            // Scheme
            Export::space($items);
            Export::title($items, 'Схема укладки: FJVR клапан');


            // Area
            Export::space($items);
            Export::title($items, 'Площадь: ' . Data::floor($floor)::get('area') . ' м');
            self::setComment($items, $comments, $floor, 'area', 0);


            // Трубы
            self::getForExportSection($items, $comments, $floor, 'pipes', 'Трубы');


            // Custom Radio
            $arr = Data::floor($floor)::get('insulation');
            if (is_array($arr) and count($arr)) {
                Export::space($items);
                Export::title($items, 'Изоляция');
                $c = count($items);
                if ($arr['item'] == 1) {
                    $items[] = Items::get(POLYSTYR_1)->setCount($arr[0]['count'])->appendTitle($arr[0]['width'] . 'x' . $arr[0]['lenght'] . 'мм ' . $arr[0]['thickness'] . 'см');
                } else if ($arr['item'] == 2) {
                    $items[] = Items::get(POLYSTYR_2)->setCount($arr[1]['count'])->appendTitle($arr[1]['width'] . 'x' . $arr[1]['lenght'] . 'мм ' . $arr[1]['thickness'] . 'см');
                }

                $c = count($items) - $c;

                self::setComment($items, $comments, $floor, 'insulation', $c);
            }
            // ------

            self::getForExportSection($items, $comments, $floor, 'skin', 'Пленка');

            // Custom Radio
            $arr = Data::floor($floor)::get('insulation_join');
            if (is_array($arr) and count($arr)) {
                Export::space($items);
                Export::title($items, 'Крепления изоляции к полу');
                $c = count($items);
                if ($arr['item'] == 1) {
                    $items[] = Items::get(PARACHUTE_1)->setCount($arr[0]['count'])->appendTitle($arr[0]['per_list'] . 'шт на лист ' . Items::get($arr[0]['size'])->title . 'мм');
                } else if ($arr['item'] == 2) {
                    $items[] = Items::get(FOAM_1)->setCount($arr[1]['count']);
                }

                $c = count($items) - $c;

                self::setComment($items, $comments, $floor, 'insulation_join', $c);
            }
            // ------

            // Гарпуны
            self::getForExportSection($items, $comments, $floor, 'clips', 'Гарпуны');

            $fjvr = Data::floor($floor)::get('fjvr');
            if ($fjvr) {
                Export::space($items);
                Export::title($items, 'FJVR клапан');
                $items[] = Items::get(FJVR)->setCount($fjvr);
                self::setComment($items, $comments, $floor, 'fjvr', 1);
            }

            $gerpex = Data::floor($floor)::get('gerpex');
            if ($gerpex) {
                Export::space($items);
                Export::title($items, 'Герпексы для коллекторов');
                $items[] = Items::get(COLLECTOR_GERPEX)->setCount($gerpex);
                self::setComment($items, $comments, $floor, 'gerpex', 1);
            }


            $corner_fixes = Data::floor($floor)::get('corner_fixes');
            if ($corner_fixes) {
                Export::space($items);
                Export::title($items, 'Угловые фиксаторы');
                $items[] = Items::get(ADDITIONAL_CORNER_FIXES)->setCount($corner_fixes);
                self::setComment($items, $comments, $floor, 'corner_fixes', 1);
            }
        }


        self::getForExportSection($items, $comments, $floor, 'additional', 'Дополнительные материалы');

        // Work
        self::getForExportSectionComplicated($items, $comments, $floor, 'work', 'Монтажные работы');
    }

    public static function getSewageForExport(&$items, &$comments, $floor)
    {
        Data::$prefix2 = 'sewage';
        $selected = Data::floor($floor)::get('items');
        $arr = array_merge(...Items::getSewageItems($selected));

        foreach ($arr as $item) {
            if ($item->getCount() > 0)
                $items[] = $item;
        }
    }

    public static function getForExportSection(&$items, &$comments, $floor, $key, $title, $arr = null)
    {
        $arr = $arr ?? Data::floor($floor)::get($key);
        if (is_array($arr) and count($arr)) {
            Export::space($items);
            Export::title($items, $title);
            $c = count($items);
            $items = self::getMany($arr, $items);
            $c = count($items) - $c;

            self::setComment($items, $comments, $floor, $key, $c);
        }
    }

    public static function setComment($items, &$comments, $floor, $key, $arr)
    {
        $comment = Data::floor($floor)::get($key . '.comment');
        if ($comment) {
            $size = is_array($arr) ? count($arr) : (int) $arr;
            end($items);
            $offset = key($items) - $size + 2;
            $comments[$offset] = [
                'size' => $size,
                'text' => $comment
            ];
        }
    }

    public static function getForExportSectionComplicated(&$items, &$comments, $floor, $key, $title)
    {
        $arr = Data::floor($floor)::get($key);
        if (is_array($arr) and count($arr)) {
            Export::space($items);
            Export::title($items, $title);
            $c = count($items);
            $items = self::getManyComplicated($arr, $items);
            $c = count($items) - $c;

            self::setComment($items, $comments, $floor, $key, $c);
        }
    }

    /**
     * @param $arr
     * @param $items
     * @return array
     */
    public static function getMany($arr, $items) : array
    {
        if (is_array($arr)) {
            foreach ($arr as $ID => $count) {
                if ($count or $ID == WORK_ADDITIONAL)
                    $items[] = Items::get($ID)->setCount($count);
            }
        }
        return $items;
    }

    /**
     * @param $arr
     * @param $items
     * @param null $categoryName
     * @return array
     */
    public static function getManyCustom($arr, $items, $categoryName = null) : array
    {
        if (is_array($arr)) {
            $n = rand(0, 1000);
            foreach ($arr as $key => $item) {
                if ($item['count'] ?? 0) {
                    $obj = (new Item(($item['title'] ?? ''), ($item['title'] ?? ''), $item['count'], $item['price'] ?? 0))->setSections($item['sections'] ?? 1)->setCategoryName($categoryName);
                }

                if ($obj->withSections()) {
                    $obj->title .= ' ' . $obj->getSections() . ' секций';
                    $obj->id = $obj->title;
                }

                $items[] = $obj;
                $n++;
            }
        }
        return $items;
    }

    /**
     * @param $arr
     * @param $items
     * @return array
     */
    public static function getManyComplicated($arr, $items) : array
    {
        if (is_array($arr)) {
            foreach ($arr as $key => $item) {
                $ID = $item['item'] ?? $key;
                if (($item['count'] ?? 0) or ($ID == WORK_ADDITIONAL and $item['price'] > 0))
                    $items[] = Items::get($ID)->setCount($item['count'] ?? 0)->setPrice($item['price'] ?? 0);
            }
        }
        return $items;
    }

    public static function getAlluminumRadiatorsCount($floor = null)
    {
        if (! $floor)
            $floor = Floor::getFloor();

        if (! is_array(Data::floor($floor)::get('items')))
            return 0;

        $sum = 0;

        if (is_array(Data::floor($floor)::get('items'))) {
            foreach (Data::floor($floor)::get('items') as $item) {
                if (strpos($item['title'], 'Алюминиевый') !== false or strpos($item['title'], 'Биметаллический') !== false)
                    if (isset($item['count'])) {
                        $sum += $item['count'];
                    }
            }
        }

        return $sum;
    }

    public static function getLengthPodvodki($type = 'radiators')
    {
        $length = 0;

        $items = Data::floor()::get('items');

        if ($type == 'radiators') {
            $length = Items::getRadiatorsCount();
        } else {
            $length += (int) $items[PRIBOR_1] * 3;
            $length += (int) $items[PRIBOR_2] * 2;
            $length += (int) $items[PRIBOR_3] * 2;
            $length += (int) $items[PRIBOR_4] * 2;
            $length += (int) $items[PRIBOR_5] * 1;
            $length += (int) $items[PRIBOR_6] * 2;
            $length += (int) $items[PRIBOR_7] * 1;
            $length += (int) $items[PRIBOR_8] * 1;
            $length += (int) $items[PRIBOR_9] * 1;
            $length += (int) $items[PRIBOR_10] * 2;
            $length += (int) $items[PRIBOR_11] * 3;
        }

        return $length;
    }

    public static function getSewageItems($selected = [], $type = null)
    {
        $items110 = [
            501 => [
                'id'    => 501,
                'title' => 'Труба Ø110 L=250',
            ],
            502 => [
                'id'    => 502,
                'title' => 'Труба Ø110 L=500',
            ],
            503 => [
                'id'    => 503,
                'title' => 'Труба Ø110 L=1000',
            ],
            504 => [
                'id'    => 504,
                'title' => 'Труба Ø110 L=2000',
            ],
            505 => [
                'id'    => 505,
                'title' => 'Труба Ø110 L=3000',
            ],


            506 => [
                'id'    => 506,
                'title' => 'Крестовина одноплоскостная Ø110 45°',
            ],
            5066 => [
                'id'    => 5066,
                'title' => 'Крестовина одноплоскостная Ø110 90°',
            ],
            507 => [
                'id'    => 507,
                'title' => 'Крестовина двухплоскостная Ø110 90°',
            ],
            508 => [
                'id'    => 508,
                'title' => 'Крестовина двухплоскостная Ø110-50-110',
            ],
            // 509 => [
            //     'id'    => 509,
            //     'title' => 'Крестовина двухплоскостная Ø110-110-50',
            // ],


            510 => [
                'id'    => 510,
                'title' => 'Редукция Ø110-50',
            ],


            511 => [
                'id'    => 511,
                'title' => 'Муфта Ø110',
            ],


            512 => [
                'id'    => 512,
                'title' => 'Компенсатор Ø110',
            ],


            513 => [
                'id'    => 513,
                'title' => 'Угол Ø110x15°',
            ],
            514 => [
                'id'    => 514,
                'title' => 'Угол Ø110x30°',
            ],
            515 => [
                'id'    => 515,
                'title' => 'Угол Ø110x45°',
            ],
            516 => [
                'id'    => 516,
                'title' => 'Угол Ø110x90°',
            ],


            517 => [
                'id'    => 517,
                'title' => 'Аэратор Ø110',
            ],


            518 => [
                'id'    => 518,
                'title' => 'Заглушка Ø110',
            ],


            519 => [
                'id'    => 519,
                'title' => 'Тройник косой Ø110x45°',
            ],
            520 => [
                'id'    => 520,
                'title' => 'Тройник косой Ø110-Ø50x45°',
            ],
            521 => [
                'id'    => 521,
                'title' => 'Тройник прямой Ø110x90°',
            ],
            522 => [
                'id'    => 522,
                'title' => 'Тройник прямой Ø110-Ø50x90°',
            ],


            523 => [
                'id'    => 523,
                'title' => 'Обратный клапан Ø110',
            ],


            524 => [
                'id'    => 524,
                'title' => 'Ревизия Ø110',
            ],


            525 => [
                'id'    => 525,
                'title' => 'Хомут со шпилькой Ø110 (106-111мм)',
            ],
        ];

        $items50 = [
            526 => [
                'id'    => 526,
                'title' => 'Труба Ø50 L=250',
            ],
            527 => [
                'id'    => 527,
                'title' => 'Труба Ø50 L=500',
            ],
            528 => [
                'id'    => 528,
                'title' => 'Труба Ø50 L=1000',
            ],
            529 => [
                'id'    => 529,
                'title' => 'Труба Ø50 L=2000',
            ],
            530 => [
                'id'    => 530,
                'title' => 'Труба Ø50 L=3000',
            ],


            531 => [
                'id'    => 531,
                'title' => 'Крестовина одноплоскостная Ø50 45°'
            ],
            581 => [
                'id'    => 581,
                'title' => 'Крестовина одноплоскостная Ø50 90°',
            ],



            532 => [
                'id'    => 532,
                'title' => 'Компенсатор Ø50',
            ],


            533 => [
                'id'    => 533,
                'title' => 'Угол Ø50x15°',
            ],
            534 => [
                'id'    => 534,
                'title' => 'Угол Ø50x30°',
            ],
            535 => [
                'id'    => 535,
                'title' => 'Угол Ø50x45°',
            ],
            536 => [
                'id'    => 536,
                'title' => 'Угол Ø50x90°',
            ],


            537 => [
                'id'    => 537,
                'title' => 'Аэратор Ø50',
            ],


            538 => [
                'id'    => 538,
                'title' => 'Заглушка Ø50',
            ],


            539 => [
                'id'    => 539,
                'title' => 'Тройник косой Ø50x45°',
            ],
            540 => [
                'id'    => 540,
                'title' => 'Тройник прямой Ø50x90°',
            ],


            541 => [
                'id'    => 541,
                'title' => 'Хомут со шпилькой Ø50 (58-62мм)',
            ],


            542 => [
                'id'    => 542,
                'title' => 'Переход резиновый Ø50-40',
            ],
            543 => [
                'id'    => 543,
                'title' => 'Переход резиновый Ø50-32',
            ],
        ];

        $items32 = [
            544 => [
                'id'    => 544,
                'title' => 'Труба Ø32 L=250',
            ],
            545 => [
                'id'    => 545,
                'title' => 'Труба Ø32 L=500',
            ],
            546 => [
                'id'    => 546,
                'title' => 'Труба Ø32 L=1000',
            ],


            547 => [
                'id'    => 547,
                'title' => 'Редукция Ø50-32',
            ],


            548 => [
                'id'    => 548,
                'title' => 'Угол Ø32x15°',
            ],
            549 => [
                'id'    => 549,
                'title' => 'Угол Ø32x30°',
            ],
            550 => [
                'id'    => 550,
                'title' => 'Угол Ø32x45°',
            ],
            551 => [
                'id'    => 551,
                'title' => 'Угол Ø32x90°',
            ],


            552 => [
                'id'    => 552,
                'title' => 'Заглушка Ø32',
            ],


            553 => [
                'id'    => 553,
                'title' => 'Тройник косой Ø32x45°',
            ],
            554 => [
                'id'    => 554,
                'title' => 'Тройник прямой Ø32x90°',
            ],


            555 => [
                'id'    => 555,
                'title' => 'Хомут со шпилькой Ø32 (40-45мм)',
            ],
        ];

        $items = [
            556 => [
                'id'    => 556,
                'title' => 'Смазка',
            ]
        ];

        $itemsTmp = [];
        foreach ($items110 as $item) {
            $itemsTmp[] = new Item($item['id'], $item['title'], ($selected[$item['id']] ?? 0));
        }
        $items110 = $itemsTmp;

        $itemsTmp = [];
        foreach ($items50 as $item) {
            $itemsTmp[] = new Item($item['id'], $item['title'], ($selected[$item['id']] ?? 0));
        }
        $items50 = $itemsTmp;

        $itemsTmp = [];
        foreach ($items32 as $item) {
            $itemsTmp[] = new Item($item['id'], $item['title'], ($selected[$item['id']] ?? 0));
        }
        $items32 = $itemsTmp;

        $itemsTmp = [];
        foreach ($items as $item) {
            $itemsTmp[] = new Item($item['id'], $item['title'], ($selected[$item['id']] ?? 0));
        }
        $items = $itemsTmp;

        if ($type == '110')
            return $items110;
        if ($type == '50')
            return $items50;
        if ($type == '32')
            return $items32;
        if ($type == 'other')
            return $items;

        return [$items110, $items50, $items32, $items];
    }

    public static function getToolsItems($selected = [])
    {
        $itemsTmp = [
            601 => [
                'id'    => 601,
                'title' => 'Большой перфоратор (Hilti TE76)',
            ],
            602 => [
                'id'    => 602,
                'title' => 'Маленький перфоратор (Machita, Bosch)',
            ],
            603 => [
                'id'    => 603,
                'title' => 'Большая болгарка (Hilti, Hitachi)',
            ],
            604 => [
                'id'    => 604,
                'title' => 'Маленькая болгарка (Hilti, Bosch)',
            ],
            605 => [
                'id'    => 605,
                'title' => 'Паяльник ППР',
            ],
            606 => [
                'id'    => 606,
                'title' => 'Паяльник ППР по большим диаметрам (32мм, 40мм, 50мм)',
            ],
            607 => [
                'id'    => 607,
                'title' => 'Матрица 20 ППР',
            ],
            608 => [
                'id'    => 608,
                'title' => 'Матрица 25 ППР',
            ],
            609 => [
                'id'    => 609,
                'title' => 'Матрица 32 ППР',
            ],
            610 => [
                'id'    => 610,
                'title' => 'Ножницы ППР',
            ],
            611 => [
                'id'    => 611,
                'title' => 'Уровень (пузырьковый, ветерпас)',
            ],
            612 => [
                'id'    => 612,
                'title' => 'Диск по бетону 230 (большой)',
            ],
            613 => [
                'id'    => 613,
                'title' => 'Диск по бетону 125 (маленький)',
            ],
            614 => [
                'id'    => 614,
                'title' => 'Сверло SDS-Max D40мм',
            ],
            635 => [
                'id'    => 635,
                'title' => 'Сверло SDS-Max D28мм',
            ],
            615 => [
                'id'    => 615,
                'title' => 'Пылесос строительный',
            ],
            616 => [
                'id'    => 616,
                'title' => 'Компрессор воздушный',
            ],
            617 => [
                'id'    => 617,
                'title' => 'Опресовщик',
            ],
            618 => [
                'id'    => 618,
                'title' => 'Удлинитель',
            ],
            619 => [
                'id'    => 619,
                'title' => 'Шуруповёрт (Bosh, Hitachi)',
            ],
            620 => [
                'id'    => 620,
                'title' => 'Трубогиб пружинный (внутренний) D12мм',
            ],
            621 => [
                'id'    => 621,
                'title' => 'Лобзик',
            ],
            622 => [
                'id'    => 622,
                'title' => 'Сабельная пила (Bosch)',
            ],
            623 => [
                'id'    => 623,
                'title' => 'Горелка по меди (Bernzomatik)',
            ],
            624 => [
                'id'    => 624,
                'title' => 'Горелка Rothenberger + балон на 8л',
            ],
            625 => [
                'id'    => 625,
                'title' => 'Припой медный мягкий',
            ],
            636 => [
                'id'    => 636,
                'title' => 'Припой медный твёрдый',
            ],
            626 => [
                'id'    => 626,
                'title' => 'Флюз для меди',
            ],
            627 => [
                'id'    => 627,
                'title' => 'Шурфики (18мм, 22мм, 28мм)',
            ],
            628 => [
                'id'    => 628,
                'title' => 'Губки для зачистки меди',
            ],
            629 => [
                'id'    => 629,
                'title' => 'Мусорные мешки',
            ],
            630 => [
                'id'    => 630,
                'title' => 'Совок/Лопата совковая',
            ],
            631 => [
                'id'    => 631,
                'title' => 'Веник',
            ],
            632 => [
                'id'    => 632,
                'title' => 'Шейвер для зачистки трубы D20',
            ],
            633 => [
                'id'    => 633,
                'title' => 'Шейвер для зачистки трубы D25',
            ],
            634 => [
                'id'    => 634,
                'title' => 'Шейвер для зачистки трубы D32',
            ],
        ];

        $items = [];

        foreach ($itemsTmp as $item) {
            $i = new Item($item['id'], $item['title'], ($selected[$item['id']] ?? 0));
            $items[] = $i;
            self::$items[$item['id']] = $i;
        }

        return $items;
    }

    public static function getKettlePiping($type = null, $count = 1, $selected = [])
    {
        $itemsTmp = [
            1 => [
                801 => [
                    'id'    => 801,
                    'title' => 'Товар 1',
                    'count' => 1
                ],
                802 => [
                    'id'    => 802,
                    'title' => 'Товар 2',
                    'count' => 2
                ],
                803 => [
                    'id'    => 803,
                    'title' => 'Товар 3',
                ],
                804 => [
                    'id'    => 804,
                    'title' => 'Товар 4',
                ],
                805 => [
                    'id'    => 805,
                    'title' => 'Товар 5',
                ],
                806 => [
                    'id'    => 806,
                    'title' => 'Товар 6',
                ],
                807 => [
                    'id'    => 807,
                    'title' => 'Товар 7',
                ],
                808 => [
                    'id'    => 808,
                    'title' => 'Товар 8',
                ],
                809 => [
                    'id'    => 809,
                    'title' => 'Товар 9',
                ],
                810 => [
                    'id'    => 810,
                    'title' => 'Товар 10',
                ]
            ],


            2 => [
                851 => [
                    'id'    => 851,
                    'title' => 'Товар 11',
                    'count' => 5
                ],
                852 => [
                    'id'    => 852,
                    'title' => 'Товар 12',
                    'count' => 4
                ],
                853 => [
                    'id'    => 853,
                    'title' => 'Товар 13',
                ],
            ]
        ];

        $items = [];

        foreach ($itemsTmp as $key => $group) {
            if ($type === null or $type === $key) {
                foreach ($group as $item) {
                    $i = new Item($item['id'], $item['title'], ($selected[$item['id']]['count'] ?? (($item['count'] ?? 0) * $count)));
                    $items[] = $i;
                    self::$items[$item['id']] = $i;
                }
            }
        }

        return $items;
    }

    public static function getKettlePipingTitle($key)
    {
        $titles = [
            1 => 'Обвязка двухконтурного котла бронзой и ППР',
            2 => 'Обвязка двухконтурного котла бронзой и ППР 2',
            3 => 'Обвязка двухконтурного котла бронзой и ППР 3'
        ];

        return $titles[$key];
    }

    public static function getBoilerPiping($type = null, $count = 1, $selected = [])
    {
        $itemsTmp = [
            1 => [
                801 => [
                    'id'    => 801,
                    'title' => 'Товар 1',
                    'count' => 1
                ],
                802 => [
                    'id'    => 802,
                    'title' => 'Товар 2',
                    'count' => 2
                ],
                803 => [
                    'id'    => 803,
                    'title' => 'Товар 3',
                ],
                804 => [
                    'id'    => 804,
                    'title' => 'Товар 4',
                ],
                805 => [
                    'id'    => 805,
                    'title' => 'Товар 5',
                ],
                806 => [
                    'id'    => 806,
                    'title' => 'Товар 6',
                ],
                807 => [
                    'id'    => 807,
                    'title' => 'Товар 7',
                ],
                808 => [
                    'id'    => 808,
                    'title' => 'Товар 8',
                ],
                809 => [
                    'id'    => 809,
                    'title' => 'Товар 9',
                ],
                810 => [
                    'id'    => 810,
                    'title' => 'Товар 10',
                ]
            ],


            2 => [
                851 => [
                    'id'    => 851,
                    'title' => 'Товар 11',
                    'count' => 5
                ],
                852 => [
                    'id'    => 852,
                    'title' => 'Товар 12',
                    'count' => 4
                ],
                853 => [
                    'id'    => 853,
                    'title' => 'Товар 13',
                ],
            ]
        ];

        $items = [];

        foreach ($itemsTmp as $key => $group) {
            if ($type === null or $type === $key) {
                foreach ($group as $item) {
                    $i = new Item($item['id'], $item['title'], ($selected[$item['id']]['count'] ?? (($item['count'] ?? 0) * $count)));
                    $items[] = $i;
                    self::$items[$item['id']] = $i;
                }
            }
        }

        return $items;
    }

    public static function getBoilerPipingTitle($key)
    {
        $titles = [
            1 => 'Обвязка бойлера бронзой и ППР',
            2 => 'Обвязка бойлера бронзой и ППР 2',
        ];

        return $titles[$key];
    }

    public static function getCollectorPiping($type = null, $count = 1, $selected = [])
    {
        $itemsTmp = [
            1 => [
                801 => [
                    'id'    => 801,
                    'title' => 'Товар 1',
                    'count' => 1
                ],
                802 => [
                    'id'    => 802,
                    'title' => 'Товар 2',
                    'count' => 2
                ],
                803 => [
                    'id'    => 803,
                    'title' => 'Товар 3',
                ],
                804 => [
                    'id'    => 804,
                    'title' => 'Товар 4',
                ],
                805 => [
                    'id'    => 805,
                    'title' => 'Товар 5',
                ],
                806 => [
                    'id'    => 806,
                    'title' => 'Товар 6',
                ],
                807 => [
                    'id'    => 807,
                    'title' => 'Товар 7',
                ],
                808 => [
                    'id'    => 808,
                    'title' => 'Товар 8',
                ],
                809 => [
                    'id'    => 809,
                    'title' => 'Товар 9',
                ],
                810 => [
                    'id'    => 810,
                    'title' => 'Товар 10',
                ]
            ],


            2 => [
                851 => [
                    'id'    => 851,
                    'title' => 'Товар 11',
                    'count' => 5
                ],
                852 => [
                    'id'    => 852,
                    'title' => 'Товар 12',
                    'count' => 4
                ],
                853 => [
                    'id'    => 853,
                    'title' => 'Товар 13',
                ],
            ]
        ];

        $items = [];

        foreach ($itemsTmp as $key => $group) {
            if ($type === null or $type === $key) {
                foreach ($group as $item) {
                    $i = new Item($item['id'], $item['title'], ($selected[$item['id']]['count'] ?? (($item['count'] ?? 0) * $count)));
                    $items[] = $i;
                    self::$items[$item['id']] = $i;
                }
            }
        }

        return $items;
    }

    public static function getCollectorPipingTitle($key)
    {
        $titles = [
            1 => 'Группа 1',
            2 => 'Группа 2',
        ];

        return $titles[$key];
    }

    public static function collectorForExport(&$items, &$comments, $floor)
    {
        $collector = Data::floor($floor)::get('collector');
        if ($collector) {
            Export::space($items);
            Export::title($items, 'Коллектор');
            $col = Items::get(isset($collector['flowmeter']) && $collector['flowmeter'] ? COLLECTOR_WITH_CONSUM : COLLECTOR_WITHOUT_CONSUM)->setCount(1);
            $col->setTitle($col->title . ' на ' . $collector . ' выходов');
            $items[] = $col;

            $count = 1;

            $tailstocks = Data::floor($floor)::get('tailstocks');
            $items[] = Items::get(COLLECTOR_TAILSTOCKS)->setCount($tailstocks);
            $count += ($tailstocks ? 1 : 0);

            $valves = Data::floor($floor)::get('col_valves');
            $items[] = Items::get(COLLECTOR_VALVES)->setCount($valves);
            $count += ($valves ? 1 : 0);

            $mp = Data::floor($floor)::get('col_mp');
            $items[] = Items::get(COLLECTOR_MP)->setCount($mp);
            $count += ($mp ? 1 : 0);

            $corners = Data::floor($floor)::get('col_corners');
            $items[] = Items::get(COLLECTOR_CORNERS)->setCount($corners);
            $count += ($corners ? 1 : 0);

            self::setComment($items, $comments, $floor, 'collector', $count);
        }
    }
    public static function lockerForExport(&$items, &$comments, $floor)
    {
        $locker = Data::floor($floor)::get('locker');
        if ($locker and $locker['isset']) {
            Export::space($items);
            Export::title($items, 'Шкаф для коллектора');
            $item = Items::get($locker['type'] == 1 ? LOCKER_IN : LOCKER_OUT)->setCount(1);
            $item->setTitle($item->title . ' шириной ' . $locker['size'] . ' см');
            $items[] = $item;

            self::setComment($items, $comments, $floor, 'locker', 1);
        }
    }
}
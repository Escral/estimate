<?php

namespace App;

class Pagination
{
    private $url;
    private $total;
    private $perPage;
    private $page;

    public function __construct($total, $perPage, $page = 1)
    {
        $this->total = $total;
        $this->perPage = $perPage;
        $this->page = $page;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param string $page
     * @param bool $explicit
     * @return string
     */
    public function getPageUrl($page, $explicit = false)
    {
        return $this->getUrl() . ((! $explicit and $page == 1) ? '' : ($this->url == '/' ? '' : '/') . $page);
    }

    /**
     * @return bool
     */
    public function hasPages()
    {
        return $this->total > $this->perPage;
    }

    /**
     * @return array
     */
    public function pages()
    {
        $pagesCount = ceil($this->total / $this->perPage);

        return range(1, $pagesCount);
    }

    /**
     * @param bool $url
     * @return bool|int|string
     */
    public function prev($url = false)
    {
        if ($this->page == 1)
            return false;

        $page = $this->page - 1;

        if ($url)
            return $this->getPageUrl($page);

        return $page;
    }

    /**
     * @param bool $url
     * @return bool|int|string
     */
    public function next($url = false)
    {
        $pagesCount = ceil($this->total / $this->perPage);

        if ($this->page == $pagesCount)
            return false;

        $page = $this->page + 1;

        if ($url)
            return $this->getPageUrl($page);

        return $page;
    }

    /**
     * @param int $page
     * @return bool
     */
    public function isActive($page)
    {
        return $this->page == $page;
    }
}
<?php

namespace App\Traits;

use App\Database;

trait Findable
{
    public static function findAsArray($table, $value, $fields = false)
    {
        $query = array();

        $fields = ($fields !== false) ? $fields : ['ID'];

        foreach ($fields as $field) {
            if ($field == 'ID' and ! preg_match('#^[0-9]+$#isu', $value))
                continue;

            $query[] = ($field == 'ID' ? 'i.' : '') . '`' . $field . '` = \'' . $value . '\'';
        }

        if (count($query)) {
            $data = Database::value("SELECT * FROM `" . $table . "` i WHERE " . implode(' OR ', $query));
        }

        return (isset($data)) ? $data : false;
    }

    public static function find($value, $fields = false)
    {
        $model = static::class;
        $table = static::getTableName();

        $object = new $model();

        $data = self::findAsArray($table, $value, $fields);

        $object->setData($data);

        return $object;
    }
}
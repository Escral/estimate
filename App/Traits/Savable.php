<?php

namespace App\Traits;

use App\Database;

trait Savable
{
    public static $modelName;

    public static function getModelName()
    {
        if (static::$modelName !== null)
            return static::$modelName;

        $classname = static::class;

        if ($pos = strrpos($classname, '\\'))
            $classname = substr($classname, $pos + 1);

        static::$modelName = $classname;

        return static::$modelName;
    }

    public static function getTableName()
    {
        return strtolower(self::getModelName());
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function save($data)
    {
        $table = static::getTableName();
        $model = static::getModelName();

        $q = implode(', ', array_fill(0, count($data), '?'));

        $array = array_slice($data, 0, count($data));
        $titles = implode(', ', array_keys($array));
        $values = array_values($array);

        return Database::query("INSERT INTO {$table} ({$titles}) VALUES ($q)", $values);
    }
}
<?php

namespace App\Traits;

use App\App;
use App\Database;
use App\Migrations\CreateOperationMigration;

trait DatabaseMigrations
{
    public static function boot()
    {
        App::migrate();
    }
}
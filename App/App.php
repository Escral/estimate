<?php

namespace App;

class App
{
    protected static $router;

    public function __construct()
    {
        Settings::init();

        $this->boot();
    }

    public function init()
    {
        try {
            $this->configure();

            Session::init();
            Data::init();

            if (ENV !== ENV_TEST and ENV !== ENV_CONSOLE)
                $this->displayPage();
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function boot()
    {
        // Include env file
        include_once Settings::get('utils_dir') . '/env.php';

        // Include helper functions
        include_once Settings::get('utils_dir') . '/helpers.php';

        // Load translations
        Translation::loadTranslations();
    }

    public function configure()
    {
        $db = new Database();
    }

    /**
     * @param mixed $router
     */
    public function setRouter($router)
    {
        self::$router = $router;
    }

    /**
     * @return \Bramus\Router\Router
     */
    public static function getRouter()
    {
        return self::$router;
    }

    public function displayPage()
    {
        self::setRouter(new \Bramus\Router\Router());

        Router::defineRoutes();

        self::$router->run();

        Data::save();
        Session::save();
    }

    public static function isAjax()
    {
        return (! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

    public static function migrate($class = null)
    {
        $migrations = scandir(base_dir('/App/Migrations'));

        if ($class) {
            return (new $class)->migrate();
        }

        foreach ($migrations as $name) {
            if ($name == '.' or $name == '..')
                continue;

            $class = '\App\Migrations\\' . str_replace('.php', '', $name);

            (new $class)->migrate();
        }
    }

    public static function seed($class = null)
    {
        $seeds = scandir(base_dir('/App/Seeds'));

        if ($class) {
            return (new $class)->execute();
        }

        foreach ($seeds as $name) {
            if ($name == '.' or $name == '..')
                continue;

            $class = '\App\Seeds\\' . str_replace('.php', '', $name);

            (new $class)->execute();
        }
    }
}
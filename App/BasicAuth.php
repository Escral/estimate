<?php

namespace App;

class BasicAuth
{
	public static function check()
	{	
		if (! isset($_SERVER['PHP_AUTH_USER'])) {
			Header ("WWW-Authenticate: Basic realm=\"Site access\"");
			Header ("HTTP/1.0 401 Unauthorized");
			exit();
		} else {
			$login = $_SERVER['PHP_AUTH_USER'];
			$pass = $_SERVER['PHP_AUTH_PW'];
			
            $users = self::getUsers();
			
			if ($login == '' or $pass == '' or ! isset($users[$login]) or $pass !== $users[$login]) {
				Header ("WWW-Authenticate: Basic realm=\"Access Denied\"");
				Header ("HTTP/1.0 401 Unauthorized");
				exit();
			}
		}
	}

	public static function getUsers()
	{
		return array(
			'vkiose' => '123456'
		);
	}
}
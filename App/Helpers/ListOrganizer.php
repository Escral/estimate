<?php

namespace App\Helpers;

use App\Entity;

class ListOrganizer
{
    /**
     * @param array $items
     * @param string $field
     * @return array
     */
    public static function groupByField($items, $field)
    {
        $result = [];

        foreach ($items as $item) {
            $result[$item->$field][] = $item;
        }

        return $result;
    }

    public static function inlineGroups($items, $putToField = false)
    {
        $result = [];

        foreach ($items as $group => $list) {
            if (! $putToField)
                $result[] = $group;
            else
                $result[] = [$putToField => $group];

            foreach ($list as $item) {
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * @param Entity[] $items
     * @return mixed
     */
    public static function toArray($items, $full = false)
    {
        foreach ($items as &$item) {
            if (! is_array($item))
                $item = $item->toArray($full);
        }

        return $items;
    }
}
<?php

namespace App\Helpers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class Export
{
    public static $exportDir = 'files';

    /**
     * @param array[] $data
     * @param array $fields
     * @param string $fileName
     * @return bool|string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public static function xls($data, $fields, $comments, $fileName)
    {
        $file = self::$exportDir . '/' . $fileName . '.xls';

        if (file_exists($file))
            unlink($file);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $alphabet = array_slice(range('A', 'Z'), 0, count($fields));

        $headerStyle = [
            'font' => [
                'bold' => true
            ],
        ];

        $commentStyle = [
            'font' => [
                'bold' => false
            ],
        ];

        $sheet->getStyle('A1:Z1')->applyFromArray($headerStyle);

        $n = 0;
        foreach ($fields as $field => $title) {
            $sheet->setCellValue($alphabet[$n] . '1', $title);
            $sheet->getColumnDimension($alphabet[$n])->setAutoSize(true);
            $n++;
        }

        $n = 2;
        foreach ($data as $item) {
            $n2 = 0;

            foreach ($fields as $field => $title) {
                $value = $item[$field] ?? '';

                if ($value === 0)
                    $value = '';

                $sheet->setCellValue($alphabet[$n2] . $n, $value);

                if (isset($item['custom']) and $item['custom']) {
                    $sheet->getStyle($alphabet[$n2] . $n)->applyFromArray($headerStyle);

                    if (isset($item['separator']) and $item['separator']) {
                        $sheet->getRowDimension($n)->setRowHeight(36);
                        $sheet->mergeCells('A' . $n . ':' . 'E' . $n);
                        $sheet->getStyle($alphabet[$n2] . $n)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                        if (isset($item['big']) and $item['big']) {
                            $sheet->getStyle($alphabet[$n2] . $n)->getFont()->setSize(18);
                        }
                    } else {
                        $sheet->mergeCells('B' . $n . ':' . 'E' . $n);
                    }
                }

                $n2++;
            }
            $n++;
        }

        foreach ($comments as $offset => $comment) {
            $sheet->mergeCells('F' . $offset . ':F' . ($offset + $comment['size']));
            $sheet->setCellValue('F' . $offset, $comment['text']);
            $sheet->getStyle('F' . $offset)->applyFromArray($commentStyle)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        }

        $writer = new Xls($spreadsheet);

        $writer->save($file);

        if (file_exists($file))
            return '/' . $file;

        return false;
    }

    public static function separator(&$items, $size = 1)
    {
        for ($i = 0; $i < $size; $i++) {
            $items[] = [
                'custom' => true,
                'separator' => true
            ];
        }
    }

    public static function space(&$items, $size = 1)
    {
        for ($i = 0; $i < $size; $i++) {
            $items[] = [
                'custom' => true
            ];
        }
    }

    public static function title(&$items, $title, $big = false)
    {
        $items[] = [
            'custom' => true,
            'title'  => $title,
            'separator' => $big
        ];
    }

    public static function section(&$items, $title, $big = false)
    {
        $items[] = [
            'custom' => true,
            'code'  => $title,
            'separator' => true,
            'big' => $big
        ];
    }
}
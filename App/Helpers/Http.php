<?php

namespace App\Helpers;

class Http
{
    public static function redirect($url, $code = 302)
    {
        header("Location: " . $url, true, $code);
    }
}
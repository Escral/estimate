<?php

namespace App;

class Factory
{
    public static $faker;
    private static $factories = [];

    public static function setFaker()
    {
        self::$faker = \Faker\Factory::create();
    }

    public static function add($class, $callback)
    {
        self::$factories[$class] = $callback;
    }

    public function get($class)
    {
        return self::$factories[$class] ?? null;
    }

    /**
     * @param string $class
     * @param array $data
     * @param int $count
     * @return mixed
     * @throws \Exception
     */
    public static function create($class, $data = [], $count = 1)
    {
        if ($count == 1) {
            return self::make($class, $data)->create();
        } else {
            $result = [];

            while ($count--) {
                $result[] = self::make($class, $data)->create();
            }

            return $result;
        }
    }

    /**
     * @param string $class
     * @param array $data
     * @return Entity
     * @throws \Exception
     */
    public static function make($class, $data = [])
    {
        $factory = self::get($class)(self::$faker);

        if (! $factory)
            throw new \Exception("Factory for model '{$class}' not found");

        $data = array_merge($factory, $data);

        $object = (new $class)->setData($data);

        return $object;
    }
}
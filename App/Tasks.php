<?php

namespace App;

class Tasks
{
	protected $tasks;

	public function getTasks()
	{
		if (! $this->tasks)
			$this->tasks = [
			    //
            ];

		return $this->tasks;
	}

	public function find($key)
	{
		if (isset($this->getTasks()[$key]))
			return $this->getTasks()[$key];

        return false;
	}

	public function executeAll()
	{
		foreach ($this->getTasks() as $task)
			$task->execute();
	}
}
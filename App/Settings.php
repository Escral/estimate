<?php

namespace App;

final class Settings
{
    protected static $base_dir;
    protected static $params;

    public static function init()
    {
        self::$base_dir = __DIR__ . '/..';

        self::$params = [
            'base_dir'         => self::$base_dir,
            'utils_dir'        => self::$base_dir . '/utils',
            'resources_dir'    => self::$base_dir . '/resources',
            'files_dir'        => self::$base_dir . '/files',
            'views_dir'        => self::$base_dir . '/resources/views',
            'translations_dir' => self::$base_dir . '/translations',

            'DB_HOST' => '127.0.0.1',

            'DB_NAME' => 'test',
            'DB_USER' => 's31268_dbuser',
            'DB_PASS' => '8KGgS@zo~@TL',

//            'DB_NAME' => 'estimate',
//            'DB_USER' => 'root',
//            'DB_PASS' => 'lewxuplg',

            'DB_ENCODING'  => 'utf8',
            'DB_COLLATION' => 'utf8_general_ci',
            'DB_TIMEZONE'  => '+02:00',

            'lang' => 'ru'
        ];
    }

    public static function get($param)
    {
        if (defined($param))
            self::$params[$param] = constant($param);

        if (! isset(self::$params[$param])) {
            throw new \Exception('Param not found: ' . $param);
        }
        return self::$params[$param];
    }
}

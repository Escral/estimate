<?php

namespace App;

use App\Helpers\Http;
use App\Models\Estimate;
use App\Models\Page;


class Router
{
    public static function defineRoutes()
    {
        $router = App::getRouter();

        $router->before('GET|POST', '/(?!register|login).*', function() {
            Auth::check();
        });

        $router->before('GET|POST', '/(?!floors|register|login).*', function() {
            if (! Estimate::getActive()) {
                Http::redirect('/floors');
            }
        });

        $router->setNamespace('\App\Controllers');

        // Tasks

        // Routes
        $router->get('/login', 'UserController@loginForm');
        $router->post('/login', 'UserController@login');
        $router->get('/logout', 'UserController@logout');
        $router->get('/register', 'UserController@registerForm');
        $router->post('/register', 'UserController@register');

        $router->post('/estimate/add', 'EstimateController@add');
        $router->post('/estimate/delete', 'EstimateController@delete');
        $router->post('/estimate/change', 'EstimateController@change');


        $router->all('/', 'FloorsController@floors');

        $router->all('/floors', 'FloorsController@floors');
        $router->all('/floors/(\w*?)', 'FloorsController@floors');
        $router->all('/export', 'PageController@export');
        $router->all('/export-file', 'PageController@exportFile');

        $router->post('/add-floor/(\d+)', 'FloorsController@add');
        $router->post('/edit-floor/(\d+)', 'FloorsController@edit');
        $router->post('/change-floor/(\d+)', 'FloorsController@change');
        $router->post('/remove-floor/(\d+)', 'FloorsController@remove');
//        $router->post('/export', 'PageController@export');
        $router->post('/clear-all', 'PageController@clear');

        $router->all('/radiators', 'RadiatorsController@items');
        $router->all('/radiators/add', 'RadiatorsController@add_items');
        $router->all('/radiators/add_radiators', 'RadiatorsController@add_radiators');
        $router->all('/radiators/remove_radiators', 'RadiatorsController@remove_radiators');

        $router->all('/radiators/futorki', 'RadiatorsController@futorki');
        $router->all('/radiators/legs', 'RadiatorsController@legs');
        $router->all('/radiators/scheme', 'RadiatorsController@scheme');
        $router->all('/radiators/duopipe', 'RadiatorsController@duopipe');
        $router->all('/radiators/insulation', 'RadiatorsController@insulation');
        $router->all('/radiators/corners', 'RadiatorsController@corners');
        $router->all('/radiators/couplings', 'RadiatorsController@couplings');
        $router->all('/radiators/transitions', 'RadiatorsController@transitions');
        $router->all('/radiators/tees', 'RadiatorsController@tees');
        $router->all('/radiators/valves', 'RadiatorsController@valves');
        $router->all('/radiators/MP', 'RadiatorsController@MP');
        $router->all('/radiators/additional', 'RadiatorsController@additional');
        $router->all('/radiators/work', 'RadiatorsController@work');

        $router->all('/radiators/beam', 'RadiatorsController@beam');
        $router->all('/radiators/insulation_riser', 'RadiatorsController@insulation_riser');
        $router->all('/radiators/corners_riser', 'RadiatorsController@corners_riser');
        $router->all('/radiators/couplings_riser', 'RadiatorsController@couplings_riser');
        $router->all('/radiators/collector', 'RadiatorsController@collector');
        $router->all('/radiators/gerpex', 'RadiatorsController@gerpex');
        $router->all('/radiators/locker', 'RadiatorsController@locker');
        $router->all('/radiators/valves_metal', 'RadiatorsController@valves_metal');
        $router->all('/radiators/gerpex_valves', 'RadiatorsController@gerpex_valves');
        $router->all('/radiators/additional_beam', 'RadiatorsController@additional_beam');
        $router->all('/radiators/work_beam', 'RadiatorsController@work_beam');




        $router->all('/water', 'WaterController@scheme');
        $router->all('/water/items', 'WaterController@items');
        $router->all('/water/pipe', 'WaterController@pipe');
        $router->all('/water/insulation', 'WaterController@insulation');
        $router->all('/water/corners', 'WaterController@corners');
        $router->all('/water/couplings', 'WaterController@couplings');
        $router->all('/water/transitions', 'WaterController@transitions');
        $router->all('/water/tees', 'WaterController@tees');
        $router->all('/water/MP', 'WaterController@MP');
        $router->all('/water/stub', 'WaterController@stub');
        $router->all('/water/additional', 'WaterController@additional');
        $router->all('/water/work', 'WaterController@work');



        $router->all('/hfloors', 'HFloorsController@scheme');
        $router->all('/hfloors/riser', 'HFloorsController@riser');
        $router->all('/hfloors/insulation_riser', 'HFloorsController@insulation_riser');
        $router->all('/hfloors/corners_riser', 'HFloorsController@corners_riser');
        $router->all('/hfloors/couplings_riser', 'HFloorsController@couplings_riser');
        $router->all('/hfloors/area', 'HFloorsController@area');
        $router->all('/hfloors/pipe', 'HFloorsController@pipe');
        $router->all('/hfloors/insulation', 'HFloorsController@insulation');
        $router->all('/hfloors/skin', 'HFloorsController@skin');
        $router->all('/hfloors/insulation_join', 'HFloorsController@insulation_join');
        $router->all('/hfloors/clips', 'HFloorsController@clips');
        $router->all('/hfloors/collector', 'HFloorsController@collector');
        $router->all('/hfloors/gerpex', 'HFloorsController@gerpex');
        $router->all('/hfloors/corner_fixes', 'HFloorsController@corner_fixes');
        $router->all('/hfloors/locker', 'HFloorsController@locker');
        $router->all('/hfloors/additional', 'HFloorsController@additional');
        $router->all('/hfloors/work', 'HFloorsController@work');


        $router->all('/hfloors/area_2', 'HFloorsController@area_2');
        $router->all('/hfloors/pipe_2', 'HFloorsController@pipe_2');
        $router->all('/hfloors/insulation_2', 'HFloorsController@insulation_2');
        $router->all('/hfloors/skin_2', 'HFloorsController@skin_2');
        $router->all('/hfloors/insulation_join_2', 'HFloorsController@insulation_join_2');
        $router->all('/hfloors/clips_2', 'HFloorsController@clips_2');
        $router->all('/hfloors/fjvr', 'HFloorsController@fjvr');
        $router->all('/hfloors/gerpex_2', 'HFloorsController@gerpex_2');
        $router->all('/hfloors/corner_fixes_2', 'HFloorsController@corner_fixes_2');
        $router->all('/hfloors/work_2', 'HFloorsController@work_2');



        $router->all('/sewage', 'SewageController@items');
        $router->all('/tools', 'ToolsController@items');




        $router->all('/boiler', 'BoilerController@kettles');
        $router->all('/boiler/add_kettles', 'BoilerController@add_kettles');
        $router->all('/boiler/remove_kettles', 'BoilerController@remove_kettles');

        $router->all('/boiler/boiler', 'BoilerController@boiler');
        $router->all('/boiler/add_boiler', 'BoilerController@add_boiler');
        $router->all('/boiler/remove_kettles', 'BoilerController@remove_kettles');
        $router->all('/boiler/add_boiler', 'BoilerController@add_boiler');
        $router->all('/boiler/add_pump', 'BoilerController@add_pump');

        $router->all('/boiler/kettle_piping', 'BoilerController@kettle_piping');
        $router->all('/boiler/set_kettle_piping_type', 'BoilerController@set_kettle_piping_type');
        $router->all('/boiler/boiler_piping', 'BoilerController@boiler_piping');
        $router->all('/boiler/set_boiler_piping_type', 'BoilerController@set_boiler_piping_type');
        $router->all('/boiler/boiler_collector', 'BoilerController@boiler_collector');
        $router->all('/boiler/collector_piping', 'BoilerController@collector_piping');
        $router->all('/boiler/set_collector_piping_type', 'BoilerController@set_collector_piping_type');
        $router->all('/boiler/pump', 'BoilerController@pump');
        $router->all('/boiler/add_boiler', 'BoilerController@add_boiler');
        $router->all('/boiler/remove_boiler', 'BoilerController@remove_boiler');
        $router->all('/boiler/add_pump', 'BoilerController@add_pump');
        $router->all('/boiler/remove_pump', 'BoilerController@remove_pump');





        // 404
        $router->set404(function() {
            header('HTTP/1.1 404 Not Found');
            print "Page not found";
        });
    }
    /**
     * Get page from URI
     *
     * @return Page
     */
    public static function getPage()
    {
        $page = new Page('index');
        
        return $page;
    }
}
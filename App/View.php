<?php

namespace App;

use App\Models\Page;

class View
{
    protected static $ext = 'php';

    protected static $hooks = [];

    public static function getExtension()
    {
        return self::$ext;
    }

    public static function render($view, $data = array())
    {
        $page = new Page('index');

        $layoutPath = views_dir('layouts/' . $page->getLayout() . '.' . self::getExtension());

        if (! file_exists($layoutPath))
            throw new \Exception("Layout file not found: " . $layoutPath);
        
        $filePath = views_dir($view . '.' . self::getExtension());

        if (! file_exists($filePath))
            throw new \Exception("View file not found: " . $filePath);

        $funct = function ($data) use ($filePath) {
            extract($data);
            include $filePath;
        };

        $funct($data);

        if (App::isAjax()) {
            return self::hook('content');
        } else {
            ob_start();

            include $layoutPath;

            $content = ob_get_contents();

            ob_end_clean();

            return $content;
        }
    }

    public static function getPageViewPath(Page $page)
    {
        return views_dir('pages/' . $page->getName() . '.' . self::getExtension());
    }

    public static function hook($name)
    {
        $hook = self::getHook($name);

        if ($hook)
            return $hook->render();
    }

    public static function getHook($name)
    {
        if (self::hookExists($name))
            return self::$hooks[$name];

        return false;
    }

    public static function hookExists($name)
    {
        return isset(self::$hooks[$name]);
    }

    public static function registerHook($name, $content)
    {
        if (! self::hookExists($name)) {
            $hook = new Hook($name, $content);
            self::$hooks[$name] = $hook;
        } else {
            throw new \Exception('Hook already registered: ' . $name);
        }
    }

    public static function include($file, $data = [])
    {
        extract($data);
        include views_dir($file);
    }
}
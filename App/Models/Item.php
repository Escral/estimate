<?php

namespace App\Models;

class Item
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $title;
    /**
     * @var int
     */
    public $count;
    /**
     * @var int
     */
    public $price;
    /**
     * @var int
     */
    public $sections;
    /**
     * @var string
     */
    public $categoryName;

    public function __construct($id, $title, $count = 0, $price = 0)
    {
        $this->id = $id;
        $this->title = $title;
        $this->count = $count;
        $this->price = $price;
    }

    public function __get($name)
    {
        if ($name == 'count') {
            if (is_array($this->$name))
                return $this->$name->$name;
        }
    }

    /**
     * @return int
     */
    public function getCount() : int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return Item
     */
    public function addCount($count)
    {
        $this->count += $count;

        return $this;
    }

    /**
     * @param int $count
     * @return Item
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @param int $count
     * @return Item
     */
    public function removeCount($count)
    {
        $this->count -= $count;

        return $this;
    }

    /**
     * @param int $price
     * @return Item
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @param int $sections
     * @return Item
     */
    public function setSections($sections)
    {
        $this->sections = $sections;

        return $this;
    }

    /**
     * @return int
     */
    public function getSections()
    {
        return $this->sections;
    }

    public function afterAdd()
    {

    }

    /**
     * @param string $title
     * @return Item
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param string $categoryName
     * @return Item
     */
    public function setCategoryName($categoryName)
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    /**
     * @param string $title
     * @return Item
     */
    public function appendTitle($title)
    {
        $this->title .= ' ' . $title;

        return $this;
    }

    public function toArray($full = false)
    {
        return [
            'id' => $this->id,
            'title' => $full ? $this->getFullTitle() : $this->title,
            'count' => $this->count,
            'price' => $this->price
        ];
    }

    public function withSections()
    {
        if (strpos($this->title, 'Алюминиевый') !== false or strpos($this->title, 'Биметаллический') !== false)
            return true;

        return false;
    }

    private function getFullTitle()
    {
        return ($this->categoryName ? $this->categoryName . ' ' : '') . $this->title;
    }
}
<?php

namespace App\Models;

use App\Auth;
use App\Entity;

class Estimate extends Entity
{
    public static function getActiveId()
    {
        $active = self::getActive();

        if ($active)
            return $active->ID;
        else
            return 0;
    }

    public static function getActive()
    {
        return Auth::user() ? self::find(Auth::user()->active_estimate) : Auth::user();
    }

    public function isActive()
    {
        return Estimate::getActive() && $this->ID == Estimate::getActive()->ID;
    }

    /**
     * If estimate is belongs to user
     * @param User|int $user
     * @return bool
     */
    public function belongsTo($user)
    {
        if ($user instanceof User)
            return $this->user_ID == $user->ID;
        else
            return $this->user_ID == $user;
    }
}
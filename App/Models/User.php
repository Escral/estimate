<?php

namespace App\Models;

use App\Auth;
use App\Entity;

class User extends Entity
{
    public static function register($login, $password, &$errors)
    {
        $user = new User();

        $user->login = $login;
        $user->password = md5($password);

        if (User::find($login, ['login'])) {
            $errors['login'] = "Пользователь с таким логином уже существует";
            return false;
        }

        return $user->create();
    }

    public function login()
    {
        Auth::login($this);
    }

    public function setActiveEstimate(Estimate $estimate)
    {
        return $this->update([
            'active_estimate' => $estimate->ID
        ]);
    }
}
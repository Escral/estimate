<?php

namespace App\Models;

use App\Data;
use App\Path;

class Floor
{
    public static function getFloorName($floor)
    {
        return Data::get('floors')[$floor];
    }

    public static function getCurrentFloorName()
    {
        return self::getFloorName(self::getFloor());
    }

    public static function getFloor()
    {
        return Data::get('floor');
    }

    public static function getFloorValue()
    {
        $floors = Data::get('floors');
        return $floors[self::getFloor()] ?? false;
    }

    public static function withRadiators()
    {
        $value = self::getFloorValue();

        return ($value == 1 || $value == 3);
    }

    public static function getFloors()
    {
        $floors = [];

        $data = Data::get('floors');

        foreach ($data as $ID => $value) {
            $floors[] = [
                'ID' => $ID,
                'title' => self::getFloorName($ID),
                'value' => $value
            ];
        }
        return $floors;
    }

    public static function setNext()
    {
        $floors = Data::get('floors');

        if (! $floors)
            return false;

        $f = $next = false;
        foreach ($floors as $floor => $value) {
            if ($f) {
                $next = $floor;
                break;
            }
            if ($floor === Data::get('floor'))
                $f = true;
        }

        if ($next !== false)
            Data::set('floor', $next);

        return $next;
    }

    public static function change($floor)
    {
        if (Floor::exists($floor)) {
            return Data::set('floor', $floor);
        }

        return false;
    }

    public static function exists($floor)
    {
        $floors = Data::get('floors');

        return isset($floors[$floor]);
    }

    public static function getStatuses()
    {
        return [
            'heating' => self::getStatus('heating'),
            'water' => self::getStatus('water'),
            'hfloors' => self::getStatus('hfloors'),
            'sewage' => self::getStatus('sewage'),
            'boiler' => self::getStatus('boiler'),
            'tools' => self::getStatus('tools'),
        ];
    }

    public static function getStatus($section)
    {
        $back = [];
        foreach (Path::$PATH as $n => $i) {
            $a = $i['next'];
            if (is_array($a)) {
                foreach ($a as $v)
                    $back[$v] = $n;
            } else {
                $back[$a] = $n;
            }
        }

        $n = 0;
        foreach ($back as $item => &$prev) {
            $prev = [
                'prev'  => $prev,
                'index' => $n++
            ];
        }

        $result = [];

        foreach (self::getFloors() as $floor) {
            $floor = $floor['ID'];

            $visited = Data::floor($floor)::get('path');
            $visited = $visited[$section] ?? null;

            $index = 0;
            $done = 0;
            $total = 0;
            $key = '';

            if (is_array($visited)) {
                foreach ($visited as $item => $is) {
                    $i = $back[$item]['index'] ?? 0;
                    if ($i > $index) {
                        $index = $i;
                        $key = $item;
                    }
                }
            }

            if ($key != '') {
                $page = $key;

                $path = [];

                $item = Path::$PATH[$name = $page];

                do {
                    $item['name'] = $name;
                    $path[$name] = $item;
                    $done++;

                    if (! isset($back[$name]) or $back[$name]['prev'] == '')
                        break;

                    $name = $back[$name]['prev'];
                } while ($item = Path::$PATH[$name]);




                $item = Path::$PATH[$name = Path::getFirstRoute($section)] ?? [];

                do {
                    $total++;

                    if (! isset(Path::$PATH[$name]) or (isset($item['next']) and $item['next'] == '') or is_array($item['next']))
                        break;

                    $name = $item['next'];
                } while ($item = Path::$PATH[$name]);
            }

            $result[$floor] = [
                'done' => $done,
                'total' => $total
            ];
        }

        return $result;
    }

    public static function getVisitedFloorsAll()
    {
        return [
            'heating' => self::getVisitedFloors('heating'),
            'water' => self::getVisitedFloors('water'),
            'hfloors' => self::getVisitedFloors('hfloors'),
            'sewage' => self::getVisitedFloors('sewage'),
            'boiler' => self::getVisitedFloors('boiler'),
            'tools' => self::getVisitedFloors('tools'),
        ];
    }

    public static function getSections()
    {
        return [
            'heating' => 'Отопление',
            'water' => 'Водоснабжение',
            'hfloors' => 'Тёплые полы',
            'sewage' => 'Канализация',
            'boiler' => 'Котельная',
            'tools' => 'Инструмент'
        ];
    }
    
    public static function getSectionTitleByName($section)
    {
        return self::getSections()[$section];
    }

    public static function getVisitedFloors($section)
    {
        $result = [];

        foreach (self::getFloors() as $floor) {
            $floor = $floor['ID'];

            if ($section == 'tools') {
                $visited = Data::get('path');
            } else {
                $visited = Data::floor($floor)::get('path');
            }

            $visited = $visited[$section] ?? [];

            $result[$floor] = count($visited);
        }

        return $result;
    }
}
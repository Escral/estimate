<?php

namespace App\Models;

class Page 
{
    protected $name;

    protected $layout = 'main';

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLayout()
    {
        return $this->layout;
    }

    public function setLayout($value)
    {
        $this->layout = $value;
    }
}
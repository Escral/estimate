<?php

namespace App\Models;

class EstimateGroup
{
    private $items;

    public function __construct($items = [])
    {
        $this->items = $items;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    public function add($item, $count = 1)
    {
        if (! isset($this->items[$item->id]))
            $this->items[$item->id] = $item;

        $this->items[$item->id]->addCount($count);

        $this->items[$item->id]->afterAdd($this->getItems());
    }

    public function remove($item, $count = 0)
    {
        if ($item instanceof Item)
            $id = $item->id;

        if (isset($this->items[$id])) {
            if ($count == 0)
                unset($this->items[$id]);
            else
                $this->items[$id]->removeCount($count);
        }
    }
}
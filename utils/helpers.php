<?php

use App\Settings;
use App\Translation;

function config($var)
{
    return Settings::get($var);
}

function base_dir($path = '')
{
    return Settings::get('base_dir') . ($path != '' ? '/' : '') . $path;
}

function views_dir($path = '')
{
    return Settings::get('views_dir') . ($path != '' ? '/' : '') . $path;
}

function files_dir($path = '')
{
    return Settings::get('files_dir') . ($path != '' ? '/' : '') . $path;
}

function lang($text)
{
    return Translation::get($text) ?: $text;
}
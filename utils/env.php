<?php

define('ENV_DEV', 'dev');
define('ENV_TEST', 'test');
define('ENV_PROD', 'prod');
define('ENV_CONSOLE', 'console');

// Current enviroment
if (defined('PHPUNIT_RUNNIGN'))
    define('ENV', ENV_TEST);
else if (! defined('ENV'))
    define('ENV', ENV_DEV);

if (ENV == ENV_DEV) {
    error_reporting(E_ALL);
    ini_set('display_errors', true);
} else if (ENV == ENV_PROD) {
    ini_set('display_errors', false);
}

ini_set('log_errors', true);
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(3);


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__page_js__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__page_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__page_js__);


/***/ }),
/* 2 */
/***/ (function(module, exports) {

$(function () {
    window.setControls();
});

window.setControls = function () {
    $(".ajax-form").bind("submit", function (e) {
        e.preventDefault();

        var action = $(this).attr("action");
        var next = $(this).data("next");

        $.ajax({
            method: "POST",
            url: action,
            data: $(this).serialize()
        }).done(function (data) {
            $("#app").replaceWith(data);

            setControls();
        });

        history.pushState(null, next, next);

        return false;
    });

    $(".path a").bind("click", function (e) {
        e.preventDefault();

        var action = $(this).attr("href");

        $.ajax({
            method: "POST",
            url: action
        }).done(function (data) {
            $("#app").replaceWith(data);

            setControls();
        });

        history.pushState(null, action, action);

        return false;
    });

    $(".ajax").bind("click", function (e) {
        e.preventDefault();

        var action = $(this).attr("href");

        $.ajax({
            method: "POST",
            url: action
        }).done(function (data) {
            $("#app").replaceWith(data);

            setControls();
        });

        return false;
    });

    $(".json").bind("click", function (e) {
        e.preventDefault();

        var action = $(this).attr("href");

        $.ajax({
            method: "POST",
            url: action
        }).done(function (data) {
            try {
                data = JSON.parse(data);
            } catch (e) {
                console.log(e);
                $("#app").replaceWith("<pre>" + data + "</pre>");
            }

            if (data.href) {
                window.location.href = data.href;
            }
        });

        return false;
    });

    $(".json-form").bind("submit", function (e) {
        e.preventDefault();

        var action = $(this).attr("action");

        $.ajax({
            method: "POST",
            url: action,
            data: $(this).serialize()
        }).done(function (data) {
            try {
                data = JSON.parse(data);
            } catch (e) {
                console.log(e);
                $("#app").replaceWith("<pre>" + data + "</pre>");
            }

            if (data.href) {
                window.location.href = data.href;
            }
        });

        return false;
    });
};

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
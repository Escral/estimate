#!/usr/bin/env php
<?php

use App\Console\ApplicationExtra;

umask(0000);

set_time_limit(0);

define('ENV', 'console');

require __DIR__.'/vendor/autoload.php';

$application = new ApplicationExtra(__DIR__.'/App/Console/Command/');
$application->run();

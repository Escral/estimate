$(function() {
    window.setControls();
});



window.setControls = function() {
    $(".ajax-form").bind("submit", function(e) {
        e.preventDefault();

        let action = $(this).attr("action");
        let next = $(this).data("next");

        $.ajax({
            method: "POST",
            url: action,
            data: $(this).serialize()
        })
            .done(function (data) {
                $("#app").replaceWith(data);

                setControls();
            });

        history.pushState(null, next, next);

        return false;
    });

    $(".path a").bind("click", function(e) {
        e.preventDefault();

        let action = $(this).attr("href");

        $.ajax({
            method: "POST",
            url: action
        })
            .done(function (data) {
                $("#app").replaceWith(data);

                setControls();
            });

        history.pushState(null, action, action);

        return false;
    });

    $(".ajax").bind("click", function(e) {
        e.preventDefault();

        let action = $(this).attr("href");

        $.ajax({
            method: "POST",
            url: action
        })
            .done(function (data) {
                $("#app").replaceWith(data);

                setControls();
            });

        return false;
    });

    $(".json").bind("click", function(e) {
        e.preventDefault();

        let action = $(this).attr("href");

        $.ajax({
            method: "POST",
            url: action
        })
            .done(function (data) {
                try {
                    data = JSON.parse(data);
                } catch (e) {
                    console.log(e);
                    $("#app").replaceWith("<pre>" + data + "</pre>");
                }

                if (data.href) {
                    window.location.href = data.href;
                }
            });

        return false;
    });


    $(".json-form").bind("submit", function(e) {
        e.preventDefault();

        let action = $(this).attr("action");

        $.ajax({
            method: "POST",
            url: action,
            data: $(this).serialize()
        })
            .done(function (data) {
                try {
                    data = JSON.parse(data);
                } catch (e) {
                    console.log(e);
                    $("#app").replaceWith("<pre>" + data + "</pre>");
                }

                if (data.href) {
                    window.location.href = data.href;
                }
            });

        return false;
    });
}
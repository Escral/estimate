<?php use App\Items;
use App\Models\Floor;
use App\View;
use App\Path;
$page = 'boiler_collector';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'boiler']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/boiler/boiler_collector" data-next="/boiler/collector_piping" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Выберите компланарный коллетор</h5>
                    <div class="card-text">
                        <div class="row">
                            <div class="col-sm">
                                <label for="type1" class="card">
                                    <img class="card-img-top" src="/public/img/boiler_piping/1.png">
                                    <div class="card-body">
                                        <h5 class="card-title">1-1-1</h5>

                                        <div class="form-group">
                                            <input id="type1" <?php if ($boiler_collector_value == 1) { ?> checked<?php } ?> name="boiler_collector_value" value="1" type="radio" class="form-control">
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-sm">
                                <label for="type2" class="card">
                                    <img class="card-img-top" src="/public/img/boiler_piping/1.png">
                                    <div class="card-body">
                                        <h5 class="card-title">2-1-0</h5>

                                        <div class="form-group">
                                            <input id="type2" <?php if ($boiler_collector_value == 2) { ?> checked<?php } ?> name="boiler_collector_value" value="2" type="radio" class="form-control">
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-sm">
                                <label for="type3" class="card">
                                    <img class="card-img-top" src="/public/img/boiler_piping/1.png">
                                    <div class="card-body">
                                        <h5 class="card-title">2-0-2</h5>

                                        <div class="form-group">
                                            <input id="type3" <?php if ($boiler_collector_value == 3) { ?> checked<?php } ?> name="boiler_collector_value" value="2" type="radio" class="form-control">
                                        </div>
                                    </div>
                                </label>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm">
                                <label for="type4" class="card">
                                    <img class="card-img-top" src="/public/img/boiler_piping/1.png">
                                    <div class="card-body">
                                        <h5 class="card-title">0-2-0</h5>

                                        <div class="form-group">
                                            <input id="type4" <?php if ($boiler_collector_value == 4) { ?> checked<?php } ?> name="boiler_collector_value" value="1" type="radio" class="form-control">
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-sm">
                                <label for="type5" class="card">
                                    <img class="card-img-top" src="/public/img/boiler_piping/1.png">
                                    <div class="card-body">
                                        <h5 class="card-title">3-0-1</h5>

                                        <div class="form-group">
                                            <input id="type5" <?php if ($boiler_collector_value == 5) { ?> checked<?php } ?> name="boiler_collector_value" value="1" type="radio" class="form-control">
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-sm">
                                <label for="type6" class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Заказной</h5>

                                        <div class="form-group">
                                            <input id="type6" <?php if ($boiler_collector_value == 6) { ?> checked<?php } ?> name="boiler_collector_value" value="2" type="radio" class="form-control">
                                            <br>

                                            <label>Выходы вверх</label>
                                            <input type="number" class="form-control" value="<?=$boiler_collector['top'] ?? ''?>" name="boiler_collector[top]">
                                            <br>
                                            <label>Выходы вправо</label>
                                            <input type="number" class="form-control" value="<?=$boiler_collector['right'] ?? ''?>" name="boiler_collector[right]">
                                            <br>
                                            <label>Выходы вниз</label>
                                            <input type="number" class="form-control" value="<?=$boiler_collector['bottom'] ?? ''?>" name="boiler_collector[bottom]">
                                            <br>
                                            <label>Диаметр подводящих чего-то там</label>
                                            <input type="number" class="form-control" value="<?=$boiler_collector['diameter'] ?? ''?>" name="boiler_collector[diameter]">
                                        </div>
                                    </div>
                                </label>
                            </div>
                        </div>

                        <br>

                        <select name="gunner" class="form-control" style="width: 300px;">
                            <option value="1"<?php if ($gunner == 1) { ?> selected<?php } ?>>С гидрострелкой</option>
                            <option value="0"<?php if ($gunner == 0) { ?> selected<?php } ?>>Без гидрострелки</option>
                        </select>

                        <br>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
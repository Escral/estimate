<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'boiler';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'boiler']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>


        <style>
            #brand {
                display: none;
            }
        </style>

        
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/boiler/boiler" data-next="/boiler/kettle_piping" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Добавить бойлер</h5>
                    <div class="card-text select-item">
                        <div class="form-group kettle-type">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input toggleable-src" data-target="select1" name="type1" type="radio" id="inp1" value="С 1 змеевиком (1S)">
                                <label class="form-check-label" for="inp1">С 1 змеевиком (1S)</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input toggleable-src" data-target="select2" name="type1" type="radio" id="inp2" value="C 2 змеевиками (2S)">
                                <label class="form-check-label" for="inp2">C 2 змеевиками (2S)</label>
                            </div>
                        </div>

                        <div class="form-group" style="width: 300px; display: none;" id="brand">
                            <label>Выберите бренд</label>

                            <select name="brand" class="form-control toggleable select1" disabled>
                                <optgroup label="Tesy">
                                    <option value="Tesy">Tesy</option>
                                    <option value="Tesy 150л">Tesy 150л</option>
                                    <option value="Tesy 200л">Tesy 200л</option>
                                    <option value="Tesy 300л">Tesy 300л</option>
                                    <option value="Tesy 500л">Tesy 500л</option>
                                    <option value="Tesy 800л">Tesy 800л</option>
                                </optgroup>
                                <optgroup label="Defro">
                                    <option value="Defro">Defro</option>
                                    <option value="Defro 120л">Defro 120л</option>
                                    <option value="Defro 150л">Defro 150л</option>
                                </optgroup>
                                <optgroup label="Drazice">
                                    <option value="Drazice">Drazice</option>
                                    <option value="Drazice 200л">Drazice 200л</option>
                                    <option value="Drazice 300л">Drazice 300л</option>
                                    <option value="Drazice 400л">Drazice 400л</option>
                                    <option value="Drazice 500л">Drazice 500л</option>
                                </optgroup>
                                <optgroup label="ACV">
                                    <option value="ACV">ACV</option>
                                    <option value="ACV 130л">ACV 130л</option>
                                    <option value="ACV 160л">ACV 160л</option>
                                    <option value="ACV 210л">ACV 210л</option>
                                    <option value="ACV 240л">ACV 240л</option>
                                    <option value="ACV 300л">ACV 300л</option>
                                </optgroup>
                                <option value="">Похуй</option>
                            </select>

                            <select name="brand" class="form-control toggleable select2" disabled>
                                <optgroup label="Tesy">
                                    <option value="Tesy">Tesy</option>
                                    <option value="Tesy 150л">Tesy 150л</option>
                                    <option value="Tesy 200л">Tesy 200л</option>
                                    <option value="Tesy 300л">Tesy 300л</option>
                                    <option value="Tesy 500л">Tesy 500л</option>
                                    <option value="Tesy 800л">Tesy 800л</option>
                                </optgroup>
                                <optgroup label="Drazice">
                                    <option value="Drazice">Drazice</option>
                                    <option value="Drazice 200л">Drazice 200л</option>
                                    <option value="Drazice 300л">Drazice 300л</option>
                                    <option value="Drazice 400л">Drazice 400л</option>
                                    <option value="Drazice 500л">Drazice 500л</option>
                                </optgroup>
                                <option value="">Похуй</option>
                            </select>
                        </div>

                        <input type="text" id="name" class="form-control mb-3" placeholder="Название бойлера ...">


                        <button type="button" class="btn btn-primary add-item">Добавить</button>
                    </div>

                    <br>

                    <h5 class="card-title">Выбранные бойлеры</h5>
                    <p class="card-text">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Бойлер</th>
                                    <th scope="col">Кол-во</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="items">
                                <?php foreach ($selected as $key => $item) { ?>
                                    <tr rel="<?=$item->id?>">
                                        <td><?=$item->title?></td>
                                        <td>
                                            <input type="number" step="1" name="boiler[<?=$key?>][count]" class="small-input" value="<?=($item->count ? $item->count : '')?>" min="0">
                                        </td>
                                        <td class="remove" data-key="<?=$key?>">
                                            <div class="fa fa-times"></div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </p>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>

        <script>
            $(".toggleable-src").bind("change", function() {
                $(".toggleable").hide().attr('disabled', true);
                $(".toggleable:not(." + $(this).data("target") + ") input").val('');
                $("." + $(this).data("target")).show().removeAttr('disabled');
                $("#brand").show();
            });


            $(".select-item select, .select-item input[type=radio], .select-item input[type=number]").bind("change", function() {
                var paramObj = {};
                var str = '';

                $.each($('form').serializeArray(), function(_, kv) {
                    if (kv.name != 'name' && kv.name != 'items' && kv.name != 'submit' && ! kv.name.match(/boiler/)) {
                        paramObj[kv.name] = kv.value;
                        if (kv.value != '')
                            str += kv.value + ' ';

                        if (kv.name == 'power' && kv.value > 0)
                            str += 'кВт ';
                    }
                });

                str = str.trim();

                $("#name").val(str);
            });

            $(".add-item").bind("click", function() {
                var item = $("#name").val();

                $.ajax({
                    method: "POST",
                    url: "/boiler/add_boiler",
                    data: {
                        item: item
                    }
                })
                    .done(function (data) {
                        try {
                            data = JSON.parse(data);
                        } catch (e) {
                            console.log(e);
                            $("#app").replaceWith(data);
                        }
                    });

                $("#name").val("");
            });


            $(".items input[type=number]").bind("change", function() {
                $.ajax({
                    method: "POST",
                    url: $("form").attr("action"),
                    data: $("form").serialize()
                })
            });


            $(".remove").bind("click", function() {
                var key = $(this).data("key");

                $.ajax({
                    method: "POST",
                    url: "/boiler/remove_boiler",
                    data: {
                        key: key
                    }
                })
                    .done(function (data) {
                        try {
                            data = JSON.parse(data);
                        } catch (e) {
                            console.log(e);
                            $("#app").replaceWith(data);
                        }
                    });
            });
        </script>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
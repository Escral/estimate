<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'pump';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'boiler']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>


        <style>
            #brand {
                display: none;
            }
        </style>

        
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/boiler/pump" data-next="/boiler/pump_piping" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Добавить насос</h5>
                    <div class="card-text select-item">
                        <div class="form-group pump-type">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input toggleable-src" data-target="select1" name="type1" type="radio" id="inp1" value="">
                                <label class="form-check-label" for="inp1">Обычный</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input toggleable-src" data-target="select2" name="type1" type="radio" id="inp2" value="Частотник">
                                <label class="form-check-label" for="inp2">Частотник</label>
                            </div>
                        </div>

                        <div class="form-group" style="width: 300px; display: none;" id="brand">
                            <label>Выберите бренд</label>

                            <select name="brand" class="form-control toggleable select1" disabled>
                                <optgroup label="Willo RS25">
                                    <option value="Willo RS25/4">Willo RS25/4</option>
                                    <option value="Willo RS25/6">Willo RS25/6</option>
                                    <option value="Willo RS25/7">Willo RS25/7</option>
                                </optgroup>
                                <optgroup label="Willo RS32">
                                    <option value="Willo RS32/4">Willo RS32/4</option>
                                    <option value="Willo RS32/6">Willo RS32/6</option>
                                    <option value="Willo RS32/7">Willo RS32/7</option>
                                </optgroup>
                                <option value="">Похуй</option>
                            </select>

                            <select name="brand" class="form-control toggleable select2" disabled>
                                <optgroup label="Willo 25">
                                    <option value="Willo Yonos Para 25/7.5">Willo Yonos Para 25/7.5</option>
                                    <option value="Willo Atmos Pico 25/4">Willo Atmos Pico 25/4</option>
                                    <option value="Willo Atmos Pico 25/6">Willo Atmos Pico 25/6</option>
                                </optgroup>
                                <option value="">Похуй</option>
                            </select>
                        </div>

                        <div class="toggleable select1 select2" style="display: none;">
                            <label>Размер улитки</label>
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="type2" type="radio" checked id="inp3" value="130мм">
                                    <label class="form-check-label" for="inp3">130мм</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" data-target="select2" name="type2" type="radio" id="inp4" value="160мм">
                                    <label class="form-check-label" for="inp4">160мм</label>
                                </div>
                            </div>
                        </div>

                        <input type="text" id="name" class="form-control mb-3" placeholder="Название насоса ...">


                        <button type="button" class="btn btn-primary add-item">Добавить</button>
                    </div>

                    <br>

                    <h5 class="card-title">Выбранные насосы</h5>
                    <p class="card-text">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Насос</th>
                                    <th scope="col">Кол-во</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="items">
                                <?php foreach ($selected as $key => $item) { ?>
                                    <tr rel="<?=$item->id?>">
                                        <td><?=$item->title?></td>
                                        <td>
                                            <input type="number" step="1" name="boiler[<?=$key?>][count]" class="small-input" value="<?=($item->count ? $item->count : '')?>" min="0">
                                        </td>
                                        <td class="remove" data-key="<?=$key?>">
                                            <div class="fa fa-times"></div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </p>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>

        <script>
            $(".toggleable-src").bind("change", function() {
                $(".toggleable").hide().attr('disabled', true);
                $(".toggleable:not(." + $(this).data("target") + ") input").val('');
                $("." + $(this).data("target")).show().removeAttr('disabled');
                $("#brand").show();
            });


            $(".select-item select, .select-item input[type=radio], .select-item input[type=number]").bind("change", function() {
                var paramObj = {};
                var str = '';

                $.each($('form').serializeArray(), function(_, kv) {
                    if (kv.name != 'name' && kv.name != 'items' && kv.name != 'submit' && ! kv.name.match(/pump/)) {
                        paramObj[kv.name] = kv.value;
                        if (kv.value != '')
                            str += kv.value + ' ';

                        if (kv.name == 'power' && kv.value > 0)
                            str += 'кВт ';
                    }
                });

                str = str.trim();

                $("#name").val(str);
            });

            $(".add-item").bind("click", function() {
                var item = $("#name").val();

                $.ajax({
                    method: "POST",
                    url: "/boiler/add_pump",
                    data: {
                        item: item
                    }
                })
                    .done(function (data) {
                        try {
                            data = JSON.parse(data);
                        } catch (e) {
                            console.log(e);
                            $("#app").replaceWith(data);
                        }
                    });

                $("#name").val("");
            });


            $(".items input[type=number]").bind("change", function() {
                $.ajax({
                    method: "POST",
                    url: $("form").attr("action"),
                    data: $("form").serialize()
                })
            });



            $(".remove").bind("click", function() {
                var key = $(this).data("key");

                $.ajax({
                    method: "POST",
                    url: "/boiler/remove_pump",
                    data: {
                        key: key
                    }
                })
                    .done(function (data) {
                        try {
                            data = JSON.parse(data);
                        } catch (e) {
                            console.log(e);
                            $("#app").replaceWith(data);
                        }
                    });
            });
        </script>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
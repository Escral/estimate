<?php use App\Items;
use App\Models\Floor;
use App\View;
use App\Path;
$page = 'collector_piping';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'boiler']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>


        <style>
            #brand {
                display: none;
            }
        </style>

        
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/boiler/collector_piping" data-next="/boiler/pump" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Выберите группу быстрого монтажа</h5>
                    <div class="card-text">

                        <div class="row">
                            <div class="col-sm">
                                <div class="card">
                                    <img class="card-img-top" src="/public/img/collector_piping/1.png">
                                    <div class="card-body">
                                        <h5 class="card-title"><?=Items::getCollectorPipingTitle(1)?></h5>
                                        
                                        <div class="form-group">
                                            <label>Количество</label>
                                            <input name="type[1]" value="<?=$types[1] ?? 0?>" type="number" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="card">
                                    <img class="card-img-top" src="/public/img/collector_piping/1.png">
                                    <div class="card-body">
                                        <h5 class="card-title"><?=Items::getCollectorPipingTitle(2)?></h5>

                                        <div class="form-group">
                                            <label>Количество</label>
                                            <input name="type[2]" value="<?=$types[2] ?? 0?>" type="number" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="btn btn-primary select-item">Выбрать</div>

                        <br>
                        <br>
                        <br>



                        <?php foreach ($items as $key => $group) { ?>
                            <h5><?=Items::getCollectorPipingTitle($key)?> x <?=$group['count']?></h5>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Кол-во</th>
                                    </tr>
                                </thead>
                                <tbody class="items">
                                    <?php foreach ($group['items'] as $key => $item) { ?>
                                        <tr rel="<?=$item->id?>">
                                            <td><?=$item->title?></td>
                                            <td>
                                                <input type="number" step="1" name="collector_piping[<?=$item->id?>][count]" class="small-input" value="<?=($item->count ? $item->count : '')?>" min="0">
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <br>
                        <?php } ?>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>

        <script>
            $(".select-item").bind("click", function(e) {
                e.preventDefault();

                $.ajax({
                    method: "POST",
                    url: "/boiler/set_collector_piping_type",
                    data: $("form").serialize()
                })
                    .done(function (data) {
                        $("#app").replaceWith(data);
                    });

                $("#name").val("");
            });
        </script>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'kettles';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'boiler']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>


        <style>
            #brand {
                display: none;
            }
        </style>

        
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/boiler" data-next="/boiler/boiler" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Добавить котёл</h5>
                    <div class="card-text select-item">
                        <div class="form-group kettle-type">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input toggleable-src" data-target="select1" name="type1" type="radio" id="inp1" value="Газовый">
                                <label class="form-check-label" for="inp1">Газовый</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input toggleable-src" data-target="select2" name="type1" type="radio" id="inp2" value="Твердотопливный">
                                <label class="form-check-label" for="inp2">Твердотопливный</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input toggleable-src" data-target="select3" name="type1" type="radio" id="inp3" value="Электрический">
                                <label class="form-check-label" for="inp3">Электрический</label>
                            </div>
                        </div>

                        <div class="select1 toggleable" style="display: none;">
                            <div class="form-group">
                                <label>Выберите количество контуров</label><br>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="type2" type="radio" id="inp33" value="Одноконтурый">
                                    <label class="form-check-label" for="inp33">Одноконтурый</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="type2" type="radio" id="inp44" value="Двухконтурный">
                                    <label class="form-check-label" for="inp44">Двухконтурный</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Выберите тип</label><br>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="type3" type="radio" id="inp5" value="">
                                    <label class="form-check-label" for="inp5">Обычный</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="type3" type="radio" id="inp6" value="Конденсационный">
                                    <label class="form-check-label" for="inp6">Конденсационный</label>
                                </div>
                            </div>
                        </div>
                        <div class="select2 toggleable" style="display: none;">
                            <div class="form-group">
                                <label>Выберите тип</label><br>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="type5" type="radio" id="inp7" value="С ручной загрузкой">
                                    <label class="form-check-label" for="inp7">С ручной загрузкой</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="type5" type="radio" id="inp8" value="С автоматической загрузкой">
                                    <label class="form-check-label" for="inp8">С автоматической загрузкой</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Введите мощность</label>

                            <div class="input-group" style="width: 150px;">
                                <input name="power" type="number" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon1">кВт</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="width: 300px; display: none;" id="brand">
                            <label>Выберите бренд</label>

                            <select name="brand" class="form-control toggleable select1" disabled>
                                <option value="Airfel">Airfel</option>
                                <option value="Ariston">Ariston</option>
                                <option value="Bosch">Bosch</option>
                                <option value="Buderus">Buderus</option>
                                <option value="Chaffoteaux">Chaffoteaux</option>
                                <option value="Demrad">Demrad</option>
                                <option value="Fondital">Fondital</option>
                                <option value="Immergas" selected>Immergas</option>
                                <option value="Motan">Motan</option>
                                <option value="Vaillant">Vaillant</option>
                                <option value="Viessmann">Viessmann</option>
                                <option value="">Похуй</option>
                            </select>

                            <select name="brand" class="form-control toggleable select2" disabled>
                                <option value="Heiztechnik" selected>Heiztechnik</option>
                                <option value="">Похуй</option>
                            </select>

                            <select name="brand" class="form-control toggleable select3" disabled>
                                <option value="Delsot">Delsot</option>
                                <option value="Protherm" selected>Protherm</option>
                                <option value="Vaillant">Vaillant</option>
                                <option value="">Похуй</option>
                            </select>
                        </div>

                        <input type="text" id="name" class="form-control mb-3" placeholder="Название котла ...">


                        <button type="button" class="btn btn-primary add-item">Добавить</button>
                    </div>

                    <br>

                    <h5 class="card-title">Выбранные котлы</h5>
                    <p class="card-text">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Котёл</th>
                                    <th scope="col">Кол-во</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="items">
                                <?php foreach ($selected as $key => $item) { ?>
                                    <tr rel="<?=$item->id?>">
                                        <td><?=$item->title?></td>
                                        <td>
                                            <input type="number" step="1" name="kettles[<?=$key?>][count]" class="small-input" value="<?=($item->count ? $item->count : '')?>" min="0">
                                        </td>
                                        <td class="remove" data-key="<?=$key?>">
                                            <div class="fa fa-times"></div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </p>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>

        <script>
            $(".toggleable-src").bind("change", function() {
                $(".toggleable").hide().attr('disabled', true);
                $(".toggleable:not(." + $(this).data("target") + ") input").val('');
                $("." + $(this).data("target")).show().removeAttr('disabled');
                $("#brand").show();
            });


            $(".select-item select, .select-item input[type=radio], .select-item input[type=number]").bind("change", function() {
                var paramObj = {};
                var str = '';

                $.each($('form').serializeArray(), function(_, kv) {
                    if (kv.name != 'name' && kv.name != 'items' && kv.name != 'submit' && ! kv.name.match(/kettles/)) {
                        paramObj[kv.name] = kv.value;
                        if (kv.value != '')
                            str += kv.value + ' ';

                        if (kv.name == 'power' && kv.value > 0)
                            str += 'кВт ';
                    }
                });

                str = str.trim();

                $("#name").val(str);
            });

            $(".add-item").bind("click", function() {
                var item = $("#name").val();

                $.ajax({
                    method: "POST",
                    url: "/boiler/add_kettles",
                    data: {
                        item: item
                    }
                })
                    .done(function (data) {
                        try {
                            data = JSON.parse(data);
                        } catch (e) {
                            console.log(e);
                            $("#app").replaceWith(data);
                        }
                    });

                $("#name").val("");
            });


            $(".items input[type=number]").bind("change", function() {
                $.ajax({
                    method: "POST",
                    url: $("form").attr("action"),
                    data: $("form").serialize()
                })
            });


            $(".remove").bind("click", function() {
                var key = $(this).data("key");

                $.ajax({
                    method: "POST",
                    url: "/boiler/remove_kettle",
                    data: {
                        key: key
                    }
                })
                    .done(function (data) {
                        try {
                            data = JSON.parse(data);
                        } catch (e) {
                            console.log(e);
                            $("#app").replaceWith(data);
                        }
                    });
            });
        </script>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
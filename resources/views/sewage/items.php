<?php use App\Models\Floor;
use App\View;

ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'sewage']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <div class="col">
            <h4 class="floor"><?=Floor::getCurrentFloorName()?></h4><br>
        </div>

        <form action="/sewage" data-next="/floors" class="ajax-form" method="post" style="padding: 0 16px;">
            <div class="row">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-header">
                            ø110
                        </div>
                        <div class="card-body">
                            <p class="card-text">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Прибор</th>
                                    <th scope="col">Кол-во</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($items110 as $item) { ?>
                                    <tr>
                                        <td class="image"><img src="/public/img/sewage/<?=$item->title?>.jpg" height="60" alt=""></td>
                                        <td><?=$item->title?></td>
                                        <td><input type="number" step="1" name="items[<?=$item->id?>]" value="<?=($item->count ? $item->count : '')?>" min="0" style="width: 80px;"></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="card">
                        <div class="card-header">
                            ø50
                        </div>
                        <div class="card-body">
                            <p class="card-text">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Прибор</th>
                                    <th scope="col">Кол-во</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($items50 as $item) { ?>
                                    <tr>
                                        <td class="image"><img src="/public/img/sewage/<?=$item->title?>.jpg" height="60" alt=""></td>
                                        <td><?=$item->title?></td>
                                        <td><input type="number" step="1" name="items[<?=$item->id?>]" value="<?=($item->count ? $item->count : '')?>" min="0" style="width: 80px;"></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="card">
                        <div class="card-header">
                            ø32
                        </div>
                        <div class="card-body">
                            <p class="card-text">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Прибор</th>
                                    <th scope="col">Кол-во</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($items32 as $item) { ?>
                                    <tr>
                                        <td class="image"><img src="/public/img/sewage/<?=$item->title?>.jpg" height="60" alt=""></td>
                                        <td><?=$item->title?></td>
                                        <td><input type="number" step="1" name="items[<?=$item->id?>]" value="<?=($item->count ? $item->count : '')?>" min="0" style="width: 80px;"></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <br>

            <div class="row justify-content-md-center">
                <div class="col-sm-8 c">
                    <div class="card">
                        <div class="card-header">
                            Дополнительно
                        </div>
                        <div class="card-body">
                            <p class="card-text">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Прибор</th>
                                    <th scope="col">Кол-во</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($items as $item) { ?>
                                    <tr>
                                        <td class="image"><img src="/public/img/sewage/<?=$item->title?>.jpg" height="60" alt=""></td>
                                        <td><?=$item->title?></td>
                                        <td><input type="number" step="1" name="items[<?=$item->id?>]" value="<?=($item->count ? $item->count : '')?>" min="0" style="width: 80px;"></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            </p>

                            <input type="hidden" name="submit" value="1">
                            <button type="submit" class="btn btn-success">Далее</button>
                        </div>
                    </div>
                </div>
            </div>



        </form>

        <style>
            .table td {
                vertical-align: middle;
            }
            .image {
                text-align: center;
            }

            .card-header {
                text-align: center;
                font-weight: bold;
                font-size: 24px;
            }
        </style>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
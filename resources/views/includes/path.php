<?php

use App\Data;
use App\Models\Floor;
use App\Path;
?>
<nav class="path" aria-label="breadcrumb">
    <ol class="breadcrumb">
        <?php foreach(Path::getPath($page) as $name => $item) { ?>
            <li class="breadcrumb-item<?php if ($name == $page) { ?> active<?php } ?>"><?php if ($name !== $page) { ?><a
                href="<?=$item['path']?>"><?=$item['title']?></a><?php } else { ?><?=$item['title']?><?php } ?></li>
        <?php } ?>
    </ol>
</nav>

<!-- Example single danger button -->
<!--<div class="container mb-3 btn-group">-->
<!--    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
<!--        --><?//=Floor::getCurrentFloorName()?>
<!--    </button>-->
<!--    <div class="dropdown-menu">-->
<!--        --><?php //foreach (Floor::getFloors() as $floor) { ?>
<!--            <a class="dropdown-item ajax" href="/change-floor/--><?//=$floor['ID']?><!--">--><?//=$floor['title']?><!--</a>-->
<!--        --><?php //} ?>
<!--    </div>-->
<!--</div>-->

<?php
    Path::remember($page);
?>
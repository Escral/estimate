<?php
use App\Controllers\Alerts;
use App\Data;

?>
<nav class="navbar navbar-expand navbar-dark bg-primary fixed-top">
    <ul class="navbar-nav">
        <li style="margin-right: 195px;" class="nav-item<?=($page == 'floors' ? ' active' : '')?>">
            <a class="nav-link" href="/floors">
                <img src="/public/img/icons/floors.svg" height="28" class="mr-1">
                <?=lang("Выбор этажей")?>
            </a>
        </li>

        <li class="nav-item<?=($page == 'heating' ? ' active' : '')?>">
            <a class="nav-link" href="/radiators">
                <img src="/public/img/icons/heating.svg" height="28" class="mr-1"> 
                <?=lang("Отопление")?>
            </a>
        </li>
        <li class="nav-item<?=($page == 'water' ? ' active' : '')?>">
            <a class="nav-link" href="/water">
                <img src="/public/img/icons/water.png" height="28" class="mr-1"> 
                <?=lang("Водоснабжение")?>
            </a>
        </li>
        <li class="nav-item<?=($page == 'hfloors' ? ' active' : '')?>">
            <a class="nav-link" href="/hfloors">
                <img src="/public/img/icons/hfloors.png" height="28" class="mr-1"> 
                <?=lang("Тёплые полы")?>
            </a>
        </li>
        <li class="nav-item<?=($page == 'sewage' ? ' active' : '')?>">
            <a class="nav-link" href="/sewage">
                <img src="/public/img/icons/sewage.png" height="28" class="mr-1"> 
                <?=lang("Канализация")?>
            </a>
        </li>
        <li class="nav-item<?=($page == 'boiler' ? ' active' : '')?>">
            <a class="nav-link" href="/boiler">
                <img src="/public/img/icons/boiler.png" height="28" class="mr-1"> 
                <?=lang("Котельная")?>
            </a>
        </li>
        <li class="nav-item<?=($page == 'tools' ? ' active' : '')?>">
            <a class="nav-link" href="/tools">
                <img src="/public/img/icons/tools.png" height="28" class="mr-1"> 
                <?=lang("Инструмент")?>
            </a>
        </li>
<!--        <li style="margin-left: 200px;" class="nav-item--><?//=($page == 'conditioners' ? ' active' : '')?><!--">-->
<!--            <a class="nav-link" href="/conditioners">--><?//=lang("Кондиционеры")?><!--</a>-->
<!--        </li>-->

        <li class="nav-item ml-auto float-right<?=($page == 'export' ? ' active' : '')?>">
            <a class="nav-link" href="/export">
                <img src="/public/img/icons/export.png" height="28" class="mr-1">
                <?=lang("Export")?>
            </a>
        </li>
        <li class="nav-item ml-auto float-right clear-all">
            <a class="nav-link" href="/clear-all">
                <img src="/public/img/icons/clear.png" height="28" class="mr-1">
                <?=lang("Очистить")?>
            </a>
        </li>
    </ul>
</nav>


<script>
    $(".clear-all").bind("click", function (e) {
        e.preventDefault();

        if (confirm('Очистить все данные?')) {
            $.ajax({
                method: "POST",
                url: '/clear-all'
            })
                .done(function () {
                    window.location.href = "/";
                });
        }
    });

    $(function() {

        $(".icon--help").bind("click", function() {
            $(this).toggleClass("active");
        });

    });
</script>


<?php while($alert = Alerts::shift()) { ?>
    <script>
        alert("<?=$alert['message']?>");
    </script>
<!--    <div class="alert alert---><?//=$alert['tag']?><!--" role="alert">-->
<!--        --><?//=$alert['message']?>
<!--    </div>-->
<?php } ?>

<!--<div class="debug">-->
<!--    <pre>--><?php //print_r(Data::getAll())?><!--</pre>-->
<!---->
<!--    <style>-->
<!--        #app {-->
<!--            padding-right: 30%;-->
<!--        }-->
<!--        .debug {-->
<!--            position: fixed;-->
<!--            height: 100%;-->
<!--            width: 30%;-->
<!--            background: #FFF;-->
<!--            border-left: 1px solid #CCC;-->
<!--            box-sizing: border-box;-->
<!--            right: 0;-->
<!--            top: 0;-->
<!--            padding: 30px;-->
<!--            overflow-y: scroll;-->
<!--        }-->
<!--    </style>-->
<!--</div>-->
<!---->

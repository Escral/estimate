<div class="sidebar sidebar-main" id="sidebar">
    <div class="sidebar-content">
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <li class="navigation-header">
                        <span>Сметы</span>
                    </li>

                    <li class="new-estimate">
                        <a class="legitRipple"><i class="icon-home4"></i> <span>+ Новая смета</span></a>
                    </li>

                    <?php use App\Models\Estimate;

                    foreach ($estimates as $estimate) { ?>
                        <li<?=($estimate->isActive() ? ' class="active"' : '')?> data-id="<?=$estimate->ID?>">
                            <a class="estimate-change">
                                <span><?=$estimate->title?></span>
                            </a>
                            <div class="estimate-remove"><i class="fa fa-times"></i></div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>

    <script>
        let initChange = function() {
            $(".estimate-change").unbind('click').bind('click', function() {
                let button = $(this);

                $.ajax({
                    method: "POST",
                    url: "/estimate/change",
                    data: {
                        id: button.parent().data("id")
                    }
                })
                    .done(function () {
                        window.location.href= "/";
                    });
            });
        };

        let initRemove = function() {
            $(".estimate-remove").unbind('click').bind('click', function() {
                let button = $(this);

                if (! confirm("Удалить смету?"))
                    return false;

                $.ajax({
                    method: "POST",
                    url: "/estimate/delete",
                    data: {
                        id: button.parent().data("id")
                    }
                })
                    .done(function () {
                        if (button.parent().data("id") == "<?=Estimate::getActiveId()?>") {
                            window.location.reload();
                        }

                        button.parent().replaceWith("");
                    });
            });
        };

        let save = function() {
            let input = $(".estmate-input");
            let link = input.find("a");
            let title = link.text();

            if (title == '') {
                input.replaceWith("");
                return false;
            }

            link.unbind('blur keypress').attr('contenteditable', false);

            $.ajax({
                method: "POST",
                url: "/estimate/add",
                data: {
                    title: title
                }
            })
                .done(function (data) {
                    try {
                        data = JSON.parse(data);
                    } catch (e) {
                        console.log(e);
                    }

                    window.location.href= "/";

                    $("<div class=\"estimate-remove\"><i class=\"fa fa-times\"></i></div>").insertAfter(link);

                    input.attr("data-id", data.id);

                    input.removeClass("estmate-input");

                    initRemove();
                    initChange();
                });
        };

        $(".new-estimate").bind("click", function() {
            $("<li class='estmate-input'>\n" +
                "    <a class=\"estimate-change\" contenteditable></a>\n" +
                "</li>").insertAfter(this);

            $(".estmate-input a").focus().bind('blur', save).bind('keypress', function (e) {
                if (e.keyCode === 13)
                    save();
            });
        });

        initRemove();
        initChange();
    </script>
</div>
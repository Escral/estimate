<?php use App\View;

ob_start(); ?>

    <form action="/login" method="post">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    Авторизация
                </div>
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <label>Логин</label>
                            <input type="text" class="form-control<?=isset($_SESSION['error']) ? ' is-invalid' : ''?>" aria-describedby="emailHelp" placeholder="Введите логин" name="login" required value="<?=$_POST['login'] ?? ''?>">
                            <div class="invalid-feedback">
                                <?=$_SESSION['error'] ?? ''?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Пароль</label>
                            <input type="password" class="form-control" placeholder="Введите пароль" name="password" required>
                        </div>

                        <button type="submit" class="btn btn-primary">Вход</button>
                    </form>

                    <br>
                    <br>

                    <div class="text-center">
                        <a href="/register">Регистрация</a>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .container {
                padding: 60px 0;
                max-width: 400px;
            }
            .card-header {
                text-align: center;
                font-weight: bold;
                font-size: 24px;
            }
        </style>
    </form>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
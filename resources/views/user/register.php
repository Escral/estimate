<?php ob_start(); ?>

    <form action="/register" method="post">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    Регистрация
                </div>
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <label>Логин</label>
                            <input type="text" class="form-control<?=isset($_SESSION['error']['login']) ? ' is-invalid' : ''?>" aria-describedby="emailHelp" placeholder="Введите логин" name="login" required pattern="[A-Za-z\-_0-9]+" value="<?=$_POST['login'] ?? ''?>">
                            <small class="form-text text-muted">Только латинскаие буквы, цифры, знаки "- _"</small>
                            <div class="invalid-feedback">
                                <?=$_SESSION['error']['login'] ?? ''?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Пароль</label>
                            <input type="password" class="form-control<?=isset($_SESSION['error']['password']) ? ' is-invalid' : ''?>" placeholder="Введите пароль" name="password" pattern=".{5,}" required>
                            <div class="invalid-feedback">
                                <?=$_SESSION['error']['password'] ?? ''?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Пароль ещё раз</label>
                            <input type="password" class="form-control<?=isset($_SESSION['error']['password_repeat']) ? ' is-invalid' : ''?>" placeholder="Повторите пароль" name="password_repeat" pattern=".{5,}" required>

                            <div class="invalid-feedback">
                                <?=$_SESSION['error']['password_repeat'] ?? ''?>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
                    </form>

                    <br>
                    <br>

                    <div class="text-center">
                        <a href="/login">Вход</a>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .container {
                padding: 60px 0;
                max-width: 400px;
            }
            .card-header {
                text-align: center;
                font-weight: bold;
                font-size: 24px;
            }
        </style>
    </form>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
<?php use App\View;

ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'tools']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <form action="/tools" data-next="/floors" class="ajax-form" method="post" style="padding: 0 16px;">

            <div class="container">
                <div class="card">
                    <div class="card-header">
                        Инструменты
                    </div>
                    <div class="card-body">
                        <p class="card-text">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Инструмент</th>
                                        <th scope="col">Кол-во</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($items as $item) { ?>
                                        <tr>
                                            <td class="image-td"><img src="/public/img/tools/<?=$item->id?>.jpg" height="60" alt=""></td>
                                            <td><?=$item->title?></td>
                                            <td style="text-align: center;"><input type="checkbox" name="items[<?=$item->id?>]" value="1" <?php if ($item->count) { ?> checked<?php } ?> style="width: 30px; height: 30px; cursor: pointer;"></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                            <input type="hidden" name="submit" value="1">
                            <button type="submit" class="btn btn-success">Далее</button>
                        </p>
                    </div>
                </div>
            </div>

            <style>
                .card-header {
                    text-align: center;
                    font-weight: bold;
                    font-size: 24px;
                }
            </style>
        </form>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
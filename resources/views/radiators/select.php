<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'select';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'heating']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/radiators" data-next="/radiators/futorki" class="card-body ajax-form" method="post">
                <h5 class="card-title">Добавить радиатор</h5>
                    <div class="card-text select-item">
                        <div class="form-group radiator-type">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input toggleable-src" data-group="type" data-target="select1" name="type1" type="radio" id="inp1" value="Стальной">
                                <label class="form-check-label" for="inp1">Стальной</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input toggleable-src" data-group="type" data-target="select2" name="type1" type="radio" id="inp2" value="Алюминиевый">
                                <label class="form-check-label" for="inp2">Алюминиевый</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input toggleable-src" data-group="type" data-target="select3" name="type1" type="radio" id="inp3" value="Биметаллический">
                                <label class="form-check-label" for="inp3">Биметаллический</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input toggleable-src" data-group="type" data-target="select4" name="type1" type="radio" id="inp4" value="Полотенцесушитель">
                                <label class="form-check-label" for="inp4">Полотенцесушитель</label>
                            </div>
                        </div>

                        <div class="select1 toggleable type" style="display: none">
                            <div class="form-group">
                                <label>Выберите тип</label><br>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="type2" type="radio" id="inp5" value="тип 11">
                                    <label class="form-check-label" for="inp5">11</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="type2" type="radio" id="inp6" value="тип 22">
                                    <label class="form-check-label" for="inp6">22</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="type2" type="radio" id="inp6" value="тип 33">
                                    <label class="form-check-label" for="inp6">33</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Выберите высоту</label><br>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length1" name="type3" type="radio" id="inp7" value="300 x">
                                    <label class="form-check-label" for="inp7">300</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length1" name="type3" type="radio" id="inp8" value="500 x">
                                    <label class="form-check-label" for="inp8">500</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length2" name="type3" type="radio" id="inp9" value="700 x">
                                    <label class="form-check-label" for="inp9">700</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length2" name="type3" type="radio" id="inp10" value="900 x">
                                    <label class="form-check-label" for="inp10">900</label>
                                </div>
                            </div>
                        
                            <div class="length1 toggleable length" style="display: none;">
                                <div class="form-group">
                                    <label>Выберите длину</label><br>
                                    <select name="width" class="form-control" style="width: 100px;">
                                        <option value="">--</option> 
                                        <option value="400">400</option>  
                                        <option value="500">500</option>  
                                        <option value="600">600</option>  
                                        <option value="700">700</option>  
                                        <option value="800">800</option>  
                                        <option value="900">900</option>  
                                        <option value="1000">1000</option>
                                        <option value="1100">1100</option>
                                        <option value="1200">1200</option>
                                        <option value="1300">1300</option>
                                        <option value="1400">1400</option>
                                        <option value="1500">1500</option>
                                        <option value="1600">1600</option>
                                        <option value="1700">1700</option>
                                        <option value="1800">1800</option>
                                        <option value="1900">1900</option>
                                        <option value="2000">2000</option>
                                        <option value="2100">2100</option>
                                        <option value="2200">2200</option>
                                        <option value="2300">2300</option>
                                        <option value="2400">2400</option>
                                        <option value="2500">2500</option>
                                        <option value="2600">2600</option>
                                        <option value="2700">2700</option>
                                        <option value="2800">2800</option>
                                        <option value="2900">2900</option>
                                        <option value="3000">3000</option>    
                                    <select>
                                </div>
                            </div>

                            
                            <div class="length2 toggleable length" style="display: none;">
                                <div class="form-group">
                                    <label>Выберите длину</label><br>
                                    <select name="width" class="form-control" style="width: 100px;">
                                        <option value="">--</option> 
                                        <option value="400">400</option>  
                                        <option value="500">500</option>  
                                        <option value="600">600</option>  
                                        <option value="700">700</option>  
                                        <option value="800">800</option>  
                                        <option value="900">900</option>  
                                        <option value="1000">1000</option>
                                    <select>
                                </div>
                            </div>
                        </div>

                        <div class="select2 toggleable type" style="display: none">
                            
                            <div class="form-group">
                                <label>Выберите высоту</label><br>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length1" name="type3" type="radio" id="inp21" value="350">
                                    <label class="form-check-label" for="inp21">350</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length1" name="type3" type="radio" id="inp22" value="500">
                                    <label class="form-check-label" for="inp22">500</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length1" name="type3" type="radio" id="inp23" value="600">
                                    <label class="form-check-label" for="inp23">600</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length2" name="type3" type="radio" id="inp24" value="700">
                                    <label class="form-check-label" for="inp24">700</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length2" name="type3" type="radio" id="inp25" value="800">
                                    <label class="form-check-label" for="inp25">800</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length1" name="type3" type="radio" id="inp26" value="1000">
                                    <label class="form-check-label" for="inp26">1000</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length1" name="type3" type="radio" id="inp27" value="1200">
                                    <label class="form-check-label" for="inp27">1200</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Введите количество секций</label>

                                <div class="input-group" style="width: 150px;">
                                    <input name="sections" type="number" class="form-control sections" min="1">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon1"> секций</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="select3 toggleable type" style="display: none">
                            
                            <div class="form-group">
                                <label>Выберите высоту</label><br>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length1" name="type3" type="radio" id="inp31" value="350">
                                    <label class="form-check-label" for="inp31">350</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="length" data-target="length1" name="type3" type="radio" id="inp32" value="500">
                                    <label class="form-check-label" for="inp32">500</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Введите количество секций</label>

                                <div class="input-group" style="width: 150px;">
                                    <input name="sections" type="number" class="form-control sections" min="1">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon1"> секций</span>
                                    </div>
                                </div>
                            </div>
                        </div>





                        <div class="select4 toggleable type" style="display: none">

                            <div class="form-group">
                                <label>Выберите тип</label><br>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="polot-type" data-target="polot-type1" name="type3" type="radio" id="inp41" value="нержавеющий">
                                    <label class="form-check-label" for="inp41">нержавеющий</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input toggleable-src" data-group="polot-type" data-target="polot-type1" name="type3" type="radio" id="inp42" value="стальной">
                                    <label class="form-check-label" for="inp42">стальной</label>
                                </div>
                            </div>

                            <div style="padding: 0 15px">
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label>Введите ширину</label>

                                        <div class="input-group" style="width: 150px;">
                                            <input name="polot-width" type="number" class="form-control sections" min="100" step="100">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon1"> мм</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-6">
                                        <label>Введите ширину</label>

                                        <div class="input-group" style="width: 150px;">
                                            <input name="length" type="number" class="form-control sections" min="100" step="100">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon1"> мм</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <input type="text" id="name" class="form-control mb-3" placeholder="Название радиатора ...">

                        <button type="button" class="btn btn-primary add-item">Добавить</button>
                    </div>


                    <!-- <h5 class="card-title">
                        Добавить радиатор

                        <a class="float-right" style="font-size: 14px" href="https://wpcalc.com/sections-of-the-radiator/">Cсылка на подбор секционных радиаторов</a>
                    </h5>
                    <p class="card-text">
                        <input type="text" class="form-control search mb-3" placeholder="Поиск...">

                        <select class="form-control mb-3 items-to-add" size="5" multiple>
                            <?php foreach ($items as $item) { ?>
                                <option value="<?=$item->id?>"><?=$item->title?></option>
                            <?php } ?>
                        </select>

                        <button type="button" class="btn btn-primary add-item">Добавить</button>
                    </p> -->

                    <br>

                    <h5 class="card-title">Выбранные радиаторы</h5>
                    <p class="card-text">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Радиатор</th>
                                    <th scope="col">Кол-во</th>
                                    <th scope="col">Кол-во секций</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody class="items">
                                <?php foreach ($selected as $key => $item) { ?>
                                    <tr rel="<?=$item->id?>">
                                        <td><?=$item->title?></td>
                                        <td>
                                            <input type="number" step="1" name="items[<?=$key?>][count]" class="small-input" value="<?=($item->count ? $item->count : '')?>" min="0">
                                        </td>
                                        <td>
                                            <?php if ($item->withSections()) { ?>
                                                <input type="number" step="1" name="items[<?=$key?>][sections]" class="small-input" value="<?=($item->sections ? $item->sections : '')?>" min="0">
                                            <?php } ?>
                                        </td>
                                        <td class="remove" data-key="<?=$key?>">
                                            <div class="fa fa-times"></div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button id="submit" type="submit" class="btn btn-success">Далее</button>


                    <div class="alert alert-warning alert-inline" role="alert">
                        Радиатор в котельной указывать не нужно
                    </div>
                </form>
            </div>
        </div>

        <script>
            $(".toggleable-src").bind("change", function() {
                $(".toggleable." + $(this).data("group")).hide().attr('disabled', true);
                $(".toggleable." + $(this).data("group") + ":not(." + $(this).data("target") + ") input").prop("checked", false);
                $(".toggleable." + $(this).data("group") + ":not(." + $(this).data("target") + ") select").val('');
                $("." + $(this).data("target")).show().removeAttr('disabled');
            });


            $(".select-item select, .select-item input[type=radio], .select-item input[type=number]").bind("change", function() {
                var paramObj = {};
                var str = '';

                $.each($('form').serializeArray(), function(_, kv) {
                    if (kv.name != 'name' && kv.name != 'items' && kv.name != 'submit' && kv.name != 'sections' && ! kv.name.match(/items/)) {
                        paramObj[kv.name] = kv.value;
                        if (kv.value != '')
                            str += kv.value + ' ';

                        if (kv.name == 'sections' && kv.value > 0)
                            str += 'секций ';

                        if (kv.name == 'polot-width' && kv.value > 0)
                            str += 'x ';
                    }
                });

                str = str.trim();

                $("#name").val(str);
            });

            $(".add-item").bind("click", function() {
                var item = $("#name").val();
                var sections = $(".toggleable.type:visible .sections:not([disabled])").val();

                $.ajax({
                    method: "POST",
                    url: "/radiators/add_radiators",
                    data: {
                        item: item,
                        sections: sections
                    }
                })
                    .done(function (data) {
                        try {
                            data = JSON.parse(data);
                        } catch (e) {
                            console.log(e);
                            $("#app").replaceWith(data);
                        }
                    });

                $("#name").val("");
            });

            $(".items input[type=number]").bind("change", function() {
                $.ajax({
                    method: "POST",
                    url: $("form").attr("action"),
                    data: $("form").serialize()
                })
            });



            $(".remove").bind("click", function() {
                var key = $(this).data("key");

                $.ajax({
                    method: "POST",
                    url: "/radiators/remove_radiators",
                    data: {
                        key: key
                    }
                })
                    .done(function (data) {
                        try {
                            data = JSON.parse(data);
                        } catch (e) {
                            console.log(e);
                            $("#app").replaceWith(data);
                        }
                    });
            });
        </script>

        <script>
            // jQuery.fn.filterByText = function(textbox) {
            //     return this.each(function() {
            //         var select = this;
            //         var options = [];
            //         $(select).find('option').each(function() {
            //             options.push({
            //                 value: $(this).val(),
            //                 text: $(this).text()
            //             });
            //         });
            //         $(select).data('options', options);

            //         $(textbox).bind('change keyup', function() {
            //             var options = $(select).empty().data('options');
            //             var search = $.trim($(this).val());

            //             var searchQ = "";
            //             $.each(search.split(" "), function (i, el) {
            //                 searchQ += "([\\s\\S]*)" + el;
            //             });

            //             var regex = new RegExp(searchQ, "gi");

            //             $.each(options, function(i) {
            //                 var option = options[i];
            //                 if (option.text.match(regex) !== null) {
            //                     $(select).append(
            //                         $('<option>').text(option.text).val(option.value)
            //                     );
            //                 }
            //             });
            //         });
            //     });
            // };

        </script>

        <script>
            $("#submit").click(function(){
                if (<?=count($selected ?? [])?> == 0) {
                    alert("Выберите радиаторы");
                    return false;
                }
            });
        </script>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
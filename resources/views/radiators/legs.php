<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'legs';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'heating']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/radiators/legs" data-next="/radiators/scheme" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Введите кол-во пар ножек для радиаторов</h5>
                    <p class="card-text">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th scope="col"></th>
                                    <th scope="col">Кол-во пар ножек</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($items as $item) { ?>
                                    <tr>
                                        <td class="image-td"><img src="/public/img/items/<?=$item->id?>.jpg" width="80" height="80"></td>
                                        <td><?=$item->title?></td>
                                        <td>
                                            <input type="number" name="items[<?=$item->id?>]" value="<?=($item->count ? $item->count : '')?>" min="0" step="1"> пар
                                    
                                            <i class="icon icon--help"><p>Подсчитывается исходя из общего количества бимметаллических и алюминиевых радиаторов</p></i>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </p>

                    <div class="alert alert-warning alert-inline" role="alert">
                        Если ножки не требуются оставьте значения пустыми и нажмите <b>Далее</b>
                    </div>

                    <br>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
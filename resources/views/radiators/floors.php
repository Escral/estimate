<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'floors';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'heating']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>
                </div>
                <form action="/" data-next="/radiators" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Сколько этажей у вашего дома?</h5>
                    <p class="card-text">
                        <ul class="list-group">
                            <?php foreach (Floor::getFloors() as $floor) { ?>
                                <li class="list-group-item">
                                    <?=$floor['title']?>

                                    <div class="col-auto float-right">
                                        <select name="floors[<?=$floor['ID']?>]" class="form-control">
                                            <option<?=($floor['value'] == 1 ? ' selected' : '')?> value="1">Отапливаемый радиатором</option>
                                            <option<?=($floor['value'] == 2 ? ' selected' : '')?> value="2">Отапливаемый тёплым полом</option>
                                            <option<?=($floor['value'] == 3 ? ' selected' : '')?> value="3">Отапливаемый радиатором и тёплым полом</option>
                                            <option<?=($floor['value'] == 0 ? ' selected' : '')?> value="0">Неотапливаемый</option>
                                        </select>
                                    </div>
                                </li>

                                <?php if ($nextFloor - 1 == $floor['ID']) { ?>
                                    <li class="list-group-item">
                                        <a href="#" class="add-floor">Добавить этаж</a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </p>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>

        <script>
            var n = <?=$nextFloor?>;

            $(".add-floor").bind("click", function(e) {
                e.preventDefault();

                $(this).parent().before("<li class=\"list-group-item\">\n" +
                    "    " + n + " этаж\n" +
                    "\n" +
                    "    <div class=\"col-auto float-right\">\n" +
                    "        <select name=\"floors[" + n + "]\" class=\"form-control\">\n" +
                    "            <option value=\"1\">Отапливаемый радиатором</option>\n" +
                    "            <option value=\"2\">Отапливаемый тёплым полом</option>\n" +
                    "            <option value=\"3\">Отапливаемый радиатором и тёплым полом</option>\n" +
                    "            <option value=\"0\">Неотапливаемый</option>\n" +
                    "        </select>\n" +
                    "    </div>\n" +
                    "</li>");

                n++;
            });
        </script>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
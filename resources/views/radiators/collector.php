<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'collector';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'heating']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/radiators/collector" data-next="/radiators/gerpex" class="card-body ajax-form" method="post">
                    <p class="card-text">
                        <div class="row">
                            <div class="col-sm-6">
                                <label style="font-size: 18px;" class="card-title">Коллектор для радиаторов на <input type="number" name="collector" value="<?=$collector ?? $count?>" style="width: 50px;" min="0" step="1"> выход(-ов)</label>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-check">
                                    <input class="form-check-input toggleable-src" data-target="image1" <?php if ($flowmeter == 1) { ?>checked<?php } ?> type="radio" name="flowmeter" id="flowmeter1" value="1" required>
                                    <label class="form-check-label" for="flowmeter1">
                                        Без расходомеров
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input toggleable-src" data-target="image2" <?php if ($flowmeter == 2) { ?>checked<?php } ?> type="radio" name="flowmeter" id="flowmeter2" value="2" required>
                                    <label class="form-check-label" for="flowmeter2">
                                        С расходомерами
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <img src="/public/img/radiatori_lucevaea_bez_rashodomerom.jpg" width="100%" class="toggleable image1" style="display: none">
                                <img src="/public/img/radiatori_lucevaea_s_rashodomerom.jpg" width="100%" class="toggleable image2" style="display: none">
                            </div>
                        </div>

                        <br>
                        <br>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th scope="col"></th>
                                    <th scope="col">Кол-во</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="image-td"><img src="/public/img/items/<?=COLLECTOR_TAILSTOCKS?>.jpg" width="60" height="60"></td>
                                    <td>Концевики на коллектор</td>
                                    <td>
                                        <input type="number" step="1" name="tailstocks" value="<?=($tailstocks ?? 2)?>" min="0"> шт.
                                        <i class="icon icon--help"><p>Текст</p></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="image-td"><img src="/public/img/items/<?=COLLECTOR_VALVES?>.jpg" width="60" height="60"></td>
                                    <td>Пара кранов с американкой 1" мама-папа</td>
                                    <td>
                                        <input type="number" step="1" name="col_valves" value="<?=($col_valves ?? 2)?>" min="0"> шт.
                                        <i class="icon icon--help"><p>Текст</p></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="image-td"><img src="/public/img/items/<?=COLLECTOR_MP?>.jpg" width="60" height="60"></td>
                                    <td>МП/ППР 32x1" папа</td>
                                    <td>
                                        <input type="number" step="1" name="mp" value="<?=($mp ?? 2)?>" min="0"> шт.
                                        <i class="icon icon--help"><p>Текст</p></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="image-td"><img src="/public/img/items/<?=COLLECTOR_CORNERS?>.jpg" width="60" height="60"></td>
                                    <td>Угол бронзовый 1" мама-папа</td>
                                    <td>
                                        <input type="number" step="1" name="col_corners" value="<?=($col_corners ?? 2)?>" min="0"> шт.
                                        <i class="icon icon--help"><p>Текст</p></i>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>

        <style>
            .toggleable {
                display: none;
            }
        </style>

        <script>
            $(".toggleable-src").bind("change", function() {
                $(".toggleable").hide().attr('disabled', true);
                $(".toggleable:not(." + $(this).data("target") + ") input").val('');
                $("." + $(this).data("target")).show().removeAttr('disabled');
                $("#brand").show();
            });
            

            $(".toggleable-src:checked").change();
        </script>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'gerpex';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'heating']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/radiators/gerpex" data-next="/radiators/locker" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Выберите герпексы для коллекторов Ø16x3/4</h5>

                    <p class="card-text">
                        <div class="row">
                            <div class="col-md-8">
                                <label style="font-size: 18px;" class="card-title">Вам потребуется <input class="small-input" type="number" name="gerpex" value="<?=$gerpex ?? $count?>" min="0" step="1"> герпекса(-ов)</label>
                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <img src="/public/img/items/<?=COLLECTOR_GERPEX?>.jpg" width="334">
                                </div>
                            </div>
                        </div>
                    </p>

                    <hr>

                    <div class="form-group">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
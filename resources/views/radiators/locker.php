<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'locker';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'heating']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/radiators/locker" data-next="/radiators/valves_metal" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Вам требуется шкаф для коллектора?</h5>
                    <p class="card-text">
                        <div class="form-check locker-isset">
                            <input class="form-check-input" type="radio" name="locker[isset]" <?php if (isset($locker['isset']) and $locker['isset'] == 1) { ?>checked<?php } ?> id="locker_isset1" value="1" required>
                            <label class="form-check-label" for="locker_isset1">
                                Да
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="locker[isset]" <?php if (isset($locker['isset']) and $locker['isset'] == 0) { ?>checked<?php } ?> id="locker_isset2" value="0" required>
                            <label class="form-check-label" for="locker_isset2">
                                Нет
                            </label>
                        </div>

                        <br>

                        <div class="row locker-params" style="display: none;">
                            <div class="col-sm-6">
                                <label style="font-size: 18px;" class="card-title">Вам потребуется шкаф шириной не менее <input type="number" name="locker[size]" value="<?=$locker['size'] ?? $size?>" style="width: 80px;" min="0" step="1"> см</label>
                            </div>
                        </div>

                        <div class="row locker-params" style="display: none;">
                            <div class="col-sm-3">
                                <div class="form-check">
                                    <input required class="form-check-input required toggleable-src" data-target="image1" <?php if (isset($locker['type']) and $locker['type'] == 1) { ?>checked<?php } ?> type="radio" name="locker[type]" id="locker1" value="1">
                                    <label class="form-check-label" for="locker1">
                                        Встроенный
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input required class="form-check-input toggleable-src" data-target="image2" <?php if (isset($locker['type']) and $locker['type'] == 2) { ?>checked<?php } ?> type="radio" name="locker[type]" id="locker2" value="2">
                                    <label class="form-check-label" for="locker2">
                                        Наружный
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <img src="/public/img/collector_locker_vstroennii.jpg" width="100%" class="toggleable image1" style="display: none">
                                <img src="/public/img/collector_locker_narujnii.jpg" width="100%" class="toggleable image2" style="display: none">
                            </div>
                        </div>
                    </p>

                    <hr>

                    <div class="form-group">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"></textarea>
                    </div>

                    <script>
                        $("#locker_isset1").bind("change", function() {
                            $(".locker-params").show();
                            $(".locker-params input").attr('disabled', false);
                        });
                        $("#locker_isset2").bind("change", function() {
                            $(".locker-params").hide();
                            $(".locker-params input").attr('disabled', true);
                        });
                        $(".locker-isset input:checked").change();
                    </script>

                    <script>
                        $(".toggleable-src").bind("change", function() {
                            $(".toggleable").hide().attr('disabled', true);
                            $(".toggleable:not(." + $(this).data("target") + ") input").val('');
                            $("." + $(this).data("target")).show().removeAttr('disabled');
                            $("#brand").show();
                        });

                        $(".toggleable-src:checked").change();
                    </script>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
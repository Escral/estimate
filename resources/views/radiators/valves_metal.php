<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'valves_metal';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'heating']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/radiators/valves_metal" data-next="/radiators/gerpex_valves" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Выберите краны для радиаторов под металлопласт Ø16</h5>
                    <p class="card-text">
                        <div class="alert alert-info alert-inline" role="alert">
                            Вам требуется <b id="in"><?=($maxValves / 2)?></b> кранов подач
                            <span id="inMore" class="negative float-right"></span>
                            <br>
                            Вам требуется <b id="out"><?=($maxValves / 2)?></b> кранов обраток
                            <span id="outMore" class="negative float-right"></span>
                        </div>

                        <br>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th scope="col"></th>
                                    <th scope="col">Кол-во</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($items as $n => $item) { $n++; ?>
                                    <tr>
                                        <td class="image-td"><img src="/public/img/items/<?=$item->id?>.jpg" width="60" height="60"></td>
                                        <td><?=$item->title?></td>
                                        <td><input id="item_<?=$n?>" type="number" name="items[<?=$item->id?>]" value="<?=($item->count ? $item->count : '')?>" min="0"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success" id="submit">Далее</button>
                </form>
            </div>
        </div>

        <script>
            var vIn = $("#in"),
                vOut = $("#out"),
                allIn = <?=($maxValves / 2)?>,
                allOut = <?=($maxValves / 2)?>;

            $("#item_1, #item_3, #item_5, #item_7").bind("change keyup", function() {
                let value1 = $("#item_1").val() * 1;
                let value2 = $("#item_3").val() * 1;
                let value3 = $("#item_5").val() * 1;
                let value4 = $("#item_7").val() * 1;
                let value = value1 + value2 + value3 + value4;
                let inValue = parseInt(vIn.text());
                let outValue = parseInt(vOut.text());

                if (value > allIn) {
                    $("#inMore").html("Обратите внимание, вы выбрали на <b>" + (value - allIn) + "</b> крана(-ов) больше чем требуется");
                    value = allIn;
                } else {
                    $("#inMore").html("");
                }

                vIn[0].innerHTML = allIn - value;
            });

            $("#item_2, #item_4, #item_6, #item_8").bind("change keyup", function() {
                let value1 = $("#item_2").val() * 1;
                let value2 = $("#item_4").val() * 1;
                let value3 = $("#item_6").val() * 1;
                let value4 = $("#item_8").val() * 1;
                let value = value1 + value2 + value3 + value4;
                let inValue = parseInt(vIn.text());
                let outValue = parseInt(vOut.text());

                if (value > allOut) {
                    $("#outMore").html("Обратите внимание, вы выбрали на <b>" + (value - allOut) + "</b> крана(-ов) больше чем требуется");
                    value = allOut;
                } else {
                    $("#outMore").html("");
                }

                vOut[0].innerHTML = allOut - value;
            });

            $("#submit").click(function(){
                let count = parseInt($("#in").text()) + parseInt($("#out").text());
                if (parseInt($("#in").text()) > 0 || parseInt($("#out").text())) {
                    if (! confirm("Обратите внимание, вам требуется выбрать ещё " + count + " кранов. Продолжить?")) {
                        return false;
                    }
                }
            });

            $("#item_1, #item_2").change();
        </script>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
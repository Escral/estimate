<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'water_floors';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'water']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>
                </div>
                <form action="/water" data-next="/water/scheme" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Сколько этажей у вашего дома?</h5>
                    <p class="card-text">
                        <ul class="list-group">
                            <?php foreach (Floor::getFloors() as $floor) { ?>
                                <li class="list-group-item">
                                    <?=$floor['title']?>

                                    <input type="hidden" name="floors[<?=$floor['ID']?>]" value="1">
                                </li>

                                <?php if ($nextFloor - 1 == $floor['ID']) { ?>
                                    <li class="list-group-item">
                                        <a href="#" class="add-floor">Добавить этаж</a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>

        <script>
            var n = <?=$nextFloor?>;

            $(".add-floor").bind("click", function(e) {
                e.preventDefault();

                $(this).parent().before("<li class=\"list-group-item\">\n" +
                    "    " + n + " этаж\n" +
                    "\n" +
                    "    <input type=\"hidden\" name=\"floors[" + n + "]\" value=\"1\">" +
                    "</li>");

                n++;
            });
        </script>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
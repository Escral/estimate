<?php use App\Models\Floor;
use App\Path;
use App\View;
$page = 'water_scheme';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'water']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/water" data-next="/water/items" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Выберите схему разводки водоснабжения</h5>
                    <p class="card-text">
                        <div class="form-check with-image inline">
                            <label class="form-check-label" for="scheme1">
                                <img src="/public/img/dvutrubnaea_na_ppr_water.jpg" height="180">
                                Двухтрубная на ППР
                            </label>
                            <input class="form-check-input" <?php if ($scheme == 1) { ?>checked<?php } ?> type="radio" name="scheme" id="scheme1" value="1" required>
                        </div>
                        <div class="form-check with-image inline">
                            <label class="form-check-label" for="scheme2">
                                <img src="/public/img/lucevaea_na_metaloplaste_water.jpg" height="180">
                                Лучевая на металлопласте
                            </label>
                            <input disabled class="form-check-input" <?php if ($scheme == 2) { ?>checked<?php } ?> type="radio" name="scheme" id="scheme2" value="2" required>
                        </div>
                    </p>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
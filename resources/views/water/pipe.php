<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'water_pipe';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'water']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/water/pipe" data-next="/water/insulation" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Введите метраж трубы без подводок (холодная + горячая)</h5>
                    <p class="card-text">
                        <div class="row">
                            <div class="col-md-8">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th scope="col">Труба</th>
                                            <th scope="col">Кол-во метров</th>
                                            <th scope="col">Кол-во приборов<br> водоснабжения на трубе</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($items as $item) { ?>
                                            <tr>
                                                <td class="image-td"><img src="/public/img/items/<?=$item->id?>.jpg" width="60" height="60"></td>
                                                <td><?=$item->title?></td>
                                                <td>
                                                    <input type="number" step="1" name="items[<?=$item->id?>]" value="<?=($item->count ? $item->count : '')?>" min="0">
                                                    
                                                    <div class="icon icon--help"><p>Подсчитывается исходя из общего количества бимметаллических и алюминиевых радиаторов</p></div>
                                                </td>
                                                <td>
                                                    <input class="on-pipes" type="number" step="1" name="water[<?=$item->id?>]" value="<?=($on_pipes[$item->id] ? $on_pipes[$item->id] : '')?>" min="0">
                                                    
                                                    <i class="icon icon--help"><p>Подсчитывается исходя из общего количества бимметаллических и алюминиевых радиаторов</p></i>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                                <div class="alert alert-inline alert-warning" id="text" style="display:none"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <iframe width="336" height="189" src="https://www.youtube.com/embed/pXWAsayTFTo?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    <div class="card-body">
                                        <p class="card-text">Посмотрите видео если ничего не понимаете</p>
                                    </div>
        </div>
                            </div>
                        </div>

                        <br>

                        <!--<h5>Метраж трубы для подводок к приборам</h5>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="riser">Выберите трубу для подводок</label>
                                <div class="input-group">
                                    <select name="supply[item]" class="form-control">
                                        <?php foreach ($items as $item) { ?>
                                            <?php if ($item->id !== PIPE_20) continue; ?>
                                            <option<?php if ($item->id == $supply['item']) { ?> selected<?php } ?> value="<?=$item->id?>"><?=$item->title?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="supply">Введите кол-во метров (холодная + горячая)</label>
                                <div class="input-group" style="width: 100px;">
                                    <input type="number" step="1" min="0" class="form-control" value="<?=($supply['size'] ?? $length)?>" name="supply[size]">
                                    <div class="input-group-append">
                                        <span class="input-group-text">м</span>
                                    </div>
                                </div>
                            </div>
                        </div>-->




                        <br>

                        <!-- <h5>Стояк <small>(если есть)</small></h5> -->
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="riser"><b>Выберите трубу для стояка</b></label>
                                <div class="input-group">
                                    <select name="riser[item]" class="form-control">
                                        <?php foreach ($items as $item) { ?>
                                            <?php if ($item->id == PIPE_20) continue; ?>
                                            <option<?php if ($item->id == $riser['item']) { ?> selected<?php } ?> value="<?=$item->id?>"><?=$item->title?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="riser">Введите кол-во метров на стояк (холодная + горячая)</label>

                                <div class="icon icon--help"><p>Подсчитывается исходя из общего количества бимметаллических и алюминиевых радиаторов</p></div>

                                <div class="input-group" style="width: 100px;">
                                    <input type="number" step="1" min="0" class="form-control" value="<?=($riser['size'] ?? '')?>" name="riser[size]">
                                    <div class="input-group-append">
                                        <span class="input-group-text">м</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="alert alert-info alert-inline">
                            <b>Стояк</b> - труба от конельной до начала этажа, либо турба от близжайшего стояка до начала этажа
                            <?php // @todo Уточнить описание ?>

                            <!-- <img src="/public/img/tailstpcks.jpg" height="60" class="float-right"> -->
                        </div>
                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button id="submit" type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>
    </div>
    
    <script>
        var max = <?=$count?>;

        $(".on-pipes").bind("change keyup", function() {
            var sum = 0;

            $(".on-pipes").each(function(i, el) {
                sum += parseInt($(el).val() || 0);
            });

            if (max - sum > 0) {
                $("#text").text("Вам осталось указать " + (max - sum) + " приборов").show();
            } else if (max - sum < 0) {
                $("#text").text("Вы указали " + (sum - max) + " лишних приборов").show();
            } else {
                $("#text").text("").hide();
            }  
        });
        $(".on-pipes").change();

        $("#submit").click(function(){
            var sum = 0;

            $(".on-pipes").each(function(i, el) {
                sum += parseInt($(el).val() || 0);
            });
            

            if ($("#text").text() !== "") {
                if (! confirm($("#text").text())) {
                    return false;
                }
            }
        });
    </script>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
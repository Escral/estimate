<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'water_additional';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'water']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/water/additional" data-next="/water/work" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Введите дополнительные материалы</h5>
                    <p class="card-text">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th scope="col"></th>
                                    <th scope="col">Кол-во</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($items as $item) { ?>
                                    <tr>
                                        <td class="image-td"><img src="/public/img/items/<?=$item->id?>.jpg" width="60" height="60"></td>
                                        <td><?=$item->title?></td>
                                        <td>
                                            <input type="number" step="1" name="items[<?=$item->id?>]" value="<?=($item->count ? $item->count : '')?>" min="0">

                                            <?php if (in_array($item->id, [ADDITIONAL_SCREWS, ADDITIONAL_FOAM, ADDITIONAL_CORNER_FIXES, ADDITIONAL_CLAMPING_1, ADDITIONAL_CLAMPING_2, ADDITIONAL_CLAMPING_3])) { ?>
                                                шт
                                            <?php } ?>
                                            <?php if (in_array($item->id, [ADDITIONAL_WIRE])) { ?>
                                                кг
                                            <?php } ?>
                                            <?php if (in_array($item->id, [ADDITIONAL_SELF_SCREWS])) { ?>
                                                упаковка(-ки)
                                            <?php } ?>

                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
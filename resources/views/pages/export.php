<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'export';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'export']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    Экспорт
                </div>
                <form action="/export-file" class="card-body json-form" method="post">
                    <table class="table table-bordered">
                        <thead>
                            <tr style="text-align: center">
                                <th></th>
                                <?php foreach (Floor::getSections() as $name => $title) { ?>
                                    <td scope="col"><?=$title?></td>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $floors = Floor::getFloors(); 
                            $firstFloor = key($floors);
                            ?>
                            <?php foreach (Floor::getFloors() as $floor => $k) { ?>
                                <tr>
                                    <td><?=Floor::getFloorName($floor)?></td>
                                    <?php foreach (Floor::getSections() as $name => $title) { ?>
                                            <?php if (in_array($name, ['tools', 'sewage', 'boiler']) and $floor > $firstFloor) { continue; } ?>

                                            <td<?php if (in_array($name, ['tools', 'sewage', 'boiler']) and $floor == $firstFloor) { ?> rowspan="<?=count(Floor::getFloors())?>"<?php } ?> style="text-align: center"><input type="checkbox" <?php if (isset($floorsVisited[$name][$floor]) and $floorsVisited[$name][$floor]) { ?>  checked<?php } ?> name="floors[<?=$name?>][<?=$floor?>]" value="1" /></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>


                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" name="separate" value="1" type="checkbox" id="separate">
                            <label class="form-check-label" for="separate">
                                Экспортировать отдельно
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" name="group" value="1" type="checkbox" id="group">
                            <label class="form-check-label" for="group">
                                Объеденить товары
                            </label>
                        </div>
                    </div>

                    <button class="btn btn-primary float-right" type="submit">Экспорт</button>
                </form>
            </div>
        </div>

        <script>
            var data = JSON.parse('<?=json_encode($floorsVisited)?>');

            $("#section").bind("change", function() {
                var section = $(this).val();

                if (data[section]) {
                    $(".filled").each(function(i, el) {
                        var floor = $(el).data("floor");

                        if (data[section][floor]) {
                            $(el).removeClass('visible');
                        } else {
                            $(el).addClass('visible');
                        }
                    });
                }
            });
        </script>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
<?php use App\Auth;
use App\Models\Estimate;
use App\Models\Floor;
use App\View;

$page = 'floors';
ob_start(); ?>

    <div id="app">
        <?php View::include ("includes/nav.php", ['page' => 'floors']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php if (Estimate::getActive()) { ?>
            <div class="container">
                <div class="card">
                    <div class="card-header">
                        Выбор этажей
                    </div>
                    <form class="card-body" onsubmit="$('#add-floor').click()">
                        <h5 class="card-title">Добавьте необходимые этажи и выберите нужный для редактирования</h5>
                        <p class="card-text">
                        <ul class="list-group">
                            <?php foreach (Floor::getFloors() as $floor) { ?>
                                <li class="list-group-item">
                                    <input type="hidden" name="floors[<?=$floor['ID']?>]" value="1">

                                    <div class="no-edit-floor" style="display: inline">
                                        <?php if (Floor::getFloor() === $floor['ID']) { ?>
                                            <span class="font-weight-bold"><?=$floor['title']?></span>
                                        <?php } else { ?>
                                            <?=$floor['title']?>
                                        <?php } ?>

                                        <a onclick="$(this).parent().parent().find('.edit-floor').css({ display: 'inline' }); $(this).parent().hide(); $(this).parent().parent().find('.edit-floor input').focus()" href="#" style="margin-left: 15px">Изменить</a>
                                    </div>

                                    <div class="edit-floor" style="display: none;">
                                        <div class="form-inline form-group" style="margin-bottom: 0; display: inline">
                                            <input id="add-floor-input" type="text" value="<?=$floor['title']?>" class="form-control" style="margin-right: 15px" onchange="$(this).next().attr('href', '/edit-floor/<?=$floor['ID']?>?name=' + $(this).val())" onkeypress="(event.keyCode == 13 ? ($(this).change() && $(this).next().click()) : '')">
                                            <a href="/edit-floor/<?=$floor['ID']?>" class="ajax">Сохранить</a>
                                        </div>
                                    </div>

                                    <small><a href="/remove-floor/<?=$floor['ID']?>"
                                              class="remove-floor ajax float-right ml-5">
                                            <div class="fa fa-times"></div>
                                        </a></small>

                                    <?php if (Floor::getFloor() !== $floor['ID']) { ?>
                                        <a href="/change-floor/<?=$floor['ID']?>" class="ajax float-right">Перейти</a>
                                    <?php } else { ?>
                                        <b class="float-right" style="color: green">(Выбран)</b>
                                    <?php } ?>

                                    <?php if ($section != '') { ?>
<!--                                        <span class="text-secondary ml-5">--><?//=Floor::getSectionTitleByName($section)?>
<!--                                            : (--><?//=$floorsVisited[$section][$floor['ID']]?><!-- / --><?//=$steps[$section]?>
<!--                                            )</span>-->
                                    <?php } ?>
                                </li>
                            <?php } ?>

                            <li class="list-group-item">
                                <div class="form-inline form-group" style="margin-bottom: 0">
                                    <input id="add-floor-input" type="text" placeholder="<?=$nextFloor . ' этаж'?>" class="form-control" style="margin-right: 15px" onchange="$('#add-floor').attr('href', '/add-floor/<?=$nextFloor?>?name=' + $(this).val())" onkeypress="event.keyCode == 13 ? ($(this).change() && $('#add-floor').click()) : ''">
                                    <script>$('#add-floor-input').focus()</script>
                                    <a id="add-floor" href="/add-floor/<?=$nextFloor?>" class="add-floor ajax">Добавить этаж</a>
                                </div>
                            </li>
                        </ul>
                        </p>

                        <div class="alert alert-info inline">После выбора этажа перейдите на страницу с нужным
                            разделом
                        </div>
                        <div class="alert alert-info inline">Если вы случайно удалили этаж который отредактировали,
                            просто добавьте его заново, все данные сохранены
                        </div>
                    </form>
                </div>
            </div>
        <?php } else { ?>
            <div class="container">
                <h1 class="text-center text-secondary font-weight-light" style="margin-top: 60px;">
                    Создайте или выберите смету в меню слева
                </h1>
            </div>
        <?php } ?>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
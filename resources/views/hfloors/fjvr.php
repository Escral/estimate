<?php use App\Items;
use App\Models\Floor;
use App\View;
use App\Path;
$page = 'hfloors_fjvr';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'hfloors']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/hfloors/fjvr" data-next="/hfloors/gerpex_2" class="card-body ajax-form" method="post">
                    <p class="card-text">
                        <div class="row">
                            <div class="col-md-8">
                                <label style="font-size: 18px;" class="card-title">Вам потребуется <input class="small-input" type="number" name="fjvr" value="<?=$fjvr ?? 1?>" min="0" max="1" step="1"> <?=Items::get(FJVR)->title?></label>
                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <img src="/public/img/fjvr.jpg" width="334">
                                </div>
                            </div>
                        </div>
                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'hfloors_corners_riser' . (isset($next) ? '_2' : '');
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'hfloors']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/hfloors/corners_riser<?=(isset($next) ? '_2' : '')?>" data-next="/hfloors/couplings_riser<?=(isset($next) ? '_2' : '')?>" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Введите кол-во уголков для стояка</h5>
                    <p class="card-text">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th scope="col">Уголок 90°</th>
                                        <th scope="col">Кол-во</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($items90 as $item) { ?>
                                        <tr>
                                            <td><?=$item->title?></td>
                                            <td><input type="number" step="1" name="items[<?=$item->id?>]" value="<?=($item->count ? $item->count : '')?>" min="0"></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th scope="col">Уголок 45°</th>
                                        <th scope="col">Кол-во</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($items45 as $item) { ?>
                                        <tr>
                                            <td><?=$item->title?></td>
                                            <td><input type="number" step="1" name="items[<?=$item->id?>]" value="<?=($item->count ? $item->count : '')?>" min="0"></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
<?php use App\Items;
use App\Models\Floor;
use App\View;
use App\Path;
$page = 'hfloors_insulation_join_2';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'hfloors']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/hfloors/insulation_join_2" data-next="/hfloors/clips_2" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Выберите способ крепления изоляции к полу</h5>
                    <p class="card-text">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col">Кол-во</th>
                                <th scope="col">Кол-во на лист</th>
                                <th scope="col">Размер</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="radio" name="insulation[item]" <?php if (isset($insulation['item']) and $insulation['item'] == 1) { ?>checked<?php } ?> id="insulation_isset1" value="1" required>
                                    </td>
                                    <td class="image-td"><img src="/public/img/items/Парашют.jpg" width="60" height="60"></td>
                                    <td>
                                        <?=Items::get(PARACHUTE_1)->title?>
                                    </td>
                                    <td>
                                        <input id="count1" name="insulation[0][count]" type="number" step="1" min="0" value="<?=$insulation[0]['count'] ?? ''?>"> шт
                                    </td>
                                    <td>
                                        <select class="form-control" name="insulation[0][per_list]" id="per_list">
                                            <option value="1"<?php if ($insulation[0]['per_list'] == 1) {?> selected<?php } ?>>1</option>
                                            <option value="2"<?php if ($insulation[0]['per_list'] == 2) {?> selected<?php } ?>>2</option>
                                            <option value="3"<?php if ($insulation[0]['per_list'] == 3) {?> selected<?php } ?>>3</option>
                                            <option value="4"<?php if ($insulation[0]['per_list'] == 4) {?> selected<?php } ?>>4</option>
                                            <option value="5"<?php if ($insulation[0]['per_list'] == 5) {?> selected<?php } ?>>5</option>
                                        </select> шт
                                    </td>
                                    <td>
                                        <select class="form-control" name="insulation[0][size]">
                                            <?php foreach ($items as $item) { ?>
                                                <option value="<?=$item->id?>"<?php if (isset($insulation[0]['size']) and $insulation[0]['size'] == $item->id) {?> selected<?php } ?>><?=$item->title?></option>
                                            <?php } ?>
                                        </select> мм
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="radio" name="insulation[item]" <?php if (isset($insulation['item']) and $insulation['item'] == 2) { ?>checked<?php } ?> id="insulation_isset2" value="2" required>
                                    </td>
                                    <td class="image-td"><img src="/public/img/items/Монтажная пена.jpg" width="60" height="60"></td>
                                    <td>
                                        <?=Items::get(FOAM_1)->title?>
                                    </td>
                                    <td>
                                        <input id="count2" name="insulation[1][count]" type="number" step="1" min="0" value="<?=$insulation[1]['count'] ?? ''?>"> шт
                                    </td>
                                    <td colspan="2">Кол-во балонов рассчитано исходя<br> из формулы - 1 балон на 6 листов (4 м<sup>2</sup>)</td>
                                </tr>
                            </tbody>
                        </table>
                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>

        <script>
            var count = <?=$insulation_normal[0]['count'] ?? 0?>;

            $("#per_list").bind('change keyup', function () {
                var per_list = $(this).val();

                $("#count1").val(Math.ceil(count * per_list));
            }).change();

        </script>

        <style>
            select.form-control {
                display: inline-block !important;
                width: auto;
            }
        </style>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
<?php use App\Models\Floor;
use App\View;
use App\Path;
$page = 'hfloors_riser' . (isset($next) ? '_2' : '');
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'hfloors']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/hfloors/riser<?=(isset($next) ? '_2' : '')?>" data-next="/hfloors/insulation_riser<?=(isset($next) ? '_2' : '')?>" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Выберите стояк</h5>
                    <p class="card-text">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="riser">Выберите трубу для стояка</label>
                                <div class="input-group">
                                    <select name="riser[item]" class="form-control">
                                        <?php foreach ($items as $item) { ?>
                                            <option<?php if ($item->id == $riser['item']) { ?> selected<?php } ?> value="<?=$item->id?>"><?=$item->title?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="riser">Введите кол-во метров на стояк (длина трубы подачи и обратки)</label>
                                <div class="input-group" style="width: 100px;">
                                    <input type="number" step="1" min="0" class="form-control" value="<?=($riser['size'] ?? '')?>" name="riser[size]">
                                    <div class="input-group-append">
                                        <span class="input-group-text">м</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
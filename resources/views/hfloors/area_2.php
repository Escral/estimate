<?php use App\Models\Floor;
use App\Path;
use App\View;
$page = 'hfloors_area_2';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'hfloors']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/hfloors/area_2" data-next="/hfloors/pipe_2" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Введите общую площадь тёплого пола</h5>
         
                    <p class="card-text">
                        <div class="row">
                            <div class="col-sm-8">
                                <input type="number" name="area" value="<?=$area ?? ''?>" required step="1" min="1"> м<sup>2</sup>
                            </div>
                            <div class="col-sm-4">
                                <a data-fancybox="image" href="/public/img/area.jpg"><img src="/public/img/area.jpg" width="100%"></a>
                            </div>
                        </div>
                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
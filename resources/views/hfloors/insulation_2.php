<?php use App\Items;
use App\Models\Floor;
use App\View;
use App\Path;
$page = 'hfloors_insulation_2';
ob_start(); ?>

    <div id="app">
        <?php View::include("includes/nav.php", ['page' => 'hfloors']); ?>

		<?php View::include ("includes/left.php", (new \App\Controllers\EstimateController())->index()); ?>

        <?php View::include("includes/path.php", ['page' => $page]); ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <?=Path::getTitle($page)?>

                    <h4 class="floor float-right"><?=Floor::getCurrentFloorName()?></h4>
                </div>
                <form action="/hfloors/insulation_2" data-next="/hfloors/skin_2" class="card-body ajax-form" method="post">
                    <h5 class="card-title">Выберите изоляцию</h5>
                    <p class="card-text">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th scope="col">Полистирол</th>
                            <th scope="col">Кол-во листов</th>
                            <th scope="col">Размер (длина x ширина)</th>
                            <th scope="col">Толщина</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <input type="radio" name="insulation[item]" <?php if (isset($insulation['item']) and $insulation['item'] == 1) { ?>checked<?php } ?> id="insulation_isset2" value="1" required>

                                <input type="hidden" name="insulation[0][item]" value="<?=POLYSTYR_1?>" required>
                            </td>
                            <td class="image-td"><img src="/public/img/items/<?=POLYSTYR_1?>.jpg" width="60" height="60"></td>
                            <td>
                                <?=Items::get(POLYSTYR_1)->title?>
                            </td>
                            <td>
                                <input id="count1" name="insulation[0][count]" type="number" step="1" min="0" value="<?=$insulation[0]['count'] ?? ''?>"> шт
                            </td>
                            <td>
                                <input name="insulation[0][width]" type="number" step="1" min="0" class="small-input" id="width1" value="<?=$insulation[0]['width'] ?? 1200?>"> x <input name="insulation[0][lenght]" type="number" step="1" min="0" class="small-input" id="lenght1" value="<?=$insulation[0]['lenght'] ?? 600?>"> мм
                            </td>
                            <td>
                                <select class="form-control" name="insulation[0][thickness]" id="thickness1">
                                    <option value="1"<?php if ($insulation[0]['thickness'] == 1) {?> selected<?php } ?>>1</option>
                                    <option value="2"<?php if ($insulation[0]['thickness'] == 2) {?> selected<?php } ?>>2</option>
                                    <option value="3"<?php if ($insulation[0]['thickness'] == 3) {?> selected<?php } ?>>3</option>
                                    <option value="4"<?php if ($insulation[0]['thickness'] == 4) {?> selected<?php } ?>>4</option>
                                    <option value="5"<?php if ($insulation[0]['thickness'] == 5) {?> selected<?php } ?>>5</option>
                                    <option value="6"<?php if ($insulation[0]['thickness'] == 6) {?> selected<?php } ?>>6</option>
                                    <option value="7"<?php if ($insulation[0]['thickness'] == 7) {?> selected<?php } ?>>7</option>
                                    <option value="8"<?php if ($insulation[0]['thickness'] == 8) {?> selected<?php } ?>>8</option>
                                    <option value="9"<?php if ($insulation[0]['thickness'] == 9) {?> selected<?php } ?>>9</option>
                                    <option value="10"<?php if ($insulation[0]['thickness'] == 10) {?> selected<?php } ?>>10</option>
                                </select> см
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="radio" name="insulation[item]" <?php if (isset($insulation['item']) and $insulation['item'] == 2) { ?>checked<?php } ?> id="insulation_isset2" value="2" required>

                                <input type="hidden" name="insulation[1][item]" value="<?=POLYSTYR_2?>" required>
                            </td>
                            <td class="image-td"><img src="/public/img/items/<?=POLYSTYR_2?>.jpg" width="60" height="60"></td>
                            <td>
                                <?=Items::get(POLYSTYR_2)->title?>
                            </td>
                            <td>
                                <input id="count2" name="insulation[1][count]" type="number" step="1" min="0" value="<?=$insulation[1]['count'] ?? ''?>"> шт
                            </td>
                            <td>
                                <input name="insulation[1][width]" type="number" step="1" min="0" class="small-input" id="width2" value="<?=$insulation[0]['width'] ?? 1158?>"> x <input name="insulation[1][lenght]" type="number" step="1" min="0" class="small-input" id="lenght2" value="<?=$insulation[0]['lenght'] ?? 580?>"> мм
                            </td>
                            <td>
                                <select class="form-control" name="insulation[1][thickness]" id="thickness1">
                                    <option value="1"<?php if ($insulation[1]['thickness'] == 1) {?> selected<?php } ?>>1</option>
                                    <option value="2"<?php if ($insulation[1]['thickness'] == 2) {?> selected<?php } ?>>2</option>
                                    <option value="3"<?php if ($insulation[1]['thickness'] == 3) {?> selected<?php } ?>>3</option>
                                    <option value="4"<?php if ($insulation[1]['thickness'] == 4) {?> selected<?php } ?>>4</option>
                                    <option value="5"<?php if ($insulation[1]['thickness'] == 5) {?> selected<?php } ?>>5</option>
                                    <option value="6"<?php if ($insulation[0]['thickness'] == 6) {?> selected<?php } ?>>6</option>
                                    <option value="7"<?php if ($insulation[0]['thickness'] == 7) {?> selected<?php } ?>>7</option>
                                    <option value="8"<?php if ($insulation[0]['thickness'] == 8) {?> selected<?php } ?>>8</option>
                                    <option value="9"<?php if ($insulation[0]['thickness'] == 9) {?> selected<?php } ?>>9</option>
                                    <option value="10"<?php if ($insulation[0]['thickness'] == 10) {?> selected<?php } ?>>10</option>
                                </select> см
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </p>

                    <hr>

                    <div class="form-group comment">
                        <label for="comment">Мой комментарий</label>
                        <textarea class="form-control" name="comment"><?=$comment ?? ''?></textarea>
                    </div>

                    <input type="hidden" name="submit" value="1">
                    <button type="submit" class="btn btn-success">Далее</button>
                </form>
            </div>
        </div>

        <script>
            var area = <?=$area ?? 0?>;

            $("#width1, #lenght1").bind('change keyup', function () {
                var width = $("#width1").val();
                var lenght = $("#lenght1").val();
                $("#count1").val(Math.ceil(area / (width * lenght / 1000000)));
            }).change();

            $("#width2, #lenght2").bind('change keyup', function () {
                var width = $("#width2").val();
                var lenght = $("#lenght2").val();
                $("#count2").val(Math.ceil(area / (width * lenght / 1000000)));
            }).change();

        </script>

        <style>
            select.form-control {
                display: inline-block !important;
                width: auto;
            }
        </style>
    </div>

<?php $content = ob_get_contents();
ob_end_clean();
App\View::registerHook('content', $content); ?>
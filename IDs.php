<?php

// Радиаторы
define("RADIATOR_STEAL_1", 1);
define("RADIATOR_STEAL_2", 2);
define("RADIATOR_ALLUM_3", 3);

// Труба
define("PIPE_20", 101);
define("PIPE_25", 102);
define("PIPE_32", 103);
define("PIPE_16", 104);

// Изоляция
define("INSULATION_20", 111);
define("INSULATION_25", 112);
define("INSULATION_32", 113);
// Углы
define("CORNER_90_20", 121);
define("CORNER_90_25", 122);
define("CORNER_90_32", 123);
define("CORNER_45_20", 124);
define("CORNER_45_25", 125);
define("CORNER_45_32", 126);

// Муфты
define("COUPLING_20", 131);
define("COUPLING_25", 132);
define("COUPLING_32", 133);

// Переходы ППР
define("TRANSITION_25_20", 141);
define("TRANSITION_32_25", 142);
define("TRANSITION_32_20", 143);

// Тройники
define("TEE_20", 151);
define("TEE_25", 152);
define("TEE_32", 153);
define("TEE_25_20_25", 154);
define("TEE_25_20_20", 155);
define("TEE_32_20_32", 156);
define("TEE_32_25_32", 157);

// Краны
define("VALVE_CORNER_IN", 161);
define("VALVE_CORNER_OUT", 162);
define("VALVE_STRAIGHT_IN", 163);
define("VALVE_STRAIGHT_OUT", 164);
define("VALVE_TERMO_CORNER_IN", 165);
define("VALVE_TERMO_CORNER_OUT", 166);
define("VALVE_TERMO_STRAIGHT_IN", 167);
define("VALVE_TERMO_STRAIGHT_OUT", 168);
define("VALVES_GERPEX", 169);

// Ножки для радиаторов
define("LEGS_STEEL", 171);
define("LEGS_ALUMINUM", 172);

// МП-шки
define("MP_20_1_2_FATHER", 181);
define("MP_20_1_2_MOTHER", 182);

// Коллектор
define("COLLECTOR_CORNER_1_2_M_F", 1901);
define("COLLECTOR_WITH_CONSUM", 1911);
define("COLLECTOR_WITHOUT_CONSUM", 1912);
define("COLLECTOR_TAILSTOCKS", 1921);
define("COLLECTOR_VALVES", 1922);
define("COLLECTOR_MP", 1923);
define("COLLECTOR_CORNERS", 1924);
define("COLLECTOR_GERPEX", 1931);

define("LOCKER_IN", 1941);
define("LOCKER_OUT", 1942);

// Работа
define("WORK_RADIATORS", 191);
define("WORK_DRILL", 192);
define("WORK_RISER", 193);
define("WORK_HIGHWAY", 194);
define("WORK_ADDITIONAL", 195);
define("WORK_COLLECTOR", 196);
define("WORK_COLLECTOR_LOCKER", 197);
define("WORK_FJVR", 198);

// Дополнительные материалы
define("ADDITIONAL_SCREWS", 201);
define("ADDITIONAL_WIRE", 202);
define("ADDITIONAL_FOAM", 203);
define("ADDITIONAL_CORNER_FIXES", 204);
define("ADDITIONAL_CLAMPING_1", 205);
define("ADDITIONAL_CLAMPING_2", 206);
define("ADDITIONAL_CLAMPING_3", 207);
define("ADDITIONAL_SELF_SCREWS", 208);
define("ADDITIONAL_WALL_INSULATION", 209);

// Футорки
define("FUTORKA_1", 211);



// Водоснабжение
// ------------------------------------------------------

// Приборы
define("PRIBOR_1", 301);
define("PRIBOR_2", 302);
define("PRIBOR_3", 303);
define("PRIBOR_4", 304);
define("PRIBOR_5", 305);
define("PRIBOR_6", 306);
define("PRIBOR_7", 307);
define("PRIBOR_8", 308);
define("PRIBOR_9", 309);
define("PRIBOR_10", 310);
define("PRIBOR_11", 311);

// Заглушки
define("STUB_1_2_FATHER", 221);

// Монтажные работы

define("WORK_PRIBOR_1", 331);
define("WORK_PRIBOR_2", 332);
define("WORK_PRIBOR_3", 333);
define("WORK_PRIBOR_4", 334);
define("WORK_PRIBOR_5", 335);
define("WORK_PRIBOR_6", 336);
define("WORK_PRIBOR_7", 337);
define("WORK_PRIBOR_8", 338);
define("WORK_PRIBOR_9", 339);
define("WORK_PRIBOR_10", 340);
define("WORK_PRIBOR_11", 341);



// Тёплый пол
// =======================
define("H_PIPE_16", 401);

// Полистирол
define("POLYSTYR_1", 411);
define("POLYSTYR_2", 412);

// Плёнка
define("SKIN_1", 421);

// Крепления изоляции к полу
define("JOIN_1", 431);
define("JOIN_2", 432);
define("JOIN_3", 433);
define("JOIN_4", 434);
define("JOIN_5", 435);

define("FOAM_1", 436); // Пена монтажная
define("PARACHUTE_1", 437); // Парашюты

define("FJVR", 438); // FJVR

// Клипсы
define("CLIPS_1", 441);

// Монтажные работы
define("WORK_HFLOORS", 451);